• Hindi Bible: Hindi Indian Revised Version 2017
• Arabic Bible: Smith & Van Dyke 1865
• Chinese Bible: Chinese Union Version Simplified
• Japanese Bible: New Japanese Bible 1973
• Russian Bible: Russian Synodal Translation 1876
• New setting for some Bibles: Language of User interface
• Removed German translation (please use the new setting)
