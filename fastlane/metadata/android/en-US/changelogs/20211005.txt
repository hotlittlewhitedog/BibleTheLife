• Better performance
• Display support for visually impaired: large fonts supported
• Swipe left/right to change page
• Search in displayed Bibles
• Support: MacOS
