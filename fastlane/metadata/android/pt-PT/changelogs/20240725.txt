(Rebuild of 1.27 to solve F-Droid problem)
• Bíblia turca: New Turkish Bible 2001
• Bíblia bengali: Bengali C.L. Bible 2016
• Bíblia suaíli: Swahili Union Version 1997
• Bíblia alemã: Elberfelder 1932
• Bíblia romena: Romanian Cornilescu 1928
• Bíblia polaca: Biblia Warszawska 1975
