#!/bin/sh
rm -rf .flatpak-builder

if [ -d "build" ]; then
  cd build || exit
  rm -rf *
  cd ..
fi

if [ -d "flatpak-build" ]; then
  cd flatpak-build || exit
  rm -rf *
  cd ..
fi

