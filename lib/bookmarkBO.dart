library bible.bookmark_bo;

class BookmarkBO {
  late int bmId;
  late String bmCurrent;
  late String bmDesc;
  late String bmDev1;
  late String bmDev2;
  late String bmDev3;
  late String bmPrev1;
  late String bmPrev2;
  late String bmPrev3;
  late String bmPrev4;
  late String bmPrev5;
}
