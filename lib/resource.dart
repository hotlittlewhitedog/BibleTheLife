library bible.resource;

import 'package:bible_multi_the_life/dbHelper.dart';
import 'package:bible_multi_the_life/pcommon.dart';
import 'package:bible_multi_the_life/penums.dart';

///Locale var
BBLocale _locale = BBLocale.EN;
BBLocale _altLocale = BBLocale.EN;
Map fav = {};

///Locale
void setLocale(final BBLocale locale, final BBLocale altLocale) {
  _locale = locale;
  _altLocale = altLocale;
}

BBLocale getLocale() {
  return _locale;
}

BBLocale getAltLocale() {
  return _altLocale;
}

Future<void> setFav() async {
  final DbHelper dal = DbHelper.instance;
  fav = await dal.generateBookmarkMap();
}

//Undefinite order
List<Map> getListAllBibleSettings() => [
    {'bbName': 'k', 'text': '(EN) King James 1611', 'order': 100},
    {'bbName': '2', 'text': '(EN) King James 2000', 'order': 100},
    {'bbName': 'v', 'text': '(ES) Reina Valera 1909', 'order': 100},
    {'bbName': '9', 'text': '(ES) Reina Valera 1989', 'order': 100},
    {'bbName': 'l', 'text': '(FR) Louis Segond 1910', 'order': 100},
    {'bbName': 'o', 'text': '(FR) Ostervald 1996', 'order': 100},
    {'bbName': 'd', 'text': '(IT) Diodati 1649', 'order': 100},
    {'bbName': '1', 'text': '(IT) Nuova Diodati 1991', 'order': 100},
    {'bbName': 'a', 'text': '(PT) Almeida CF 1995', 'order': 100},
    {'bbName': 's', 'text': '(DE) Schlachter 1951', 'order': 100},
    {'bbName': 'e', 'text': '(DE) Elberfelder 1932', 'order': 100},
    {'bbName': 'i', 'text': '(IN) Hindi Indian Revised 2017', 'order': 100},
    {'bbName': 'y', 'text': '(AR) Smith & Van Dyke 1865', 'order': 100},
    {'bbName': 'c', 'text': '(CN) Chinese Union Version Simplified', 'order': 100},
    {'bbName': 'j', 'text': '(JP) New Japanese Bible 1973', 'order': 100},
    {'bbName': 'r', 'text': '(RU) Russian Synodal Translation 1876', 'order': 100},
    {'bbName': 't', 'text': '(TR) New Turkish Bible 2001', 'order': 100},
    {'bbName': 'b', 'text': '(BD) Bengali C.L. 2016', 'order': 100},
    {'bbName': 'h', 'text': '(SW) Swahili Union Version 1997', 'order': 100},
    {'bbName': 'u', 'text': '(RO) Romanian Cornilescu 1928', 'order': 100},
    {'bbName': 'z', 'text': '(PL) Biblia Warszawska 1975', 'order': 100}
  ];

List<String> getListAllFontNames() => [
  "Aphont.bold",
  "Aphont.regular",
  "BebasNeue.regular",
  "ComicSansMs3.regular",
  "HelveticaNeue.condensed",
  "OpenSans.regular",
  "AveriaGruesaLibre.regular",
  "Bptypewrite.regular",
  "Caslon-os.regular",
  "Code-new-roman.regular",
  "Courier-prime-code.regular",
  "Cutive.regular",
  "DayRoman.regular",
  "DejaVu-sans.regular",
  "Donegal-one.regular",
  "Droid-sans-mono.regular",
  "Droid-sans.regular",
  "Expressway-free.regular",
  "Handlee.regular",
  "Larabiefont-free.regular",
  "Libre-baskerville.regular",
  "LibreFranklin-Light",
  "Montserrat.regular",
  "Nk57-monospace.condensed",
  "Nk57-monospace.regular",
  "Noto-sans.regular",
  "Noto-serif.regular",
  "Oswald.regular",
  "Questrial.regular",
  "RobotoCondensed.regular",
  "Ropa-sans.regular"
];

List<int> getListAllFontSizes() =>  [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 44, 45, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94, 96, 98, 100];

List<int> getListAllCustomPrimaryColors() => [
  //0
  -1,

  //1 WHITE
  0xFFFFFFFF,

  //2 RED
  0xFFF44336,

  //3 PINK
  0xFFE91E63,

  //4 VIOLET
  0xFF9C27B0,

  //5 DEEP PURPLE
  0xFF673AB7,

  //6 INDIGO
  0xFF3F51B5,

  //7 BLUE
  0xFF2196F3,

  //8 LIGHT BLUE
  0xFF03A9F4,

  //9 CYAN
  0xFF00BCD4,

  //10 TEAL
  0xFF009688,

  //11 GREEN
  0xFF4CAF50,

  //12 LIGHT GREEN
  0xFF8BC34A,

  //13 LIME
  0xFFCDDC39,

  //14 YELLOW
  0xFFFFEB3B,

  //15 AMBER
  0xFFFFC107,

  //16 ORANGE
  0xFFFF9800,

  //17 DEEP ORANGE
  0xFFFF5722,

  //18 BROWN
  0xFF795548,

  //19 GREY
  0xFF9E9E9E,

  //20 BLUE GREY
  0xFF607D8B,

  //21 BLACK
  0xFF424242,
];

Map<int, List<int>> _getListAllCustomShadeColors() => {
      0: [-1],
      1: [
        //WHITE:
        0xFFFFFFFF,
        0xFF141414,
        0xFF1E1F22,
        0xFF114957,
        0xFF165E70,
        0xFF2AACB8,
        0xFF72D6D6,
        0xFF5B786A,
        0xFF5F826B,
        0xFF2D543F,
        0xFF293C40,
        0xFF373B39,
        0xFF393B40,
        0xFF402F33,
        0xFF450505,
        0xFF66313F,
        0xFFBA6387,
        0xFF73AD2B,
        0xFF8BB33D,
        0xFF7EC482,
        0xFF42BD77,
        0xFF857042,
        0xFF5E5339,
        0xFFF49810,
        0xFFF2C55C,
        0xFFC29E4A,
        0xFFB06100,
        0xFFCF8E6D,
        0xFFD64D5B,
        0xFFF75464,
        0xFFFA6675,
        0xFFFA7DB1,
        0xFFC77DBB,
        0xFF56A8F5,
        0xFF467FF2,
        0xFF548AF7,
        0xFFB189F5,
        0xFFBCBEC4,
        0xFFC3C3C3,
        0xFFBBBBBB,
        0xFF9DA0A8,
        0xFF868991,
        0xFF6F737A,

        //0xFFFDFDFD, 0xFFFAFAFA, 0xFFF5F5F5, 0xFFF0F0F0, 0xFFEBEBEB, 0xFFFFFFFF, 0xFFE5E5E5, 0xFFDADADA, 0xFFBEBEBE, 0xFFA0A0A0,
      ],
      2: [
        //RED:
        0xFFFFEBEE, 0xFFFFCDD2, 0xFFEF9A9A, 0xFFE57373, 0xFFEF5350, 0xFFF44336, 0xFFE53935, 0xFFD32F2F, 0xFFC62828, 0xFFB71C1C,
      ],
      3: [
        //PINK:
        0xFFFCE4EC, 0xFFF8BBD0, 0xFFF48FB1, 0xFFF06292, 0xFFEC407A, 0xFFE91E63, 0xFFD81B60, 0xFFC2185B, 0xFFAD1457, 0xFF880E4F,
      ],
      4: [
        //VIOLET:
        0xFFF3E5F5, 0xFFE1BEE7, 0xFFCE93D8, 0xFFBA68C8, 0xFFAB47BC, 0xFF9C27B0, 0xFF8E24AA, 0xFF7B1FA2, 0xFF6A1B9A, 0xFF4A148C,
      ],
      5: [
        //DEEP PURPLE:
        0xFFEDE7F6, 0xFFD1C4E9, 0xFFB39DDB, 0xFF9575CD, 0xFF7E57C2, 0xFF673AB7, 0xFF5E35B1, 0xFF512DA8, 0xFF4527A0, 0xFF311B92,
      ],
      6: [
        //INDIGO:
        0xFFE8EAF6, 0xFFC5CAE9, 0xFF9FA8DA, 0xFF7986CB, 0xFF5C6BC0, 0xFF3F51B5, 0xFF3949AB, 0xFF303F9F, 0xFF283593, 0xFF1A237E,
      ],
      7: [
        //BLUE:
        0xFFE3F2FD, 0xFFBBDEFB, 0xFF90CAF9, 0xFF64B5F6, 0xFF42A5F5, 0xFF2196F3, 0xFF1E88E5, 0xFF1976D2, 0xFF1565C0, 0xFF0D47A1,
      ],
      8: [
        //LIGHT BLUE:
        0xFFE1F5FE, 0xFFB3E5FC, 0xFF81D4FA, 0xFF4FC3F7, 0xFF29B6F6, 0xFF03A9F4, 0xFF039BE5, 0xFF0288D1, 0xFF0277BD, 0xFF01579B,
      ],
      9: [
        //CYAN:
        0xFFE0F7FA, 0xFFB2EBF2, 0xFF80DEEA, 0xFF4DD0E1, 0xFF26C6DA, 0xFF00BCD4, 0xFF00ACC1, 0xFF0097A7, 0xFF00838F, 0xFF006064,
      ],
      10: [
        //TEAL:
        0xFFE0F2F1, 0xFFB2DFDB, 0xFF80CBC4, 0xFF4DB6AC, 0xFF26A69A, 0xFF009688, 0xFF00897B, 0xFF00796B, 0xFF00695C, 0xFF004D40,
      ],
      11: [
        //GREEN:
        0xFFE8F5E9, 0xFFC8E6C9, 0xFFA5D6A7, 0xFF81C784, 0xFF66BB6A, 0xFF4CAF50, 0xFF43A047, 0xFF388E3C, 0xFF2E7D32, 0xFF1B5E20,
      ],
      12: [
        //LIGHT GREEN:
        0xFFF1F8E9, 0xFFDCEDC8, 0xFFC5E1A5, 0xFFAED581, 0xFF9CCC65, 0xFF8BC34A, 0xFF7CB342, 0xFF689F38, 0xFF558B2F, 0xFF33691E,
      ],
      13: [
        //LIME:
        0xFFF9FBE7, 0xFFF0F4C3, 0xFFE6EE9C, 0xFFDCE775, 0xFFD4E157, 0xFFCDDC39, 0xFFC0CA33, 0xFFAFB42B, 0xFF9E9D24, 0xFF827717,
      ],
      14: [
        //YELLOW:
        0xFFFFFDE7, 0xFFFFF9C4, 0xFFFFF59D, 0xFFFFF176, 0xFFFFEE58, 0xFFFFEB3B, 0xFFFDD835, 0xFFFBC02D, 0xFFF9A825, 0xFFF57F17,
      ],
      15: [
        //AMBER:
        0xFFFFF8E1, 0xFFFFECB3, 0xFFFFE082, 0xFFFFD54F, 0xFFFFCA28, 0xFFFFC107, 0xFFFFB300, 0xFFFFA000, 0xFFFF8F00, 0xFFFF6F00,
      ],
      16: [
        //ORANGE:
        0xFFFFF3E0, 0xFFFFE0B2, 0xFFFFCC80, 0xFFFFB74D, 0xFFFFA726, 0xFFFF9800, 0xFFFB8C00, 0xFFF57C00, 0xFFEF6C00, 0xFFE65100,
      ],
      17: [
        //DEEP ORANGE:
        0xFFFBE9E7, 0xFFFFCCBC, 0xFFFFAB91, 0xFFFF8A65, 0xFFFF7043, 0xFFFF5722, 0xFFF4511E, 0xFFE64A19, 0xFFD84315, 0xFFBF360C,
      ],
      18: [
        //BROWN:
        0xFFEFEBE9, 0xFFD7CCC8, 0xFFBCAAA4, 0xFFA1887F, 0xFF8D6E63, 0xFF795548, 0xFF6D4C41, 0xFF5D4037, 0xFF4E342E, 0xFF3E2723,
      ],
      19: [
        //GREY:
        0xFFFAFAFA, 0xFFF5F5F5, 0xFFEEEEEE, 0xFFE0E0E0, 0xFFBDBDBD, 0xFF9E9E9E, 0xFF757575, 0xFF616161, 0xFF424242, 0xFF212121,
      ],
      20: [
        //BLUE GREY:
        0xFFECEFF1, 0xFFCFD8DC, 0xFFB0BEC5, 0xFF90A4AE, 0xFF78909C, 0xFF607D8B, 0xFF546E7A, 0xFF455A64, 0xFF37474F, 0xFF263238,
      ],
      21: [
        //BLACK:
        0xFFFAFAFA, 0xFFF2F2F2, 0xFFE0E0E0, 0xFFBDBDBD, 0xFF757575, 0xFF424242, 0xFF212121, 0xFF141414, 0xFF0A0A0A, 0xFF000000,
      ],
    };

List<int> getListCustomShadeColors(final int k) => _getListAllCustomShadeColors()[ k ] ?? [];

/* OLD:
List<Color> getListAllCustomColors() => [
  Colors.yellow,
  Colors.yellowAccent,
  Colors.amber,
  Colors.amberAccent,
  Colors.orange,
  Colors.orangeAccent,
  Colors.deepOrange,
  Colors.deepOrangeAccent,
  Colors.pink,
  Colors.pinkAccent,
  Colors.red,
  Colors.redAccent,
  Colors.lime,
  Colors.limeAccent,
  Colors.teal,
  Colors.tealAccent,
  Colors.lightGreen,
  Colors.lightGreenAccent,
  Colors.green,
  Colors.greenAccent,
  Colors.lightBlue,
  Colors.lightBlueAccent,
  Colors.blue,
  Colors.blueAccent,
  Colors.purple,
  Colors.purpleAccent,
  Colors.deepPurple,
  Colors.deepPurpleAccent,
  Colors.white,
  Colors.white60,
  Colors.white10,
  Colors.white12,
  Colors.white24,
  Colors.white30,
  Colors.white38,
  Colors.white54,
  Colors.white70,
  Colors.grey.shade100,
  Colors.grey.shade200,
  Colors.grey.shade300,
  Color(0xFFD6D6D6), //shade350
  Colors.grey.shade400,
  Colors.grey,
  Colors.grey.shade600,
  Colors.grey.shade700,
  Colors.blueGrey,
  Colors.black,
  Colors.black26,
  Colors.black12,
  Colors.black38,
  Colors.black45,
  Colors.black54,
  Colors.black87,
];
*/


/// Better to use getString(int id)
String getStringByName(final String id) {
  final String k = id;
  switch (getAltLocale()) {
    case BBLocale.EN:
      return _String.map[k]!;
    case BBLocale.ES:
      return _StringEs.map[k]!;
    case BBLocale.PT:
      return _StringPt.map[k]!;
    case BBLocale.FR:
      return _StringFr.map[k]!;
    case BBLocale.IT:
      return _StringIt.map[k]!;
    //case BBLocale.DE:
    //return _StringDe.map[k];
    default:
      return _String.map[k]!;
  }
}

/// Get string in current locale
String getString(var id) {
  final String k = id.toString().replaceFirst("id.", "");
  switch (getAltLocale()) {
    case BBLocale.EN:
      return _String.map[k]!;
    case BBLocale.ES:
      return _StringEs.map[k]!;
    case BBLocale.PT:
      return _StringPt.map[k]!;
    case BBLocale.FR:
      return _StringFr.map[k]!;
    case BBLocale.IT:
      return _StringIt.map[k]!;
    //case BBLocale.DE:
    //return _StringDe.map[k];
    default:
      return _String.map[k]!;
  }
}

/* This code is not yet generated */
enum id {
  mnuFavAdd,
  mnuFavUpdate,
  mnuAbout,
  mnuArts,
  mnuAltLanguage,
  bible,
  mnuBiblePreferred,
  mnuBibleToDisplay,
  mnuBooks,
  mnuFavorites,
  mnuChapters,
  mnuClose,
  mnuDelete,
  mnuHelp,
  mnuHelpEmo,
  mnuHelpCustomBible,
  mnuHistory,
  mnuInviteFriend,
  mnuPrbls,
  mnuQuit,
  mnuSearch,
  mnuSettings,
  mnuSettingsBibles,
  mnuSettingsFonts,
  mnuSettingsFontSize,
  mnuSettingsLayoutDynamicHeader,
  mnuSettingsLayoutDynamicSub,
  mnuSettingsStyleHighlightSearch,
  mnuSettingsCustomBibles,
  mnuSettingsCustomBibleVFontFamily,
  mnuSettingsCustomBibleVFontSize,
  mnuSettingsCustomBibleVTextFgColor,
  mnuSettingsCustomBibleVTextBgColor,
  mnuSettingsCustomBibleVNumberFgColor,
  mnuSettingsCustomBibleVNumberBgColor,
  mnuSettingsCustomBibleVCrFgColor,
  mnuSettingsCustomBibleVCrBgColor,
  mnuSettingsCustomBibleResetWithDefaults,
  mnuSettingsCustomBibleResetWithBible,
  mnuSettingsCustomBibleCopy,
  mnuSettingsCustomBiblePaste,
  mnuThemes,
  mnuOpen,
  mnuOpenChapter,
  mnuOpenCR,
  mnuClipboard,
  mnuClipboardClear,
  mnuClipboardAddVerse,
  mnuClipboardAddChapter,
  mnuSave,
  mnuShare,
  favAll,
  favOrderByBook,
  favOrderByDate,
  hintFavDesc,
  hintSearchBible,
  toastAdded,
  toastDeleted,
  toastWarnKJV2000Limit500,
  toastWarnKJV2000LimitFullBook,
  toastWarnNoResultFound,
  validatorSearchQueryLimit,
  aboutContactMe,
  copiedClipboard,
  emptyClipboard,
  cr,
  crShort,
  inviteFriendClipboardMsg1,
  inviteFriendClipboardMsg2,
  inviteFriendPageMsg,
  ART_APP_HELP_CONTENT,
  fav0,
  fav1,
  fav2,
  fav10,
  fav20,
  fav23,
  fav30,
  fav40,
  fav50,
  fav60,
  fav70,
  fav80,
  fav90,
  fav100,
  fav110
}

//Note: replace blockquote by br
abstract class _String {
  static const map = {
    "mnuFavAdd": "Add a favorite",
    "mnuFavUpdate": "Update a favorite",
    "mnuAbout": "About / Contact",
    "mnuArts": "Articles",
    "mnuAltLanguage": "Language of user interface",
    "bible": "Bible",
    "mnuBiblePreferred": "Bible preferred",
    "mnuBibleToDisplay": "Bibles to display",
    "mnuBooks": "Books",
    "mnuFavorites": "Favorites",
    "mnuChapters": "Chapters",
    "mnuClose": "Close",
    "mnuDelete": "Delete",
    "mnuHelp": "Help",
    "mnuHelpEmo": "- You can find other emoji in your virtual keyboard or on Internet\n\n- The emoji can be displayed differently on several devices and tools",
    "mnuHelpCustomBible": "- \"Auto\" means that the writing color and/or background color are those of theme\n\n- The advantage is that when changing theme, these colors will be automatically adapted (dynamic)\n\n- \"Auto\" is particularly useful for the background color. This will avoid you having, for example, a text written in blue on a black background while the theme is white!",
    "mnuHistory": "History",
    "mnuInviteFriend": "Invite a friend",
    "mnuPrbls": "Parables",
    "mnuSearch": "Search",
    "mnuSettings": "Settings",
    "mnuSettingsBibles": "Bible",
    "mnuSettingsFonts": "Font (default)",
    "mnuSettingsFontSize": "Font size (default)",
    "mnuSettingsLayoutDynamicHeader" : "--- Layout (grid) ---",
    "mnuSettingsLayoutDynamicSub": "How much columns?",
    "mnuSettingsStyleHighlightSearch": "Search style",
    "mnuSettingsCustomBibles": "Customize Bibles",
    "mnuSettingsCustomBibleVFontFamily": "Font",
    "mnuSettingsCustomBibleVFontSize": "Font size",
    "mnuSettingsCustomBibleVTextFgColor": "Verse: forecolor",
    "mnuSettingsCustomBibleVTextBgColor": "Verse: backcolor",
    "mnuSettingsCustomBibleVNumberFgColor": "Number: forecolor",
    "mnuSettingsCustomBibleVNumberBgColor": "Number: backcolor",
    "mnuSettingsCustomBibleVCrFgColor": "Reference: forecolor",
    "mnuSettingsCustomBibleVCrBgColor": "Reference: backcolor",
    "mnuSettingsCustomBibleResetWithDefaults": "Reset (Default values)",
    "mnuSettingsCustomBibleResetWithBible": "Reset (Bible values)",
    "mnuSettingsCustomBibleCopy": "Copy",
    "mnuSettingsCustomBiblePaste": "Paste",
    "mnuThemes": "Theme",
    "mnuOpen": "Open",
    "mnuOpenChapter": "Chapter",
    "mnuOpenCR": "Cross references",
    "mnuClipboard": "Clipboard",
    "mnuClipboardClear": "Clear all",
    "mnuClipboardAddVerse": "Add the verse",
    "mnuClipboardAddChapter": "Add the chapter",
    "mnuSave": "Save",
    "mnuShare": "Share",
    "favAll": "[ALL]",
    "favOrderByBook": "By book",
    "favOrderByDate": "By date",
    "hintFavDesc": "Description",
    "hintSearchBible": "Search in {0}",
    "toastAdded": "Added",
    "toastDeleted": "Deleted",
    "toastWarnKJV2000Limit500": "No more than 500 verses © KJ2K, RVA1989, LND1991, IRV2017, NJB1973, CUVS, NTB2001, BCL2016, RUC1928, BW1975",
    "toastWarnKJV2000LimitFullBook": "No full book © KJ2K, RVA1989, LND1991, IRV2017, NJB1973, CUVS, NTB2001, BCL2016, RUC1928, BW1975",
    "toastWarnNoResultFound": "No result found!",
    "validatorSearchQueryLimit": "Min. $searchQueryLimit characters",
    "aboutContactMe": "For any issues or suggestions, please contact me.",
    "copiedClipboard": "Copied to clipboard",
    "emptyClipboard": "Empty!",
    "cr": "Cross references",
    "crShort": "CR",
    "inviteFriendClipboardMsg1":
        "Hi,\n\nI invite you to install this Bible app (Bible Multi The Life), available on App Store:\n\n",
    "inviteFriendClipboardMsg2": "Glory to God.",
    "inviteFriendPageMsg":
        "The text has been copied to the clipboard.\n\nNow you can paste it into emails, forums, Facebook, SMS...\n\nGlory to God.",
    "ART_APP_HELP_CONTENT": """
<i>Hi,</i> I\'m a Christian programmer and made this application with my heart to serve the Lord.<br><br> The end of time is close with tribulations, antichrist, Jesus and the final judgment.<br><br>
<i>Whattt! Are you serious?</i> Yes, I\'m serious, signs are everywhere.<br><br> This application, giving the real Word of God, can help you be strong during this period when the devil will try to deceive you more than ever and to be ready
when Christ will come back.<br><br> Remember yourselves that the devil knows the Scriptures.<br><br> I added only good original Bibles (no re-edition where possible) in several languages, I can understand, to help you find Jesus because he\'s the only way towards our Father.<br><br>
Don\'t remain in the darkness little children of God.<br><br> Be good to your brothers and sisters, share the word of God, love, pray, repent, have faith, joy,
follow the commandments, stop your sins and sow abundantly.<br><br> Let\'s follow Jesus, the good Shepherd, our only saviour... God bless you. Amen.<br><br>
<br>
<b>John 14.6:</b> Jesus said unto him, I am the way, the truth, and the life: no man comes unto the Father, but by me.
<br>
<br>
<b>John 15.12:</b> This is my commandment, That you love one another, as I have loved you.
<br>
<br>
<b>John 10.11:</b> I am the good shepherd: the good shepherd gives his life for the sheep.
<br>
<br>
<b>Psalms 23.3:</b> He restores my soul: he leads me in the paths of righteousness for his name\'s sake.<br>
<br>
<b>Psalms 23.4:</b> Yea, though I walk through the valley of the shadow of death, I will fear no evil: for you are with me; your rod and your staff they comfort me.<br>
<br>
<b>Psalms 23.5:</b> You prepare a table before me in the presence of my enemies: you anoint my head with oil; my cup runs over.<br>
<br>
<b>Psalms 23.6:</b> Surely goodness and mercy shall follow me all the days of my life: and I will dwell in the house of the LORD forever.
<br>
<br>
<b>1Corinthians 3.16:</b> Know you not that you are the temple of God, and that the Spirit of God dwells in you?<br>
<br>
<b>1Corinthians 3.17:</b> If any man defiles the temple of God, him shall God destroy; for the temple of God is holy, which temple you are.<br>
<br><br>
<H>Search in the Bible</H>
Syntax: &lt;book&gt;<br>
Syntax: &lt;book&gt; &lt;chapter&gt;<br>
Syntax: &lt;book&gt; &lt;chapter&gt; &lt;verse&gt;<br>
Syntax: &lt;book&gt; &lt;chapter&gt; &lt;verse from&gt; &lt;verse to&gt;<br>
Syntax: &lt;book&gt; &lt;expression&gt;<br>
Syntax: &lt;book&gt; &lt;chapter&gt; &lt;expression&gt;<br>
Syntax: &lt;expression&gt;<br>
<br><br>
<b>Examples:</b><br>
• Genesis 1<br>
Gives chapter 1 of Genesis<br>
<br>
• Genesis 1 5<br>
Gives the verse 1.5 of Genesis :)<br>
<br>
• Genesis fire<br>
Searches in Genesis all occurrences of \'fire\'<br>
<br>
• 1 6 lord<br>
Searches in Genesis chapter 6 all occurrences of \'lord\'<br>
<br>
• Abraham<br>
Searches \'Abraham\' in all the Bible<br>
<br>
• Revelation horse<br>
Searches the horses of the Revelation :)<br>
<br>
• 66 babylone<br>
Searches \'babylone\' in book 66 (Revelation)<br>
<br>
• peter%church<br>
Searches in the order, \'peter\' and \'church\' in all the Bible<br>
<br><br>
<b>Remarks:</b><br>
- The book names are case sensitive, so select them in the list<br>
- Put accents in your words if they have:<br>
Example: j<u>é</u>zabel (French), jezebel (English), <u>É</u>lie (French), Elijah (English), Elia (Italian), El<u>í</u>as (Spanish)<br>
<br><br>
<H>Agreement</H>
This application will remain totally free, without advertising, redirections, offline and will not be sold. It is not a demo but a complete version.<br>
<br><br>
<H>Contact</H>
For any issues or suggestions, please contact me (About menu).<br>
<br><br>
""",
    "fav0": "Internal",
    "fav1": "Favorite",
    "fav2": "Reading",
    "fav10": "Love",
    "fav20": "Top 100",
    "fav23": "To read",
    "fav30": "To study",
    "fav40": "To solve",
    "fav50": "Done",
    "fav60": "Exclamation",
    "fav70": "Question",
    "fav80": "Important",
    "fav90": "Danger",
    "fav100": "Death",
    "fav105": "Life",
    "fav110": "Prophecy"
  };
}

abstract class _StringFr {
  static const map = {
    "mnuFavAdd": "Ajouter un favori",
    "mnuFavUpdate": "Modifier un favori",
    "mnuAbout": "A propos / Contact",
    "mnuArts": "Articles",
    "mnuAltLanguage": "Langue de l'interface utilisateur",
    "bible": "Bible",
    "mnuBiblePreferred": "Bible préférée",
    "mnuBibleToDisplay": "Bibles à afficher",
    "mnuBooks": "Livres",
    "mnuFavorites": "Favoris",
    "mnuChapters": "Chapitres",
    "mnuClose": "Fermer",
    "mnuDelete": "Supprimer",
    "mnuHelp": "Aide",
    "mnuHelpEmo": "- Vous pouvez trouver d’autres emoji dans votre clavier virtuel ou sur Internet\n\n- Les emoji peuvent être affichés différemment sur plusieurs appareils et outils",
    "mnuHelpCustomBible": "- \"Auto\" signifie que la couleur d'écriture et/ou couleur de fond sont celles du thème\n\n- L'avantage est qu'en changeant de thème, ces couleurs seront automatiquement adaptées (dynamiques)\n\n- \"Auto\" est particulièrement utile pour la couleur de fond. Cela vous évitera d'avoir, par exemple, un texte écrit en bleu sur un fond noir alors que le thème est blanc!",
    "mnuHistory": "Historique",
    "mnuInviteFriend": "Inviter un ami",
    "mnuPrbls": "Paraboles",
    "mnuSearch": "Chercher",
    "mnuSettings": "Paramètres",
    "mnuSettingsBibles": "Bible",
    "mnuSettingsFonts": "Police (défaut)",
    "mnuSettingsFontSize": "Taille de la police (défaut)",
    "mnuSettingsLayoutDynamicHeader" : "--- Disposition (grille) ---",
    "mnuSettingsLayoutDynamicSub": "Combien de colonnes?",
    "mnuSettingsStyleHighlightSearch": "Style de la recherche",
    "mnuSettingsCustomBibles": "Personnaliser les Bibles",
    "mnuSettingsCustomBibleVFontFamily": "Police",
    "mnuSettingsCustomBibleVFontSize": "Taille de la police",
    "mnuSettingsCustomBibleVTextFgColor": "Verset: couleur d'écriture",
    "mnuSettingsCustomBibleVTextBgColor": "Verset: couleur de fond",
    "mnuSettingsCustomBibleVNumberFgColor": "Nombre: couleur d'écriture",
    "mnuSettingsCustomBibleVNumberBgColor": "Nombre: couleur de fond",
    "mnuSettingsCustomBibleVCrFgColor": "Référence: couleur d'écriture",
    "mnuSettingsCustomBibleVCrBgColor": "Référence: couleur de fond",
    "mnuSettingsCustomBibleResetWithDefaults": "Réinitialiser (valeurs par défaut)",
    "mnuSettingsCustomBibleResetWithBible": "Réinitialiser (valeurs d'une Bible)",
    "mnuSettingsCustomBibleCopy": "Copier",
    "mnuSettingsCustomBiblePaste": "Coller",
    "mnuThemes": "Thème",
    "mnuOpen": "Ouvrir",
    "mnuOpenChapter": "Chapitre",
    "mnuOpenCR": "Références croisées",
    "mnuClipboard": "Presse-papier",
    "mnuClipboardClear": "Vider",
    "mnuClipboardAddVerse": "Ajouter le verset",
    "mnuClipboardAddChapter": "Ajouter le chapitre",
    "mnuSave": "Sauver",
    "mnuShare": "Partager",
    "favAll": "[TOUT]",
    "favOrderByBook": "Par livre",
    "favOrderByDate": "Par date",
    "hintFavDesc": "Description",
    "hintSearchBible": "Chercher dans {0}",
    "toastAdded": "Ajouté",
    "toastDeleted": "Supprimé",
    "toastWarnKJV2000Limit500": "No more than 500 verses © KJ2K, RVA1989, LND1991, IRV2017, NJB1973, CUVS, NTB2001, BCL2016, RUC1928, BW1975",
    "toastWarnKJV2000LimitFullBook": "No full book © KJ2K, RVA1989, LND1991, IRV2017, NJB1973, CUVS, NTB2001, BCL2016, RUC1928, BW1975",
    "toastWarnNoResultFound": "Aucun résultat trouvé!",
    "validatorSearchQueryLimit": "Min. $searchQueryLimit caractères",
    "aboutContactMe": "Si vous avez des problèmes ou des suggestions, s\'il vous plaît contactez moi.",
    "copiedClipboard": "Copié dans le presse-papier",
    "emptyClipboard": "Vide!",
    "cr": "Références croisées",
    "crShort": "RC",
    "inviteFriendClipboardMsg1":
        "Bonjour,\n\nJe t\'invite à installer cette application de la Bible (Bible Multi The Life), disponible sur App Store.\n\n",
    "inviteFriendClipboardMsg2": "Gloire à Dieu.",
    "inviteFriendPageMsg":
        "Le texte a été copié dans le presse-papier.\n\nVous pouvez à présent, le coller dans des emails, forums, Facebook, SMS...\n\nGloire à Dieu.",
    "ART_APP_HELP_CONTENT": """
<i>Salut,</i> je suis un programmeur chrétien et j\'ai fait cette application avec mon coeur pour servir le Seigneur.<br><br> La fin des temps est proche avec les tribulations, l\'antéchrist, Jésus et le jugement final.<br><br>
<i>Whattt! Are you serious?</i> Oui, je suis sérieux, les signes sont partout.<br><br> Cette application donnant la vraie parole de Dieu peut vous aider à être fort durant cette période quand le diable essayera de vous tromper plus que jamais et d\'être prêt quand le Christ reviendra.<br><br>
Rappelez-vous que le diable connaît les Écritures.<br><br> Je n\'ai ajouté que de bonnes bibles originales (pas de re-édition quand c\'est possible) en plusieurs langues, que je peux comprendre, pour vous aider à trouver Jésus parce qu\'il est le seul chemin vers
notre Père.<br><br> Ne restez pas dans les ténèbres petits enfants de Dieu.<br><br> Soyez bon avec vos frères et soeurs, partagez la parole de Dieu, l\'amour, priez, repentez-vous, ayez la foi, la joie,
suivez les commandements, arrêtez vos péchés et semez abondamment.<br><br> Suivons Jésus, le bon Berger, notre unique sauveur&#8230; Que Dieu vous bénisse. Amen.<br><br>
<br>
<b>Jean 14.6:</b> Jésus lui dit: Je suis le chemin, la vérité, et la vie. Nul ne vient au Père que par moi.
<br>
<br>
<b>Jean 15.12:</b> C\'est ici mon commandement: Aimez-vous les uns les autres, comme je vous ai aimés.
<br>
<br>
<b>Jean 10.11:</b> Je suis le bon berger. Le bon berger donne sa vie pour ses brebis.
<br>
<br>
<b>Psaumes 23.3:</b> Il restaure mon âme, Il me conduit dans les sentiers de la justice, A cause de son nom.<br>
<br>
<b>Psaumes 23.4:</b> Quand je marche dans la vallée de l\'ombre de la mort, Je ne crains aucun mal, car tu es avec moi: ta houlette et ton bâton me rassurent.<br>
<br>
<b>Psaumes 23.5:</b> Tu dresses devant moi une table, En face de mes adversaires; Tu oins d\'huile ma tête, Et ma coupe déborde.<br>
<br>
<b>Psaumes 23.6:</b> Oui, le bonheur et la grâce m\'accompagneront Tous les jours de ma vie, Et j\'habiterai dans la maison de l\'Éternel Jusqu\'à la fin de mes jours.
<br>
<br>
<b>1Corinthiens 3.16:</b> Ne savez-vous pas que vous êtes le temple de Dieu, et que l\'Esprit de Dieu habite en vous?<br>
<br>
<b>1Corinthiens 3.17:</b> Si quelqu\'un détruit le temple de Dieu, Dieu le détruira; car le temple de Dieu est saint, et c\'est ce que vous êtes.<br>
<br><br>
<H>Chercher dans la Bible</H>
Syntaxe: &lt;livre&gt; <br>
Syntaxe: &lt;livre&gt; &lt;chapitre&gt; <br>
Syntaxe: &lt;livre&gt; &lt;chapitre&gt; &lt;verset&gt; <br>
Syntaxe: &lt;livre&gt; &lt;chapitre&gt; &lt;verset de&gt; &lt;verset à&gt;<br>
Syntaxe: &lt;livre&gt; &lt;expression&gt; <br>
Syntaxe: &lt;livre&gt; &lt;chapitre&gt; &lt;expression&gt; <br>
Syntaxe: &lt;expression&gt;<br>
<br><br>
<b>Exemples:</b><br>
• Genèse 1<br>
Donne le chapitre 1 de la Genèse<br>
<br>
• Genèse 1 5<br>
Donne le verset 1.5 de la Genèse :)<br>
<br>
• Genèse feu<br>
Cherche dans la Genèse toutes les occurrences de \'feu\'<br>
<br>
• 1 6 seigneur<br>
Cherche dans la Genèse chapitre 6 toutes les occurences de \'seigneur\'<br>
<br>
• Abraham<br>
Cherche \'Abraham\' dans toute la bible<br>
<br>
• Apocalypse cheval<br>
Cherche les chevaux de l\'Apocalypse :)<br>
<br>
• 66 babylone<br>
Cherche \'babylone\' dans le livre 66 (Apocalypse)<br>
<br>
• pierre%église<br>
cherche dans l\'ordre, \'pierre\' et \'église\' dans toute la bible<br>
<br><br>
<b>Remarques:</b><br>
- Les noms des livres sont sensibles à la casse, donc sélectionnez les dans la liste<br>
- Ajoutez des accents dans vos mots si ils en ont:<br>
Exemple: j<u>é</u>zabel (français), jezebel (anglais), <u>É</u>lie (français), Elijah (anglais), Elia (italien), El<u>í</u>as (espagnol)<br>
<br><br>
<H>Accord</H>
Cette application restera totalement gratuite, sans publicité, redirections, hors connexion et ne sera pas vendue. Ce n\'est pas une démo mais une version complète.<br>
<br><br>
<H>Contact</H>
Si vous avez des problèmes ou des suggestions, contactez-moi (menu A Propos).<br>
<br><br>
""",
    "fav0": "Internal",
    "fav1": "Favori",
    "fav2": "Lecture",
    "fav10": "Amour",
    "fav20": "Top 100",
    "fav23": "A lire",
    "fav30": "A étudier",
    "fav40": "A résoudre",
    "fav50": "Fait",
    "fav60": "Exclamation",
    "fav70": "Question",
    "fav80": "Important",
    "fav90": "Danger",
    "fav100": "Mort",
    "fav105": "Vie",
    "fav110": "Prophétie"
  };
}

abstract class _StringEs {
  static const map = {
    "mnuFavAdd": "Añadir un favorito",
    "mnuFavUpdate": "Modificar un favorito",
    "mnuAbout": "A propósito / Contacto",
    "mnuArts": "Artículos",
    "mnuAltLanguage": "Idioma de la interfaz de usuario",
    "bible": "Biblia",
    "mnuBiblePreferred": "Biblia preferida", //lengua
    "mnuBibleToDisplay": "Biblias a mostrar",
    "mnuBooks": "Libros",
    "mnuFavorites": "Favoritos",
    "mnuChapters": "Capítulos",
    "mnuClose": "Cerrar",
    "mnuDelete": "Eliminar",
    "mnuHelp": "Ayuda",
    "mnuHelpEmo": "- Usted puede encontrar otros emojis en su teclado virtual o en Internet\n\n- El Emoji se puede mostrar de manera diferente en varios dispositivos y herramientas",
    "mnuHelpCustomBible": "- \"Auto\" significa que el color de escritura y/o color de fondo son los del tema\n\n- La ventaja es que al cambiar de tema, estos colores se adaptarán automáticamente (dinámico)\n\n- \"Auto\" es particularmente útil para el color de fondo. Esto evitará que tengas, por ejemplo, un texto escrito en azul sobre un fondo negro cuando el tema es blanco!",
    "mnuHistory": "Historial",
    "mnuInviteFriend": "Invitar a un amigo",
    "mnuPrbls": "Parábolas",
    "mnuSearch": "Buscar",
    "mnuSettings": "Parámetros",
    "mnuSettingsBibles": "Biblia",
    "mnuSettingsFonts": "Tipo de fuente (defecto)",
    "mnuSettingsFontSize": "Tamaño de fuente (defecto)",
    "mnuSettingsLayoutDynamicHeader" : "--- Diseño (cuadrícula) ---",
    "mnuSettingsLayoutDynamicSub": "¿Quanto columnas?",
    "mnuSettingsStyleHighlightSearch": "Estilo de búsqueda",
    "mnuSettingsCustomBibles": "Personalizar las Biblias",
    "mnuSettingsCustomBibleVFontFamily": "Tipo de fuente",
    "mnuSettingsCustomBibleVFontSize": "Tamaño de fuente",
    "mnuSettingsCustomBibleVTextFgColor": "Versículo: color de escritura",
    "mnuSettingsCustomBibleVTextBgColor": "Versículo: color de fondo",
    "mnuSettingsCustomBibleVNumberFgColor": "Número: color de escritura",
    "mnuSettingsCustomBibleVNumberBgColor": "Número: color de fondo",
    "mnuSettingsCustomBibleVCrFgColor": "Referencia: color de escritura",
    "mnuSettingsCustomBibleVCrBgColor": "Referencia: color de fondo",
    "mnuSettingsCustomBibleResetWithDefaults": "Reiniciar (valores por defecto)",
    "mnuSettingsCustomBibleResetWithBible": "Reiniciar (valores de una Biblia)",
    "mnuSettingsCustomBibleCopy": "Copiar",
    "mnuSettingsCustomBiblePaste": "Pegar",
    "mnuThemes": "Tema",
    "mnuOpen": "Abrir",
    "mnuOpenChapter": "Capítulo",
    "mnuOpenCR": "Referencias cruzadas",
    "mnuClipboard": "Portapapeles",
    "mnuClipboardClear": "Vaciar",
    "mnuClipboardAddVerse": "Añadir el versículo",
    "mnuClipboardAddChapter": "Añadir el capítulo",
    "mnuSave": "Salvarse",
    "mnuShare": "Compartir",
    "favAll": "[TODOS]",
    "favOrderByBook": "Por libro",
    "favOrderByDate": " Por fecha",
    "hintFavDesc": "Descripción",
    "hintSearchBible": "Buscar en {0}",
    "toastAdded": "Añadido",
    "toastDeleted": "Suprimido",
    "toastWarnKJV2000Limit500": "No more than 500 verses © KJ2K, RVA1989, LND1991, IRV2017, NJB1973, CUVS, NTB2001, BCL2016, RUC1928, BW1975",
    "toastWarnKJV2000LimitFullBook": "No full book © KJ2K, RVA1989, LND1991, IRV2017, NJB1973, CUVS, NTB2001, BCL2016, RUC1928, BW1975",
    "toastWarnNoResultFound": "¡Ningún resultado encontrado!",
    "validatorSearchQueryLimit": "Mín. $searchQueryLimit caracteres",
    "aboutContactMe": "Si usted tiene problemas o sugerencias, póngase en contacto conmigo.",
    "copiedClipboard": "Copiado al portapapeles",
    "emptyClipboard": "¡Vacío!",
    "cr": "Referencias cruzadas",
    "crShort": "RC",
    "inviteFriendClipboardMsg1":
        "Hola,\n\nte invito a instalar esta aplicación de la Bibli (Bible Multi The Life), disponible en App Store.\n\n",
    "inviteFriendClipboardMsg2": "Gloria a Dios.",
    "inviteFriendPageMsg":
        "El texto ha sido copiado al portapapeles.\n\nAhora es posible, pegarlo en emails, forums, Facebook, SMS...\n\nGloria a Dios.",
    "ART_APP_HELP_CONTENT": """
<i>Ola,</i> soy un programador cristiano e hice esta aplicación con mi corazón para servir al Señor.<br><br> El fin de los tiempos es próximo con las tribulaciones, el antéchrist, el Jesús y el juicio final.<br><br>
<i>Whattt! Are you serious?</i> Sí, soy serio, los signos están por todas partes.<br><br> Esta aplicación que concede la verdadera palabra(voz) de Dios puede ayudarle a ser fuerte durante este período cuando el diablo tratará de engañarle más que nunca y de estar listo cuando el Cristo volverá.<br><br> Recuerde que el diablo conoce las Escrituras. Añadí sólo la buenas Biblia originales (ninguna re-edición cuando es posible) en varias lenguas, que puedo comprender, para ayudarse a encontrar a Jesús porque es el solo camino hacia nuestro Padre.<br><br>
No quede en las tinieblas chicos de Dios.<br><br> Sea bueno con sus hermanos y hermanas, comparte la palabra de Dios, el amor, reugue, arrepiéntase, tenga la fe, la alegría, siga los mandos, pare(detenga) sus pecados y siembre abundantemente.<br><br> Sigamos a Jesús, el buen Pastor, nuestro salvador único&#8230; Qué Dios le bendiga. Amen.<br><br>
<br>
<b>Juan 14.6:</b> Jesús le dice: Yo soy el camino, y la verdad, y la vida: nadie viene al Padre, sino por mí.
<br>
<br>
<b>Juan 15.12:</b> Este es mi mandamiento: Que os améis los unos á los otros, como yo os he amado.
<br>
<br>
<b>Juan 10.11:</b> Yo soy el buen pastor: el buen pastor su vida da por las ovejas.
<br>
<br>
<b>Salmos 23.3:</b> Confortará mi alma; Guiárame por sendas de justicia por amor de su nombre.<br>
<br>
<b>Salmos 23.4:</b> Aunque ande en valle de sombra de muerte, No temeré mal alguno; porque tú estarás conmigo: Tu vara y tu cayado me infundirán aliento.<br>
<br>
<b>Salmos 23.5:</b> Aderezarás mesa delante de mí, en presencia de mis angustiadores: Ungiste mi cabeza con aceite: mi copa está rebosando.<br>
<br>
<b>Salmos 23.6:</b> Ciertamente el bien y la misericordia me seguirán todos los días de mi vida: Y en la casa de Jehová moraré por largos días.
<br>
<br>
<b>1Corintios 3.16:</b> ¿No sabéis que sois templo de Dios, y que el Espíritu de Dios mora en vosotros?<br>
<br>
<b>1Corintios 3.17:</b> Si alguno violare el templo de Dios, Dios destruirá al tal: porque el templo de Dios, el cual sois vosotros, santo es.<br>
<br><br>
<H>Buscar en la Biblia</H>
Sintaxis: &lt;libro&gt;<br>
Sintaxis: &lt;libro&gt; &lt;capítulo&gt;<br>
Sintaxis: &lt;libro&gt; &lt;capítulo&gt; &lt;versículo&gt;<br>
Sintaxis: &lt;libro&gt; &lt;capítulo&gt; &lt;versículo de&gt; &lt;versículo a&gt;<br>
Sintaxis: &lt;libro&gt; &lt;expresión&gt;<br>
Sintaxis: &lt;libro&gt; &lt;capítulo&gt; &lt;expresión&gt;<br>
Sintaxis: &lt;expresión&gt;<br>
<br><br>
<b>Ejemplos:</b><br>
• Génesis 1<br>
Dar el capítulo 1 del Génesis<br>
<br>
• Génesis 1 5<br>
Dar el versículo 1.5 de Génesis :)<br>
<br>
• Génesis \'fuego\'<br>
Buscar en Génesis todas las apariciones de \'fuego\'<br>
<br>
• Génesis 6 señor<br>
Buscar en Génesis capítulo 6 todas las apariciones de \'señor\'<br>
<br>
• Abram<br>
Buscando \'Abram\' en la Biblia<br>
<br>
• Apocalipsis cabal<br>
Buscando los caballos del Apocalipsis :)<br>
<br>
• 66 babilonia<br>
Buscando \'babilonia\' en el libro 66 (Apocalipsis)<br>
<br>
• pedro%chiesa<br>
Busca en la orden, \'pedro\' y \'iglesia\' en toda la Biblia<br>
<br><br>
<b>Comentarios:</b><br>
- Los nombres de los libros son fragiles, pues seleccione en la lista<br>
- Añada acentos en sus palabras si lo tienen:<br>
Ejemplo: j<u>é</u>zabel (francés), jezebel (inglés)<br>
<br><br>
<H>Acuerdo</H>
Esta aplicación quedará totalmente gratuita, sin publicidad, redirecciones, fuera de conexión y no será vendida. No es un demo sino una versión completa.<br>
<br><br>
<H>Contacto</H>
Si usted tiene problemas o sugerencias, póngase en contacto conmigo (menú A propósito).<br>
<br><br>    
""",
    "fav0": "Internal",
    "fav1": "Favorito",
    "fav2": "Lectura",
    "fav10": "Amor",
    "fav20": "Top 100",
    "fav23": "A leer",
    "fav30": "A estudiar",
    "fav40": "A resolver",
    "fav50": "Hecho",
    "fav60": "Exclamación",
    "fav70": "Pregunta",
    "fav80": "Importante",
    "fav90": "Peligro",
    "fav100": "Muerte",
    "fav105": "Vida",
    "fav110": "Profecía"
  };
}

abstract class _StringPt {
  static const map = {
    "mnuFavAdd": "Adicionar um favorito",
    "mnuFavUpdate": "Modificar um favorito",
    "mnuAbout": "Sobre / Contato",
    "mnuArts": "Artigos",
    "mnuAltLanguage": "Idioma da interface do usuário",
    "bible": "Bíblia",
    "mnuBiblePreferred": "Bíblia favorita",
    "mnuBibleToDisplay": "Bíblias para mostrar",
    "mnuBooks": "Livros",
    "mnuFavorites": "Favoritos",
    "mnuChapters": "Capítulos",
    "mnuClose": "Fechar",
    "mnuDelete": "Excluir",
    "mnuHelp": "Ajuda",
    "mnuHelpEmo": "- Pode encontrar outros emojis no seu teclado virtual ou na Internet\n\n- O Emoji pode ser exibido de forma diferente em vários dispositivos e ferramentas",
    "mnuHelpCustomBible": "- \"Auto\" significa que a cor de escrita e/ou cor de fundo são as do tema\n\n- A vantagem é que ao mudar de tema, estas cores serão automaticamente adaptadas (dinâmicas)\n\n- \"Auto\" é particularmente útil para a cor de fundo. Isso vai evitar que você tenha, por exemplo, um texto escrito em azul sobre um fundo preto quando o tema é branco!",
    "mnuHistory": "Histórico",
    "mnuInviteFriend": "Convidar um amigo",
    "mnuPrbls": "Parábolas",
    "mnuSearch": "Buscar",
    "mnuSettings": "Configurações",
    "mnuSettingsBibles": "Bíblia",
    "mnuSettingsFonts": "Fonte (padrão)",
    "mnuSettingsFontSize": "Tamanho da fonte (padrão)",
    "mnuSettingsLayoutDynamicHeader" : "--- Layout (grade) ---",
    "mnuSettingsLayoutDynamicSub": "Quantas colunas?",
    "mnuSettingsStyleHighlightSearch": "Tipo de pesquisa",
    "mnuSettingsCustomBibles": "Personalizar as Bíblias",
    "mnuSettingsCustomBibleVFontFamily": "Fonte",
    "mnuSettingsCustomBibleVFontSize": "Tamanho da fonte",
    "mnuSettingsCustomBibleVTextFgColor": "Versículo: cor da escrita",
    "mnuSettingsCustomBibleVTextBgColor": "Versículo: cor de fundo",
    "mnuSettingsCustomBibleVNumberFgColor": "Número: cor da escrita",
    "mnuSettingsCustomBibleVNumberBgColor": "Número: cor de fundo",
    "mnuSettingsCustomBibleVCrFgColor": "Referência: cor da escrita",
    "mnuSettingsCustomBibleVCrBgColor": "Referência: cor de fundo",
    "mnuSettingsCustomBibleResetWithDefaults": "Reiniciar (valores por defeito)",
    "mnuSettingsCustomBibleResetWithBible": "Reiniciar (valores de uma Bíblia)",
    "mnuSettingsCustomBibleCopy": "Copiar",
    "mnuSettingsCustomBiblePaste": "Colar",
    "mnuThemes": "Tema",
    "mnuOpen": "Abrir",
    "mnuOpenChapter": "Capítulo",
    "mnuOpenCR": "Referências cruzadas",
    "mnuClipboard": "Clipboard",
    "mnuClipboardClear": "Limpar tudo",
    "mnuClipboardAddVerse": "Adicionar o versículo",
    "mnuClipboardAddChapter": "Adicionar o capítulo",
    "mnuSave": "Salvar",
    "mnuShare": "Compartilhar",
    "favAll": "[TUDO]",
    "favOrderByBook": "Por livro",
    "favOrderByDate": "Por data",
    "hintFavDesc": "Descrição",
    "hintSearchBible": "Buscar em {0}",
    "toastAdded": "Adicionado",
    "toastDeleted": "Excluído",
    "toastWarnKJV2000Limit500": "No more than 500 verses © KJ2K, RVA1989, LND1991, IRV2017, NJB1973, CUVS, NTB2001, BCL2016, RUC1928, BW1975",
    "toastWarnKJV2000LimitFullBook": "No full book © KJ2K, RVA1989, LND1991, IRV2017, NJB1973, CUVS, NTB2001, BCL2016, RUC1928, BW1975",
    "toastWarnNoResultFound": "Nenhum resultado encontrado!",
    "validatorSearchQueryLimit": "Mín. $searchQueryLimit caracteres",
    "aboutContactMe": "Se você tiver algum problema ou sugestão, entre em contato comigo.",
    "copiedClipboard": "Copiado ao Clipboard",
    "emptyClipboard": "Vazio!",
    "cr": "Referências cruzadas",
    "crShort": "RC",
    "inviteFriendClipboardMsg1":
        "Olá,\n\nConvido você a instalar este app da Bíblia (Bible Multi The Life), disponível em App Store.\n\n",
    "inviteFriendClipboardMsg2": "Glória a Deus.",
    "inviteFriendPageMsg":
        "O texto foi copiado ao Clipboard.\n\nAgora você pode colar conteúdo para emails, forums, Facebook, SMS...\n\nGlória a Deus.",
    "ART_APP_HELP_CONTENT": """
<i>Olá,</i> eu sou um programador cristão e eu criei este app com meu coração para servir ao Senhor.<br><br> O fim dos tempos está próximo, com tribulações, anticristo, Jesus e o julgamento final.<br><br>
<i>Whattt! Are you serious?</i> Sim, estou falando sério, os sinais estão por toda parte.<br><br> Este app ao entregar a verdadeira palavra de Deus pode ajudá-lo a ser mais forte durante este tempo, quando o diabo tentará enganá-lo mais do que nunca e estar pronto quando Cristo retornar.<br><br> Lembre-se de que o diabo conhece as escrituras.<br><br> Adicionei apenas Bíblias bons, originais (sem re-edições, quando for possivel) em várias línguas, que eu posso entender, para ajudar lhe a encontrar o Jesus porque ele é o único caminho de nosso Pai.<br><br> Filhos de Deus, não fiquem na escuridão.<br><br> Sejam bons com seus irmãos e irmãs, compartilhem a palavra de Deus, amem, orem, arrependam-se, tenham fé, alegria, sigam os mandamentos, deixem de pecar  e semeiem abundantemente.<br><br> Vamos a seguir o Jesus, o bom pastor, nosso único salvador&#8230; Deus te abençoe. Amém.<br><br>
<br>
<b>João 14.6:</b> Disse-lhe Jesus: Eu sou o caminho, e a verdade e a vida; ninguém vem ao Pai, senão por mim.
<br>
<br>
<b>João 15.12:</b> O meu mandamento é este: Que vos ameis uns aos outros, assim como eu vos amei.
<br>
<br>
<b>João 10.11:</b> Eu sou o bom Pastor; o bom Pastor dá a sua vida pelas ovelhas.
<br>
<br>
<b>Salmos 23.3:</b> Refrigera a minha alma; guia-me pelas veredas da justiça, por amor do seu nome.<br>
<br>
<b>Salmos 23.4:</b> Ainda que eu andasse pelo vale da sombra da morte, não temeria mal algum, porque tu estás comigo; a tua vara e o teu cajado me consolam.<br>
<br>
<b>Salmos 23.5:</b> Preparas uma mesa perante mim na presença dos meus inimigos, unges a minha cabeça com óleo, o meu cálice transborda.<br>
<br>
<b>Salmos 23.6:</b> Certamente que a bondade e a misericórdia me seguirão todos os dias da minha vida; e habitarei na casa do SENHOR por longos dias.
<br>
<br>
<b>1Corintios 3.16:</b> Não sabeis vós que sois o templo de Deus e que o Espírito de Deus habita em vós?<br>
<br>
<b>1Corintios 3.17:</b> Se alguém destruir o templo de Deus, Deus o destruirá; porque o templo de Deus, que sois vós, é santo.<br>
<br><br>
<H>Procurar em a Bíblia</H>
Sintaxe: &lt;livro&gt;<br>
Sintaxe: &lt;livro&gt; &lt;capítulo&gt;<br>
Sintaxe: &lt;livro&gt; &lt;capítulo&gt; &lt;versículo&gt;<br>
Sintaxe: &lt;livro&gt; &lt;capítulo&gt; &lt;versículo de&gt; &lt;versículo a&gt;<br>
Sintaxe: &lt;livro&gt; &lt;expressão&gt;<br>
Sintaxe: &lt;livro&gt; &lt;capítulo&gt; &lt;expressão&gt;<br>
Sintaxe: &lt;expressão&gt;<br>
<br><br>
<b>Exemplos:</b><br>
• Génesis 1<br>
Retorna o capítulo 1 del Génesis<br>
<br>
• Génesis 1 5<br>
Retorna o versículo 1.5 de Génesis :)<br>
<br>
• Génesis \'fogo\'<br>
Procura em Génesis todas as ocorrências de \'fogo\'<br>
<br>
• Génesis 6 senhor<br>
Procura em Génesis, capítulo 6, todas as ocorrências de \'senhor\'<br>
<br>
• Abraão<br>
Procura por \'Abraão\' em a Bíblia<br>
<br>
• Apocalipse caval<br>
Procura por os cavalos de Apocalipse :)<br>
<br>
• 66 babilônia<br>
Procura por \'babilônia\' em o livro 66 (Apocalipse)<br>
<br>
• pedro%igreja<br>
Procura em ordem definido por \'pedro\' e \'igreja\' em toda a Bíblia<br>
<br><br>
<b>Comentarios:</b><br>
- Os nomes dos livros são sensíveis a maiúsculas e minúsculas, portanto, selecione-os da lista<br>
- Coloque acentos em palavras se eles tiverem:<br>
Ejemplo: j<u>é</u>zabel (francés), jezebel (inglés)<br>
<br><br>
<H>Acordo</H>
Este app permanecerá totalmente livre, sem anúncios, redirecionamentos, totalmente offline e não será vendido. Não é uma versão demo é uma versão completa.<br>
<br><br>
<H>Contacto</H>
Se você tiver algum problema ou sugestão, entre em contato comigo (Sobre no menu).<br>
<br><br>
""",
    "fav0": "Internal",
    "fav1": "Favorito",
    "fav2": "Leitura",
    "fav10": "Amor",
    "fav20": "Top 100",
    "fav23": "Para ler",
    "fav30": "Para estudar",
    "fav40": "Para resolver",
    "fav50": "Concluído",
    "fav60": "Exclamação",
    "fav70": "Pergunta",
    "fav80": "Importante",
    "fav90": "Perigo",
    "fav100": "Morte",
    "fav105": "Vida",
    "fav110": "Profecia"
  };
}

abstract class _StringIt {
  static const map = {
    "mnuFavAdd": "Aggiungere un favorito",
    "mnuFavUpdate": "Modificare un favorito",
    "mnuAbout": "A proposito / Contatto",
    "mnuArts": "Articoli",
    "mnuAltLanguage": "Lingua dell'interfaccia utente",
    "bible": "Bibbia",
    "mnuBiblePreferred": "Bibbia preferita", //Lingua
    "mnuBibleToDisplay": "Bibbie da visualizzare",
    "mnuBooks": "Libri",
    "mnuFavorites": "Favoriti",
    "mnuChapters": "Capitoli",
    "mnuClose": "Chiudere",
    "mnuDelete": "Eliminare",
    "mnuHelp": "Aiuto",
    "mnuHelpEmo": "- Puoi trovare altre emoji nella tua tastiera virtuale o su Internet\n\n- Le emoji possono essere visualizzate in modo diverso su diversi dispositivi e strumenti",
    "mnuHelpCustomBible": "- \"Auto\" significa che il colore di scrittura e/o sfondo sono quelli del tema\n\n- Il vantaggio è che cambiando il tema, questi colori saranno automaticamente adattati (dinamici)\n\n- \"Auto\" è particolarmente utile per il colore di sfondo. Questo vi eviterà di avere ad esempio un testo scritto in blu su uno sfondo nero mentre il tema è bianco!",
    "mnuHistory": "Cronologia",
    "mnuInviteFriend": "Invitare un amico",
    "mnuPrbls": "Parabole",
    "mnuSearch": "Cercare",
    "mnuSettings": "Parametri",
    "mnuSettingsBibles": "Bibbia",
    "mnuSettingsFonts": "Stile carattere (predefinito)",
    "mnuSettingsFontSize": "Dimensione carattere (predefinito)",
    "mnuSettingsLayoutDynamicHeader" : "--- Disposizione (griglia) ---",
    "mnuSettingsLayoutDynamicSub": "Quanto colonne?",
    "mnuSettingsStyleHighlightSearch": "Stile di ricerca",
    "mnuSettingsCustomBibles": "Personalizzare le Bibbie",
    "mnuSettingsCustomBibleVFontFamily": "Stile carattere",
    "mnuSettingsCustomBibleVFontSize": "Dimensione carattere",
    "mnuSettingsCustomBibleVTextFgColor": "Versetto: colore di scrittura",
    "mnuSettingsCustomBibleVTextBgColor": "Versetto: colore di sfondo",
    "mnuSettingsCustomBibleVNumberFgColor": "Numero: colore di scrittura",
    "mnuSettingsCustomBibleVNumberBgColor": "Numero: colore di sfondo",
    "mnuSettingsCustomBibleVCrFgColor": "Riferimento: colore di scrittura",
    "mnuSettingsCustomBibleVCrBgColor": "Riferimento: colore di sfondo",
    "mnuSettingsCustomBibleResetWithDefaults": "Resettare (valori predefiniti)",
    "mnuSettingsCustomBibleResetWithBible": "Resettare (valori di una Bibbia)",
    "mnuSettingsCustomBibleCopy": "Copiare",
    "mnuSettingsCustomBiblePaste": "Incollare",
    "mnuThemes": "Tema",
    "mnuOpen": "Aprire",
    "mnuOpenChapter": "Capitolo",
    "mnuOpenCR": "Riferimenti incrociati",
    "mnuClipboard": "Clipboard",
    "mnuClipboardClear": "Svuotare",
    "mnuClipboardAddVerse": "Aggiungere il versetto",
    "mnuClipboardAddChapter": "Aggiungere il capitolo",
    "mnuSave": "Salvare",
    "mnuShare": "Condividere",
    "favAll": "[TUTTO]",
    "favOrderByBook": "Per libro",
    "favOrderByDate": "Per data",
    "hintFavDesc": "Descrizione",
    "hintSearchBible": "Cercare in {0}",
    "toastAdded": "Aggiunto",
    "toastDeleted": "Eliminato",
    "toastWarnKJV2000Limit500": "No more than 500 verses © KJ2K, RVA1989, LND1991, IRV2017, NJB1973, CUVS, NTB2001, BCL2016, RUC1928, BW1975",
    "toastWarnKJV2000LimitFullBook": "No full book © KJ2K, RVA1989, LND1991, IRV2017, NJB1973, CUVS, NTB2001, BCL2016, RUC1928, BW1975",
    "toastWarnNoResultFound": "Nessun risultato trovato!",
    "validatorSearchQueryLimit": "Min. $searchQueryLimit caratteri",
    "aboutContactMe": "Se avete dei problemi o delle suggestioni, contattatemi.",
    "copiedClipboard": "Copiato negli appunti",
    "emptyClipboard": "Vuoto!",
    "cr": "Riferimenti incrociati",
    "crShort": "RI",
    "inviteFriendClipboardMsg1":
        "Ciao,\n\nTi invito a installare questa applicazione della Bibbia (Bible Multi The Life), disponibile su App Store.\n\n",
    "inviteFriendClipboardMsg2": "Gloria a Dio.",
    "inviteFriendPageMsg":
        "Il testo è stato copiato negli appunti.\n\nOra è possibile, incollarlo in emails, forums, Facebook, SMS...\n\nGloria a Dio.",
    "ART_APP_HELP_CONTENT": """
<i>Ciao,</i> sono un programmatore cristiano e ho fatto questa applicazione col il mio cuore per servire il Signore.<br><br> La fine dei tempi è vicini con le tribolazioni, l\'anticristo, Gesù ed il giudizio finale.<br><br>
<i>Whattt! Are you serious?</i> Sì, sono serio, i segni sono dovunque.<br><br> Questa applicazione che dà la vera parola di Dio può aiutarvi ad essere forti durante questo periodo quando il diavolo provera di ingannarvi più che mai e di essere pronto quando il Cristo ritornerà.<br><br>
Ricordatevi che il diavolo conosce le Scritture.<br><br> Ho aggiunto che le buone Bibbie originali (non re-edizione quando è possibile) in diverse lingue, che posso capire, per aiutarvi a trovare Gesù, perché è la sola strada verso nostro Padre.<br><br>
Non restate nelle tenebre piccoli bambini di Dio.<br><br> Siate buono coi vostri fratelli e sorelle, condividete la parola di Dio, l\'amore, pregate, pentitesivi, abbiate la fede, la gioia, segua i comandi,
fermate i vostri peccati e seminate abbondantemente.<br><br> Seguiamo Gesù, il buono Pastore, il nostro unico salvatore&#8230; Che Dio vi benedicesse. Amen.<br><br>
<br>
<b>Giovanni 14.6:</b> Gesù gli disse: Io son la via, la verità, e la vita; niuno viene al Padre se non per me.
<br>
<br>
<b>Giovanni 15.12:</b> Quest’è il mio comandamento: Che voi vi amiate gli uni gli altri, come io ho amati voi.
<br>
<br>
<b>Giovanni 10.11:</b> Io sono il buon pastore; il buon pastore mette la sua vita per le pecore.
<br>
<br>
<b>Salmi 23.3:</b> Egli mi ristora l’anima; Egli mi conduce per sentieri di giustizia, Per amor del suo Nome.<br>
<br>
<b>Salmi 23.4:</b> Avvegnachè io camminassi nella valle dell’ombra della morte, Io non temerei male alcuno; perciocchè tu sei meco; La tua bacchetta, e la tua verga mi consolano.<br>
<br>
<b>Salmi 23.5:</b> Tu apparecchi davanti a me la mensa, al cospetto de’ miei nemici; Tu ungi il mio capo con olio; la mia coppa trabocca.<br>
<br>
<b>Salmi 23.6:</b> Per certo, beni e benignità mi accompagneranno Tutti i giorni della mia vita; Ed io abiterò nella Casa del Signore Per lunghi giorni.
<br>
<br>
<b>1Corinzi 3.16:</b> Non sapete voi che siete il tempio di Dio, e che lo Spirito di Dio abita in voi?<br>
<br>
<b>1Corinzi 3.17:</b> Se alcuno guasta il tempio di Dio, Iddio guasterà lui; perciocchè il tempio del Signore è santo, il quale siete voi.<br>
<br><br>
<H>Cerca la Bibbia</H>
Sintassi: &lt;libro&gt;<br>
Sintassi: &lt;libro&gt; &lt;capitolo&gt;<br>
Sintassi: &lt;libro&gt; &lt;capitolo&gt; &lt;versetto&gt;<br>
Sintassi: &lt;libro&gt; &lt;capitolo&gt; &lt;versetto da&gt; &lt;versetto a&gt;<br>
Sintassi: &lt;libro&gt; &lt;expressione&gt;<br>
Sintassi: &lt;libro&gt; &lt;capitolo&gt; &lt;expressione&gt;<br>
Sintassi: &lt;expressione&gt;<br>
<br><br>
<b>Esempi:</b><br>
• Genesi 1<br>
Dare capitolo 1 della Genesi<br>
<br>
• Genesi 1 5<br>
Dare il versetto 1.5 della Genesi :)<br>
<br>
• Genesi fuoco<br>
Cerca in Genesi tutte le occorrenze di \'fuoco\'<br>
<br>
• Genesi 6 signore<br>
Cerca in Genesi capitolo 6 tutte le occorrenze di \'signore\'<br>
<br>
• Abramo<br>
Cerca \'Abramo\' in tutta la Bibbia<br>
<br>
• Apocalisse caval
Cerca i cavalli dell\'Apocalisse :)<br>
<br>
• 66 babilonia<br>
Cerca \'babilonia\' nel libro 66 (Apocalisse)<br>
<br>
• pietro%chiesa<br>
cerca nell\'ordine, \'pietro\' e \'chiesa\' in tutta la Bibbia<br>
<br><br>
<b>Commenti:</b><br>
- I nomi dei libri sono sensibili alla rompo, selezionate gli nell\'elenco dunque.<br>
- Aggiungete degli accenti nelle vostre parole se ne hanno:<br>
Esempio: j<u>é</u>zabel (francese), jezebel (inglese)<br>
<br><br>
<H>Accordo</H>
Questa applicazione resterà totalmente gratuita, senza pubblicità, redirezioni, fuori connessione e non sarà venduta. Non è un démo ma una versione completa.<br>
<br><br>
<H>Contatto</H>
Se avete dei problemi o delle suggestioni, contattatemi (menù A proposito).<br>
<br><br>    
""",
    "fav0": "Internal",
    "fav1": "Favorito",
    "fav2": "Lettura",
    "fav10": "Amore",
    "fav20": "Top 100",
    "fav23": "A leggere",
    "fav30": "A studiare",
    "fav40": "A risolvere",
    "fav50": "Fatto",
    "fav60": "Esclamazione",
    "fav70": "Domanda",
    "fav80": "Importante",
    "fav90": "Pericolo",
    "fav100": "Morte",
    "fav105": "Vita",
    "fav110": "Profezia"
  };
}

/* Not updated
abstract class _StringDe {
  static const map = {
    "mnuFavAdd": "Einen Favoriten hinzufügen",
    "mnuFavUpdate": "Einen Favoriten ändern",
    "mnuAbout": "Über / Kontakt",
    "mnuArts": "Artikeln",
    "mnuAltLanguage": "Alternative Sprache (Artikeln)",
    "bible": "Bibel",
    "mnuBiblePreferred": "Lieblingsbibel",
    "mnuBibleToDisplay": "Zu zeigende Bibeln",
    "mnuBooks": "Bücher",
    "mnuFavorites": "Favoriten",
    "mnuChapters": "Kapitel",
    "mnuClose": "Schließen",
    "mnuDelete": "Löschen",
    "mnuHelp": "Hilfe",
    "mnuHelpEmo": "- Sie können andere Emoji in Ihrer virtuellen Tastatur oder im Internet finden\n\n- Die Emoji können auf mehreren Geräten und Tools unterschiedlich angezeigt werden",
    "mnuHistory": "Geschichte",
    "mnuInviteFriend": "Lade einen Freund ein",
    "mnuPrbls": "Parabeln",
    "mnuSearch": "Suchen",
    "mnuSettings": "Einstellungen",
    "mnuSettingsBibles": "Bibel",
    "mnuSettingsFonts": "Schriftart",
    "mnuSettingsFontSize": "Schriftgröße",
    "mnuSettingsLayoutDynamic1": "Anzeige für 1 Sprache",
    "mnuSettingsLayoutDynamic2": "Anzeige für 2 Sprachen",
    "mnuSettingsLayoutDynamic3": "Anzeige für 3 Sprachen",
    "mnuSettingsLayoutDynamic4": "Anzeige für 4 Sprachen",
    "mnuSettingsLayoutDynamic5": "Anzeige für 5 Sprachen",
    "mnuSettingsLayoutDynamic6": "Anzeige für 6 Sprachen",
    "mnuSettingsLayoutDynamic7": "Anzeige für 7 Sprachen",
    "mnuSettingsLayoutDynamicSub": "Wie viele Spalten?",
    "mnuSettingStyleHighlightSearch": "Stil der Suche",
    "mnuThemes": "Thema",
    "mnuOpen": "Öffnen",
    "mnuOpenChapter": "Kapitel",
    "mnuOpenCR": "Querverweisen",
    "mnuClipboard": "Zwischenablage",
    "mnuClipboardClear": "Leer",
    "mnuClipboardAddVerse": "Vers hinzufügen",
    "mnuClipboardAddChapter": "Kapitel hinzufügen",
    "mnuSave": "Speichern",
    "mnuShare": "Teilen",
    "favAll": "[ALLE]",
    "favOrderByBook": "Nach Buch",
    "favOrderByDate": "Nach Datum",
    "hintFavDesc": "Beschreibung",
    "hintSearchBible": "In {0} suchen",
    "toastAdded": "Hinzugefügt",
    "toastDeleted": "Gelöscht",
    "toastWarnKJV2000Limit500": "No more than 500 verses © KJ2K, RVA1989, LND1991, IRV2017, NJB1973, CUVS, NTB2001, BCL2016, RUC1928, BW1975",
    "toastWarnKJV2000LimitFullBook": "No full book © KJ2K, RVA1989, LND1991, IRV2017, NJB1973, CUVS, NTB2001, BCL2016, RUC1928, BW1975",
    "toastWarnNoResultFound": "Keine Ergebnisse gefunden!",
    "validatorSearchQueryLimit": "Min. $searchQueryLimit Zeichen",
    "aboutContactMe": "Wenn Sie Probleme oder Vorschläge haben, kontaktieren Sie mich bitte.",
    "copiedClipboard": "In die Zwischenablage kopiert",
    "emptyClipboard": "Leer!",
    "cr": "Querverweisen",
    "crShort": "QV",
    "inviteFriendClipboardMsg1": "Hallo,\n\nIch lade Sie ein, diese Bibelanwendung (Bible Multi The Life) zu installieren. Diese ist im App Store erhältlich ist.\n\n",
    "inviteFriendClipboardMsg2": "Ehre sei Gott.",
    "inviteFriendPageMsg":
    "Der Text wurde in die Zwischenablage kopiert.\n\nSie können ihn jetzt in E-Mails, Foren, Facebook, SMS...\n\nEhre sei Gott.",
    "ART_APP_HELP_CONTENT": """
<i></i>Hallo,</i> Ich bin ein christlicher Programmierer und habe diese App mit meinem Herzen gemacht, um dem Herrn zu dienen.<br><br> Das Ende der Zeit ist nahe mit den Leiden, der Antichrist, Jesus und dem endgültigen Gericht.<br><br>
<i>Whattt! Are you serious?</i> Ja, ich meine es ernst, die Zeichen sind überall.<br><br> Diese Anwendung, die das wahre Wort Gottes gibt, kann Ihnen helfen, in dieser Zeit stark zu sein, in der der Teufel versucht, Sie mehr denn je zu täuschen und bereit sein, wenn Christus zurückkommt.<br><br>
Denken Sie daran, der Teufel kennt die heiligen Schriften.<br><br> Ich habe nur gute Originalbibeln (wenn möglich keine Nachbearbeitung) in mehreren Sprachen hinzugefügt, die ich verstehen kann, um Ihnen zu helfen, Jesus zu finden.<br><br>
Bleib nicht in der Dunkelheit, kleine Kinder Gottes.<br><br> Sei gut zu deinen Brüdern und Schwestern, teile das Wort Gottes, liebe, bete, bereue, habe Glauben, Freude,
Befolgen Sie die Gebote, stoppen Sie Ihre Sünden und säen Sie reichlich aus.<br><br> Folgen wir Jesus, dem guten Hirten, unserem einzigen Retter... Gott schütze dich. Amen.<br><br>
<br>
<b>Johannes 14.6:</b> Jesus spricht zu ihm: Ich bin der Weg und die Wahrheit und das Leben; niemand kommt zum Vater, denn durch mich!
<br>
<br>
<b>Johannes 15.12:</b> Das ist mein Gebot, daß ihr einander liebet, gleichwie ich euch geliebt habe.
<br>
<br>
<b>Johannes 10.11:</b> Ich bin der gute Hirt; der gute Hirt läßt sein Leben für die Schafe.
<br>
<br>
<b>Psalmen 23.3:</b> Er erquickt meine Seele, er führt mich auf rechter Straße um seines Namens willen.
<br>
<br>
<b>Psalmen 23.4:</b> Und ob ich schon wanderte im finstern Todestal, fürchte ich kein Unglück; denn du bist bei mir, dein Stecken und dein Stab, die trösten mich!
<br>
<br>
<b>Psalmen 23.5:</b> Du bereitest vor mir einen Tisch angesichts meiner Feinde; du hast mein Haupt mit Öl gesalbt, mein Becher fließt über.
<br>
<br>
<b>Psalmen 23.6:</b> Nur Güte und Gnade werden mir folgen mein Leben lang, und ich werde bleiben im Hause des HERRN immerdar.
<br>
<br>
<b>1Korinther 3.16:</b> Wisset ihr nicht, daß ihr Gottes Tempel seid und der Geist Gottes in euch wohnt?
<br>
<br>
<b>1Korinther 3.17:</b> Wenn jemand den Tempel Gottes verderbt, den wird Gott verderben; denn der Tempel Gottes ist heilig, und der seid ihr.<br>
<br><br>
<H>In der Bibel suchen</H>
Syntax: &lt;Buch&gt; <br>
Syntax: &lt;Buch&gt; &lt;Kapitel&gt; <br>
Syntax: &lt;Buch&gt; &lt;Kapitel&gt; &lt;Vers&gt; <br>
Syntax: &lt;Buch&gt; &lt;Kapitel&gt; &lt;Vers von&gt; &lt;Vers bis&gt; <br>
Syntax: &lt;Buch&gt; &lt;Ausdruck&gt; <br>
Syntax: &lt;Buch&gt; &lt;Kapitel&gt; &lt;Ausdruck&gt; <br>
Syntax: &lt;Ausdruck&gt; <br>
<br><br>
<b>Beispielen:</b><br>
• 1Mose 1<br>
Gib Kapitel 1 der Genesis<br>
<br>
• 1Mose 1 5<br>
Gib den Vers 1.5 der Genesis :)<br>
<br>
• 1Mose Feuer<br>
Suche \'Feuer\' in der Genesis<br>
<br>
• 1 6 Lord<br>
Suche \'Lord\' in Kapitel 6 der Genesis<br>
<br>
• Abraham<br>
Suche \'Abraham\' in der ganzen Bibel<br>
<br>
• Offenbarung Pferd<br>
Suche nach den Pferden der Apokalypse :)<br>
<br>
• 66 Babylone<br>
Suche \'Babylon\' in Buch 66 (Offenbarung)<br>
<br>
• Petrus%Gemeinde<br>
Suche in der Reihenfolge 'Petrus' und 'Gemeinde'<br>
<br><br>
<b>Bemerkungen:</b><br>
- Die Buchnamen sind sensibel, also wählen Sie sie aus der Liste<br>
- Setzen Sie Akzente in Ihren Wörtern, wenn sie welche haben:<br>
Beispiel: j<u>é</u>zabel (français), jezebel (anglais), <u>É</u>lie (français), Elijah (anglais), Elia (italien), El<u>í</u>as (espagnol)<br>
<br><br>
<H>Vereinbarung</H>
Die App bleibt komplett kostenlos, ohne Werbung, Umleitung, nicht online und wird nicht verkauft. Das ist keine Demo, sondern eine komplette Version.<br>
<br><br>
<H>Kontakt</H>
Falls Sie Probleme oder Anregungen haben, wenden Sie sich bitte an mich.<br>
<br><br>
""",
    "fav0": "Internal",
    "fav1": "Favorit",
    "fav2": "Lesen",
    "fav10": "Liebe",
    "fav20": "Top 100",
    "fav23": "Zu lesen",
    "fav30": "Zu studieren",
    "fav40": "Zu lösen",
    "fav50": "Erledigt",
    "fav60": "Ausruf",
    "fav70": "Frage",
    "fav80": "Wichtig",
    "fav90": "Gefahr",
    "fav100": "Tod",
    "fav105": "Leben",
    "fav110": "Prophezeiung"
  };
}
*/

