library bible.pcommon;

import 'dart:convert';
import 'dart:io';
import 'package:bible_multi_the_life/dbHelper.dart';
import 'package:bible_multi_the_life/penums.dart';
import 'package:bible_multi_the_life/pstyle.dart';
import 'package:bible_multi_the_life/resource.dart' as R;
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:share_plus/share_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart' as URL;

//_snapBuild: 40;
bool isDebug = false;
final bool shouldSimulateChromebook = false; //Should be false
int debugCounter = 0;
const int historyLimit = 5;
const int searchQueryLimit = 3;
const int pageRowLimit = 20;
const int bookmarkIdDevLimit = 1000;

Map<String, CustomVerseStyle> _mapCustomVerseStyle = {};
String _customVerseStyleClipboard = "";

///To update for each Release: AndroidManifest + pubspec.yaml + DbHelper + android/app/build.gradle
///Get versionNumber, versionDate, appOtherUrls
Map getVersion() {
  final String versionDate = "20250313"; //Manifest Version Code
  final String versionNumber = "1.32.0"; //Manifest version Name
  /* Droïd */
  const String appOtherUrls = "* Android (Google):\nhttps://play.google.com/store/apps/details?id=org.hlwd.bible_multi_the_life \n\n* Android (F-Droid):\nhttps://f-droid.org/en/packages/org.hlwd.bible_multi_the_life \n\n* Linux (Snap):\nhttps://snapcraft.io/bible-multi-the-life \n\n* Linux (Flatpak):\nhttps://flathub.org/apps/org.biblemulti.thelife \n\n* More:\nhttps://www.biblemulti.org \n\n";
  /* Apple */
  //const String appOtherUrls = ""; //Let empty for Apple

  return {
    'versionNumber': versionNumber,
    'versionDate': versionDate,
    'appOtherUrls': appOtherUrls,
  };
}

Future<ThemeData> getThemeData() async {
  try {
    final String themeName = await Prefs.getThemeName;
    if (themeName.compareTo('DARK') == 0) return Future.value(PStyle.instance.darkThemeData);
  } catch (ex, stacktrace) {
    if (isDebug) printLog(ex, stacktrace);
  }

  return Future.value(PStyle.instance.lightThemeData);
}

/* T O O L S */

/// Copy text to clipboard
/// [toastMsg] is displayed in toast if not empty
Future<void> copyTextToClipboard(final BuildContext context, final String textToCopy, final String toastMsg, final bool shouldShowToast) async {
  try {
    await Clipboard.setData(ClipboardData(text: textToCopy));

    if (!toastMsg.isEmpty) {
      if (shouldShowToast) showToast(context, toastMsg, Toast.LENGTH_SHORT);
    }
  } catch (ex, stacktrace) {
    if (isDebug) printLog(ex, stacktrace);
  }
}

Future<void> share(final String textToShare) async {
  try {
    await Share.share(textToShare.isEmpty ? "Clipboard is empty :)" : textToShare);
  } catch (ex, stacktrace) {
    if (isDebug) printLog(ex, stacktrace);
  }
}

void showToast(final BuildContext context, final String toastMsg, final Duration toastDuration) {
  try {
    toast(toastMsg, duration: toastDuration, context: context);
  } catch (ex, stacktrace) {
    if (isDebug) printLog(ex, stacktrace);
  }
}

Future<void> launchUrl(final BuildContext context, final dynamic url) async {
  try {
    Uri uri;
    if (url is String) {
      uri = Uri.parse(url);
    } else {
      uri = url;
    }

    try {
      final String toastCopiedClipboard = R.getString(R.id.copiedClipboard);
      await copyTextToClipboard(context, url.toString(), toastCopiedClipboard, true);
    } catch (ex, stacktrace) {
      if (isDebug) printLog(ex, stacktrace);
    }

    if (await URL.canLaunchUrl(uri)) {
      await URL.launchUrl(uri);
    }
  } catch (ex, stacktrace) {
    if (isDebug) printLog(ex, stacktrace);
  }
}

Future<void> launchEmail({required final context, required final String email, required final String subject}) async {
  String encodeQueryParameters(Map<String, String> params) {
    return params.entries.map((e) => '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}').join('&');
  }

  final Uri emailLaunchUri = Uri(
    scheme: 'mailto',
    path: email,
    query: encodeQueryParameters(<String, String>{'subject': subject}),
  );

  launchUrl(context, emailLaunchUri);
}

String nowYYYYMMDD() => DateFormat("yyyyMMdd").format(DateTime.now());

String replaceCustomHtml(String html) {
  try {
    html = html.replaceAll("<HS>", "<br><span><u>").replaceAll("</HS>", "</u></span>").replaceAll("<H>", "<h1><u>").replaceAll("</H>", "</u></h1>");
  } catch (ex, stacktrace) {
    if (isDebug) printLog(ex, stacktrace);
  }
  return html;
}

String rq(String str) {
  try {
    str = str.replaceAll("'", "''");
  } catch (ex, stacktrace) {
    if (isDebug) printLog(ex, stacktrace);
  }
  return str;
}

String getLTR() => "\u202A";

String getRTL() => "\u202B";

void printLog(final Object ex, final stacktrace) {
  try {
    print(">> Error: ${ex.toString()}\n${stacktrace}");
  } catch (exInternal) {
    if (isDebug) print(exInternal);
  }
}

/* L O C A L E S */
String convertLocaleToString(final BBLocale bbLocale) {
  try {
    final String localeStr = bbLocale.toString().toUpperCase();
    final int len = localeStr.length;
    if (len < 2) return 'EN';
    return localeStr.substring(len - 2, len);
  } catch (ex, stacktrace) {
    if (isDebug) printLog(ex, stacktrace);
  }
  return 'EN';
}

BBLocale convertStringToBBLocale(String localeAsString) {
  localeAsString = localeAsString.toUpperCase();
  switch (localeAsString) {
    case 'EN':
      return BBLocale.EN;
    case 'ES':
      return BBLocale.ES;
    case 'PT':
      return BBLocale.PT;
    case 'FR':
      return BBLocale.FR;
    case 'IT':
      return BBLocale.IT;
    case 'DE':
      return BBLocale.DE;
    case 'IN':
      return BBLocale.IN;
    case 'AR':
      return BBLocale.AR;
    case 'CN':
      return BBLocale.CN;
    case 'JP':
      return BBLocale.JP;
    case 'RU':
      return BBLocale.RU;
    case 'TR':
      return BBLocale.TR;
    case 'BD':
      return BBLocale.BD;
    case 'TZ':
      return BBLocale.TZ;
    case 'RO':
      return BBLocale.RO;
    case 'PL':
      return BBLocale.PL;
    default:
      return BBLocale.EN;
  }
}

String getBibleName(final String bbName0, final bool isVeryVerboseVersion) {
  switch (bbName0) {
    case "k":
      return isVeryVerboseVersion ? "King James 1611" : "(EN) KJV 1611";
    case "2":
      return isVeryVerboseVersion ? "King James 2000" : "(EN) KJ2K";
    case "v":
      return isVeryVerboseVersion ? "Reina Valera 1909" : "(ES) Valera 1909";
    case "9":
      return isVeryVerboseVersion ? "Reina Valera 1989" : "(ES) Valera 1989";
    case "l":
      return isVeryVerboseVersion ? "Louis Segond 1910" : "(FR) Segond";
    case "o":
      return isVeryVerboseVersion ? "Ostervald 1996" : "(FR) Ostervald";
    case "d":
      return isVeryVerboseVersion ? "Diodati 1649" : "(IT) Diodati 1649";
    case "1":
      return isVeryVerboseVersion ? "Nuova Diodati 1991" : "(IT) Nuova Diodati";
    case "a":
      return isVeryVerboseVersion ? "Almeida CF 1995" : "(PT) Almeida";
    case "s":
      return isVeryVerboseVersion ? "Schlachter 1951" : "(DE) Schlachter";
    case "i":
      return isVeryVerboseVersion ? "Hindi Indian Revised 2017" : "(IN) Hindi Indian";
    case "y":
      return isVeryVerboseVersion ? "Smith & Van Dyke 1865" : "(AR) Smith & Van Dyke";
    case "c":
      return isVeryVerboseVersion ? "Chinese Union Version Simplified" : "(CN) Chinese Simplified";
    case "j":
      return isVeryVerboseVersion ? "New Japanese Bible 1973" : "(JP) New Japanese";
    case "r":
      return isVeryVerboseVersion ? "Russian Synodal Translation 1876" : "(RU) Synodal";
    case "t":
      return isVeryVerboseVersion ? "New Turkish Bible 2001" : "(TR) New Turkish";
    case "b":
      return isVeryVerboseVersion ? "Bengali C.L. 2016" : "(BD) Bengali";
    case "h":
      return isVeryVerboseVersion ? "Swahili Union Version 1997" : "(SW) Swahili";
    case "e":
      return isVeryVerboseVersion ? "Elberfelder 1932" : "(DE) Elberfelder";
    case "u":
      return isVeryVerboseVersion ? "Romanian Cornilescu 1928" : "(RO) Cornilescu";
    case "z":
      return isVeryVerboseVersion ? "Biblia Warszawska 1975" : "(PL) Warszawska";
    default:
      return '';
  }
}

String getLocaleNameVerbose(final String bbLocale) {
  switch (bbLocale.toUpperCase()) {
    case "EN":
      return "English";
    case "ES":
      return "Española";
    case "FR":
      return "Français";
    case "IT":
      return "Italiano";
    case "PT":
      return "Português";
    case "DE":
      return "Deutsch";
    case "IN":
      return "हिंदी";
    case "AR":
      return "عربي";
    case "CN":
      return "简体中文";
    case "JP":
      return "日本";
    case "RU":
      return "Русский";
    case "TR":
      return "Türkçe";
    case "BD":
      return "বাংলা";
    case "TZ":
      return "Kiswahili";
    case "RO":
      return "Română";
    case "PL":
      return "Polski";
    default:
      return '';
  }
}

String getLocaleName(final int indexSelected) {
  final List<String> lstLocale = ["EN", "en", "ES", "es", "FR", "fr", "IT", "it", "PT", "DE", "IN", "AR", "CN", "JP", "RU", "TR", "BD", "TZ", "de", "RO", "PL"];
  return (indexSelected >= 0 && indexSelected <= 20) ? lstLocale[indexSelected] : "EN";
}

/* S T Y L E S */
void saveCustomVerseStyle(String bbName, final Map<String, dynamic> customBibleInternal, final bool isLightThemeAsDefault) async {
  try {
    bbName = bbName.toLowerCase();

    //Check
    if (DbHelper.instance.getAllBBNamesOfDb().indexOf(bbName) < 0) {
      if (isDebug) print("saveCustomVerseStyle: Invalid bbName: '${bbName}'");
      return;
    }

    //Save Pref
    final String appPrefKey = "CUSTOM_BIBLE_${bbName.toUpperCase()}";
    final String customBibleJsonString = """
    {
      "vFontFamily": "${customBibleInternal["vFontFamily"]}",
      "vFontSize": "${customBibleInternal["vFontSize"]}",
      "vNumberFgColor": "${customBibleInternal["vNumberFgColor"]}",
      "vNumberBgColor": "${customBibleInternal["vNumberBgColor"]}",
      "vTextFgColor": "${customBibleInternal["vTextFgColor"]}",
      "vTextBgColor": "${customBibleInternal["vTextBgColor"]}",
      "vCrFgColor": "${customBibleInternal["vCrFgColor"]}",
      "vCrBgColor": "${customBibleInternal["vCrBgColor"]}" 
    }""";

    _PreferencesHelper.setString(appPrefKey, customBibleJsonString);

    //Save Map
    final String bbNames = await Prefs.getBibleName;
    final bool isBBNameDisplayed = bbNames.indexOf(bbName) >= 0;
    if (isBBNameDisplayed) {
      final CustomVerseStyle customVerseStyle = CustomVerseStyle.convertInternalToCustomVerseStyle(customBibleInternal, isLightThemeAsDefault);
      _mapCustomVerseStyle[bbName] = customVerseStyle;
    }
  } catch (ex, stacktrace) {
    if (isDebug) printLog(ex, stacktrace);
  }
}

Map<String, dynamic> getCustomBibleInternalDefault(final bool isLightThemeAsDefault) {
  final CustomVerseStyle _defaultThemeVerseStyle = isLightThemeAsDefault ? CustomVerseStyle.lightTheme_getDefault() : CustomVerseStyle.darkTheme_getDefault();
  final Map<String, dynamic> internal = {
    "vFontFamily": "${_defaultThemeVerseStyle.vFontFamily}",
    "vFontSize": "${_defaultThemeVerseStyle.vFontSize}",
    "vNumberFgColor": "${_defaultThemeVerseStyle.vNumberFgColor.value}",
    "vNumberBgColor": "${_defaultThemeVerseStyle.vNumberBgColor.value}",
    "vTextFgColor": "${_defaultThemeVerseStyle.vTextFgColor.value}",
    "vTextBgColor": "${_defaultThemeVerseStyle.vTextBgColor.value}",
    "vCrFgColor": "${_defaultThemeVerseStyle.vCrFgColor.value}",
    "vCrBgColor": "${_defaultThemeVerseStyle.vCrBgColor.value}",
  };

  return internal;
}

CustomVerseStyle getCustomVerseStyle_(final bool isLightThemeAsDefault) => isLightThemeAsDefault ? CustomVerseStyle.lightTheme_getDefault() : CustomVerseStyle.darkTheme_getDefault();

Future<CustomVerseStyle> getCustomVerseStyle(String bbName, final bool doCompleteTests, final bool isLightThemeAsDefault) async {
  try {
    bbName = bbName.toLowerCase();

    //Check
    if (doCompleteTests) {
      if (DbHelper.instance.getAllBBNamesOfDb().indexOf(bbName) < 0) {
        if (isDebug) print("getCustomBibleVerseStyle: Invalid bbName: '${bbName}'");
        return getCustomVerseStyle_(isLightThemeAsDefault);
      }
    }

    //Get Map
    if (doCompleteTests) {
      final String bbNames = await Prefs.getBibleName;
      final bool isBBNameDisplayed = bbNames.indexOf(bbName) >= 0;
      if (isBBNameDisplayed) return _mapCustomVerseStyle[bbName] ?? getCustomVerseStyle_(isLightThemeAsDefault);
    } else {
      return _mapCustomVerseStyle[bbName] ?? getCustomVerseStyle_(isLightThemeAsDefault);
    }

    //From Pref, if not in map
    final String customBibleJsonString = await Prefs.getCustomBibleInternal(bbName);
    if (customBibleJsonString.isEmpty) return getCustomVerseStyle_(isLightThemeAsDefault);

    final Map<String, String> customBibleInternal = jsonDecode(customBibleJsonString);
    final CustomVerseStyle res = CustomVerseStyle.convertInternalToCustomVerseStyle(customBibleInternal, isLightThemeAsDefault);

    _mapCustomVerseStyle[bbName] = res;
    return res;
  } catch (ex, stacktrace) {
    if (isDebug) printLog(ex, stacktrace);
  }

  return getCustomVerseStyle_(isLightThemeAsDefault);
}

Future<void> refreshCustomVerseStylesDisplayed(final bool isLightThemeAsDefault) async {
  try {
    _mapCustomVerseStyle.clear();

    final CustomVerseStyle res = isLightThemeAsDefault ? CustomVerseStyle.lightTheme_getDefault() : CustomVerseStyle.darkTheme_getDefault();
    final String bbNames = await Prefs.getBibleName;
    final int size = bbNames.length;

    //From Prefs
    for (int index = 0; index < size; index++) {
      final String bbName = bbNames[index];

      final String customBibleJsonString = await Prefs.getCustomBibleInternal(bbName);
      if (customBibleJsonString.isEmpty) {
        _mapCustomVerseStyle[bbName] = res;
      } else {
        try {
          final Map<String, dynamic> customBibleInternal = jsonDecode(customBibleJsonString);
          _mapCustomVerseStyle[bbName] = CustomVerseStyle.convertInternalToCustomVerseStyle(customBibleInternal, isLightThemeAsDefault);
        } catch (ex, stacktrace) {
          _mapCustomVerseStyle[bbName] = res;
          if (isDebug) printLog(ex, stacktrace);
        }
      }
    }
  } catch (ex, stacktrace) {
    if (isDebug) printLog(ex, stacktrace);
  }
}

String getCustomVerseStyleClipboard() => _customVerseStyleClipboard;

void setCustomVerseStyleClipboard(final String value) => _customVerseStyleClipboard = value;


/* U.I. */
void onShowDialog({required BuildContext context, required final R.id titleId, required final R.id contentId, required final TextStyle textStyle}) {
  showDialog(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return WillPopScope(
        onWillPop: () async => true,
        child: AlertDialog(
          scrollable: true,
          title: Text(R.getString(titleId)),
          content: Text(R.getString(contentId), style: textStyle, textAlign: TextAlign.left),
          actions: [
            OutlinedButton(
              onPressed: () => Navigator.pop(context),
              child: Text(R.getString(R.id.mnuClose)),
            ),
          ],
        ),
      );
    },
  );
}

/// Get AppBar height
Future<double> getAppBarHeight(final BuildContext context) async {
  double height = kToolbarHeight;
  bool isChromebook = shouldSimulateChromebook;

  try {
    if (Platform.isAndroid) {
      final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
      final AndroidDeviceInfo deviceInfo = await deviceInfoPlugin.androidInfo;
      if (deviceInfo.model.toUpperCase().contains('CHROMEBOOK') || deviceInfo.device.toUpperCase().contains('CHROMEBOOK')) {
        isChromebook = true;
      } else if (deviceInfo.version.baseOS != null && deviceInfo.version.baseOS!.toUpperCase().contains('CHROMEOS')) {
        isChromebook = true;
      }

      if (isChromebook) {
        final MediaQueryData mediaQueryData = MediaQuery.of(context);
        final double statusBarHeight = mediaQueryData.systemGestureInsets.top; //mediaQueryData.padding.top;
        if (statusBarHeight > 0.0) {
          height += 40.0 + statusBarHeight;
        }
      }
    }
  } catch (ex, stacktrace) {
    if (isDebug) printLog(ex, stacktrace);
  }

  return Future.value(height);
}

/// Generate a list of TextSpan based on a list of font names
List<TextSpan> getListTextSpanWithFontNames(final List<String> lstFontName, final VerseStyle menuVerseStyle) {
  try {
    const sample = "\nThe quick brown fox jumps over the lazy dog. THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG.";
    final double fontSizeMenu = PStyle.instance.fontSize;
    final int size = lstFontName.length;
    final List<TextSpan> genLstAllTextSpanWithFontNames = List.generate(
        size,
        (index) => TextSpan(
              text: "\n",
              style: TextStyle(color: menuVerseStyle.defaultFgColor, fontSize: fontSizeMenu),
              children: <TextSpan>[
                TextSpan(text: lstFontName[index], style: TextStyle(fontSize: fontSizeMenu)),
                TextSpan(text: sample, style: TextStyle(fontFamily: lstFontName[index], color: menuVerseStyle.defaultFgColor, fontSize: fontSizeMenu)),
              ],
            ));
    return genLstAllTextSpanWithFontNames;
  } catch (ex, stacktrace) {
    if (isDebug) printLog(ex, stacktrace);
  }
  return [];
}

/// Generate a list of TextSpan based on a list of font sizes
List<TextSpan> getListTextSpanWithFontSizes(final List<int> lstFontSize, final VerseStyle menuVerseStyle) {
  try {
    final double fontSizeMenu = PStyle.instance.fontSize;
    final int size = lstFontSize.length;
    final List<TextSpan> genLstAllTextSpanWithFontSizes = List.generate(
        size,
        (index) => TextSpan(
              text: "${lstFontSize[index]}",
              style: TextStyle(color: menuVerseStyle.defaultFgColor, fontSize: fontSizeMenu),
            ));
    return genLstAllTextSpanWithFontSizes;
  } catch (ex, stacktrace) {
    if (isDebug) printLog(ex, stacktrace);
  }
  return [];
}

List<TextSpan> getListTextSpanWithCustomColors(final List<int> lstCustomColor, final VerseStyle menuVerseStyle) {
  try {
    final double fontSize = PStyle.instance.fontSizeForMenu();
    final int size = lstCustomColor.length;
    final List<TextSpan> genLstTextSpanWithCustomColors = List.generate(
      size,
      (index) => lstCustomColor[index] < 0
          ? TextSpan(
              text: "Auto",
              style: TextStyle(
                color: menuVerseStyle.defaultFgColor,
                fontSize: fontSize,
                background: Paint()
                  ..color = menuVerseStyle.defaultFgColor
                  ..style = PaintingStyle.stroke
                  ..strokeCap = StrokeCap.square
                  ..strokeWidth = 2.0,
              ),
            )
          : TextSpan(
              text: "███",
              style: TextStyle(
                color: Color(lstCustomColor[index]),
                fontSize: fontSize,
                background: Paint()
                  ..color = menuVerseStyle.defaultFgColor
                  ..style = PaintingStyle.stroke
                  ..strokeCap = StrokeCap.square
                  ..strokeWidth = 2.0,
              ),
            ),
    );
    return genLstTextSpanWithCustomColors;
  } catch (ex, stacktrace) {
    if (isDebug) printLog(ex, stacktrace);
  }
  return [];
}

/* P R E F E R E N C E S */
abstract class AppPrefKey {
  static const BIBLE_NAME = 'BIBLE_NAME';
  static const BIBLE_LOCALE = 'BIBLE_LOCALE';
  static const BIBLE_ALT_LOCALE = 'BIBLE_ALT_LOCALE';
  static const THEME_NAME = 'THEME_NAME';
  static const BOOK_NUMBER = 'BOOK_NUMBER';
  static const CHAPTER_NUMBER = 'CHAPTER_NUMBER';
  static const FONT_NAME = 'FONT_NAME';
  static const FONT_SIZE = 'FONT_SIZE';
  static const TAB_SELECTED = 'TAB_SELECTED';
  static const CLIPBOARD_IDS = 'CLIPBOARD_IDS';
  static const LAYOUT_DYNAMIC1 = "LAYOUT_DYNAMIC1";
  static const STYLE_HIGHLIGHT_SEARCH = 'STYLE_HIGHLIGHT_SEARCH';
}

class _PreferencesHelper {
  //Helper
  static Future<SharedPreferences> get prefs => SharedPreferences.getInstance();

  static Future<String> getString(String appPrefKey) async {
    final p = await prefs;
    return p.getString(appPrefKey) ?? '';
  }

  static Future<List<String>> getStringList(String appPrefKey) async {
    final p = await prefs;
    return p.getStringList(appPrefKey) ?? [];
  }

  static Future setString(String appPrefKey, String value) async {
    final p = await prefs;
    return p.setString(appPrefKey, value);
  }

  static Future setStringList(String appPrefKey, List<String> value) async {
    final p = await prefs;
    return p.setStringList(appPrefKey, value);
  }
}

class Prefs {
  static Future<String> get getBibleLocale => _PreferencesHelper.getString(AppPrefKey.BIBLE_LOCALE);

  static Future saveBibleLocale(String value) => _PreferencesHelper.setString(AppPrefKey.BIBLE_LOCALE, value);

  static Future<String> get getBibleAltLocale => _PreferencesHelper.getString(AppPrefKey.BIBLE_ALT_LOCALE);

  static Future saveBibleAltLocale(String value) => _PreferencesHelper.setString(AppPrefKey.BIBLE_ALT_LOCALE, value);

  static Future<String> get getBibleName => _PreferencesHelper.getString(AppPrefKey.BIBLE_NAME);

  static Future saveBibleName(String value) => _PreferencesHelper.setString(AppPrefKey.BIBLE_NAME, value);

  static Future<String> get getThemeName => _PreferencesHelper.getString(AppPrefKey.THEME_NAME);

  static Future saveThemeName(String value) => _PreferencesHelper.setString(AppPrefKey.THEME_NAME, value);

  static Future<String> get getFontName => _PreferencesHelper.getString(AppPrefKey.FONT_NAME);

  static Future saveFontName(String value) => _PreferencesHelper.setString(AppPrefKey.FONT_NAME, value);

  static Future<String> get getFontSize => _PreferencesHelper.getString(AppPrefKey.FONT_SIZE);

  static Future saveFontSize(String value) => _PreferencesHelper.setString(AppPrefKey.FONT_SIZE, value);

  static Future<String> get getLayoutDynamic1 => _PreferencesHelper.getString(AppPrefKey.LAYOUT_DYNAMIC1);

  static Future saveLayoutDynamic1(String value) => _PreferencesHelper.setString(AppPrefKey.LAYOUT_DYNAMIC1, value);

  static Future<String> get getTabSelected => _PreferencesHelper.getString(AppPrefKey.TAB_SELECTED);

  static Future saveTabSelected(String value) => _PreferencesHelper.setString(AppPrefKey.TAB_SELECTED, value);

  static Future<List<String>> get getClipboardIds => _PreferencesHelper.getStringList(AppPrefKey.CLIPBOARD_IDS);

  static Future saveClipboardIds(List<String> value) => _PreferencesHelper.setStringList(AppPrefKey.CLIPBOARD_IDS, value);

  static Future<String> get getStyleHighlightSearch => _PreferencesHelper.getString(AppPrefKey.STYLE_HIGHLIGHT_SEARCH);

  static Future saveStyleHighlightSearch(String value) => _PreferencesHelper.setString(AppPrefKey.STYLE_HIGHLIGHT_SEARCH, value);

  static Future<String> getCustomBibleInternal(String bbName) => _PreferencesHelper.getString("CUSTOM_BIBLE_${bbName.toUpperCase()}");
}

//HIVE
/* PROBLEM WITH HIVE.
//Need to create it, remove all close, change savePref

Box box;
Future<bool> openBox() async {
  var dir = await getApplicationDocumentsDirectory();
  Hive.init(dir.path);

  box = await Hive.openBox('AppPref');
  return Future.value(true);
}

String getPref(final String appPrefKey) {
  //Box box;
  try {
    final String value = box.containsKey(appPrefKey) ? box.get(appPrefKey) : '';
    return value;
  } catch(ex, stacktrace) {
    if (isDebug) P.printLog(ex, stacktrace);
  } finally {
    try {
      box.close();
    } catch(ex, stacktrace) {
      if (isDebug) P.printLog(ex, stacktrace);
    }
  }
  return '';
}

/// Get preference
/// @return String
String _getPref(final String appPrefKey) {
  Box box;
  try {
    box = Hive.box('AppPref');
    final String value = box.containsKey(appPrefKey) ? box.get(appPrefKey) : '';
    return value;
  } catch(ex, stacktrace) {
    if (isDebug) P.printLog(ex, stacktrace);
  } finally {
    try {
      box.close();
    } catch(ex, stacktrace) {
      if (isDebug) P.printLog(ex, stacktrace);
    }
  }
  return '';
}

/// Save preference as string
void savePref(final String appPrefKey, String value) {
  Box box;
  try {
    if (value == null) value = '';
    box = Hive.box('AppPref');
    box.put(appPrefKey, value);
  } catch(ex, stacktrace) {
    if (isDebug) P.printLog(ex, stacktrace);
  } finally {
    try {
      box.close();
    } catch(ex, stacktrace) {
      if (isDebug) P.printLog(ex, stacktrace);
    }
  }
}
*/

/*
/// Method to slow code and test async methods
void slow() {
  sleep(Duration(milliseconds: 2000));
}
*/

/*
void PrintStackTrace(final StackTraceElement[] stackTrace, final PrintWriter pw) {
  for(StackTraceElement stackTraceElement : stackTrace) {
    pw.println(stackTraceElement);
  }
}

String StackTraceToString(final StackTraceElement[] stackTrace)
{
  final StringWriter sw = new StringWriter();
  PrintStackTrace(stackTrace, new PrintWriter(sw));

  return sw.toString();
}
*/
