library bible.pstyle;

import 'package:flutter/material.dart';

/*
- stay in settings when leaving customize Bible
- add accent colors
- click reset bible
- decision of menu style (min allowed, max allowed?)
- add themes
*/

class PStyle {
  //Singleton
  PStyle._privateConstructor();
  static final PStyle instance = PStyle._privateConstructor();

  /* Common style properties */
  String fontFamily = "Droid-sans.regular";
  double fontSize = 20.0;
  get fontSizeForIconInPage => 20.0;

  //TODO: MENU, we will use the selected font family for Menus
  String fontFamilyForMenu() => fontFamily;

  //TODO: MENU, minimal height
  double fontSizeForMenu({ double? max }) {
    if (max == null) max = 20.0;
    final double sizeMin = 16.0;
    final double sizeMax = max < sizeMin ? 20.0 : max;
    final fontSizeMenu = (fontSize > sizeMax)
        ? sizeMax
        : (fontSize < sizeMin)
            ? sizeMin
            : fontSize;
    return fontSizeMenu;
  }

  ///Get verse style used
/*Future<VerseStyle> getVerseStyle() async {
  final String themeName = await P.Prefs.getThemeName;
  return Future.value(themeName.compareTo('LIGHT') == 0 ? LightVerseStyle() : DarkVerseStyle());
}*/

  ///Get verse style used
  VerseStyle getVerseStyle(final String themeName) => themeName.compareTo('LIGHT') == 0 ? LightVerseStyle() : DarkVerseStyle();

  //Official Dark
  final ThemeData darkThemeData = ThemeData.dark();
  /*.copyWith(
      brightness: Brightness.dark,
      primaryColor: Colors.blue,
  );*/

  /* Working but predicated attributes
  final ThemeData darkThemeData = ThemeData.dark().copyWith(
      primaryColor: Colors.blue,
      textTheme: ThemeData.dark().textTheme.copyWith( // Modification du thème de texte
        bodyText1: TextStyle(fontSize: 16.0, color: Colors.white), // Style de texte par défaut
        headline1: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold, color: Colors.white),
      ),
  );
   */

  /*
  final ThemeData darkThemeDataPrev = ThemeData(
    brightness: Brightness.dark,
    colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blue).copyWith(secondary: Colors.blueAccent),
  );
   */
  //primarySwatch: Colors.deepOrange,
  //accentColor: Colors.deepOrangeAccent,

  //TEST
  /*final ThemeData darkThemeData = ThemeData.dark().copyWith(
    accentColor: Colors.blueAccent, //accentColor: Colors.deepOrangeAccent,
    scaffoldBackgroundColor: Colors.black26,
    appBarTheme: AppBarTheme(elevation: 1.0, brightness: Brightness.light),
    buttonTheme: ButtonThemeData(hoverColor: Colors.deepOrange),
    );*/

  //TEST Light less agressive!
  /*final ThemeData lightThemeData = ThemeData(
    primarySwatch: Colors.red,
    accentColor: Colors.blueAccent,
    //based on < 0.5: brightness: ColorScheme.lerp(ColorScheme.light().copyWith(), ColorScheme.dark().copyWith(), 0.6).brightness,
  );
   */

  //Official Light
  final ThemeData lightThemeData = ThemeData.light();
  /*final ThemeData lightThemeDataPrev = ThemeData(
    brightness: Brightness.light,
    colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blue).copyWith(secondary: Colors.blueAccent),
  );
   */
  //colorScheme: ColorScheme.light(onBackground: Colors.grey, brightness: Brightness.light),
}

/* Styles */
abstract class VerseStyle {
  late Color defaultFgColor;
  late Color inverseDefaultFgColor;
  late Color bookNameFgColor;
  late Color accentFgColor;
}

//Official LightVerseStyle
class LightVerseStyle implements VerseStyle {
  Color defaultFgColor = Colors.black;
  Color inverseDefaultFgColor = Colors.white;
  Color bookNameFgColor = Colors.blue;
  Color accentFgColor = Colors.blue;
}

//Official DarkVerseStyle
class DarkVerseStyle implements VerseStyle {
  Color defaultFgColor = Colors.white;
  Color inverseDefaultFgColor = Colors.black;
  Color bookNameFgColor = Colors.blue;
  Color accentFgColor = Colors.blue;
}

Color convertStringColorToColor(final String colorIntValue) => Color(int.parse(colorIntValue.toString()));

class CustomVerseStyle {
  late String vFontFamily;
  late double vFontSize;
  late Color vNumberFgColor;
  late Color vNumberBgColor;
  late Color vTextFgColor;
  late Color vTextBgColor;
  late Color vCrFgColor;
  late Color vCrBgColor;

  CustomVerseStyle.lightTheme_getDefault() {
    this.vFontFamily = PStyle.instance.fontFamily;
    this.vFontSize = PStyle.instance.fontSize;
    this.vNumberFgColor = Color(0xFF2196F3);
    this.vNumberBgColor = ThemeData.light().colorScheme.background;
    this.vTextFgColor  = Color(0xFF000000); //TODO: COLOR, get real Light text color (use material: true?)
    this.vTextBgColor = ThemeData.light().colorScheme.background;
    this.vCrFgColor = Color(0xFFF44336);
    this.vCrBgColor = ThemeData.light().colorScheme.background;
  }

  CustomVerseStyle.darkTheme_getDefault() {
    this.vFontFamily = PStyle.instance.fontFamily;
    this.vFontSize = PStyle.instance.fontSize;
    this.vNumberFgColor = Color(0xFF2196F3);
    this.vNumberBgColor = ThemeData.dark().colorScheme.background;
    this.vTextFgColor  = Color(0xFFFFFFFF); //TODO: COLOR, get real Dark text color (use material: true?)
    this.vTextBgColor = ThemeData.dark().colorScheme.background;
    this.vCrFgColor = Color(0xFFF44336);
    this.vCrBgColor = ThemeData.dark().colorScheme.background;
  }

  CustomVerseStyle.convertInternalToCustomVerseStyle(final Map<String, dynamic> internal, final bool isLightThemeAsDefault) {
    final CustomVerseStyle _defaultThemeVerseStyle = isLightThemeAsDefault ? CustomVerseStyle.lightTheme_getDefault() : CustomVerseStyle.darkTheme_getDefault();
    final Color _defaultThemeVerseStyleFgColor = _defaultThemeVerseStyle.vTextFgColor;
    final Color _defaultThemeVerseStyleBgColor = _defaultThemeVerseStyle.vTextBgColor;

    this.vFontFamily = internal['vFontFamily']!;
    this.vFontSize = double.parse(internal['vFontSize']!);

    this.vNumberFgColor = internal['vNumberFgColor'].toString() == "auto" ? _defaultThemeVerseStyleFgColor : convertStringColorToColor(internal['vNumberFgColor']!);
    this.vNumberBgColor = internal['vNumberBgColor'].toString() == "auto" ? _defaultThemeVerseStyleBgColor : convertStringColorToColor(internal['vNumberBgColor']!);
    this.vTextFgColor = internal['vTextFgColor'].toString() == "auto" ? _defaultThemeVerseStyleFgColor : convertStringColorToColor(internal['vTextFgColor']!);
    this.vTextBgColor = internal['vTextBgColor'].toString() == "auto" ? _defaultThemeVerseStyleBgColor : convertStringColorToColor(internal['vTextBgColor']!);
    this.vCrFgColor = internal['vCrFgColor'].toString() == "auto" ? _defaultThemeVerseStyleFgColor : convertStringColorToColor(internal['vCrFgColor']!);
    this.vCrBgColor = internal['vCrBgColor'].toString() == "auto" ? _defaultThemeVerseStyleBgColor : convertStringColorToColor(internal['vCrBgColor']!);
  }
}


