import 'package:flutter/widgets.dart';
import 'package:bible_multi_the_life/pcommon.dart' as P;
import 'package:diacritic/diacritic.dart' as Diacritic;

class BibleSearchStyleBO {
  late BuildContext context;
  final Map<String, String> mapStyle = Map();
  final Map<String, String> mapColor = Map();

  BibleSearchStyleBO(final BuildContext context) {
    try {
      this.context = context;

      this.mapStyle.clear();
      this.mapColor.clear();

      //-- Colors (Thanks all guys, if you read it)
      //Grimm Fairytale colors
      this.mapColor["GrimmStyleRed0"] = "#690202";
      this.mapColor["GrimmStyleRed1"] = "#822626";
      this.mapColor["GrimmStyleWhite0"] = "#e6ddbc";
      this.mapColor["GrimmStyleWhite1"] = "#ffffff";
      this.mapColor["GrimmStyleGray0"] = "#525252";
      this.mapColor["GrimmStyleBlack0"] = "#262525";
      this.mapColor["GrimmStyleBlack1"] = "#000000";
      this.mapColor["GrimmStyleBlue0"] = "#4A148C";

      //The Life, The Light
      this.mapColor["App0StyleRed0"] = "#fa0201";
      this.mapColor["App0StyleBlue0"] = "#2196f3";
      this.mapColor["App0StyleBlack0"] = "#000000";
      this.mapColor["App0StyleBlack1"] = "#303030";
      this.mapColor["App0StyleBlack2"] = "#424242";
      this.mapColor["App0StyleWhite0"] = "#fafafa";

      //KJV Bible app colors
      this.mapColor["App1StyleColor0"] = "#bd635e";
      this.mapColor["App1StyleColor1"] = "#af8254";
      this.mapColor["App1StyleColor2"] = "#8c6587";
      this.mapColor["App1StyleColor3"] = "#565daa";
      this.mapColor["App1StyleColor4"] = "#4a759c";
      this.mapColor["App1StyleColor5"] = "#e2863f";
      this.mapColor["App1StyleColor6"] = "#e1a239";
      this.mapColor["App1StyleColor7"] = "#6ab06a";
      this.mapColor["App1StyleColor8"] = "#6abdb5";
      this.mapColor["App1StyleColor9"] = "#5ca5da";

      //BLB app colors
      this.mapColor["App2StyleColor0"] = "#ffffa9";
      this.mapColor["App2StyleColor1"] = "#bcffba";
      this.mapColor["App2StyleColor2"] = "#cdddff";
      this.mapColor["App2StyleColor3"] = "#fecccb";
      this.mapColor["App2StyleColor4"] = "#fecca9";
      this.mapColor["App2StyleColor5"] = "#ffaaff";

      //Kaki colors
      this.mapColor["KakiStyleColor0"] = "#56aa60";
      this.mapColor["KakiStyleColor1"] = "#96923f";
      this.mapColor["KakiStyleColor2"] = "#8a512b";
      this.mapColor["KakiStyleColor3"] = "#6f2020";
      this.mapColor["KakiStyleColor4"] = "#4b0a2c";
      this.mapColor["KakiStyleColor5"] = "#880E4F";
      this.mapColor["KakiStyleColor6"] = "#66BB6A";
      this.mapColor["KakiStyleColor7"] = "#81C784";

      //Daffodil bible colors
      this.mapColor["DaffodilStyleColor0"] = "#f4deac";
      this.mapColor["DaffodilStyleColor1"] = "#f0cb7d";
      this.mapColor["DaffodilStyleColor2"] = "#e6ac2e";
      this.mapColor["DaffodilStyleColor3"] = "#949b55";
      this.mapColor["DaffodilStyleColor4"] = "#964b34";
      this.mapColor["DaffodilStyleColor5"] = "#4E342E";
      this.mapColor["DaffodilStyleColor6"] = "#3E2723";
      this.mapColor["DaffodilStyleColor7"] = "#FFC107";

      //-- Styles (in min when possible)
      _createStyle("GrimmStyle0", "GrimmStyleWhite0", "GrimmStyleBlack0");
      _createStyle("GrimmStyle1", "GrimmStyleBlack0", "GrimmStyleWhite0");
      _createStyle("GrimmStyle2", "GrimmStyleWhite0", "GrimmStyleGray0");
      _createStyle("GrimmStyle3", "GrimmStyleWhite0", "GrimmStyleRed0");
      _createStyle("GrimmStyle4", "GrimmStyleWhite0", "GrimmStyleRed1");

      _createStyle("App0Style0a", "App0StyleRed0", "App0StyleWhite0");
      _createStyle("App0Style0ab", "KakiStyleColor5", "App0StyleWhite0");
      _createStyle("App0Style0b", "App0StyleRed0", "App0StyleBlack1");
      _createStyle("App0Style0c", "App0StyleRed0", "App0StyleBlack2");
      _createStyle("App0Style0d", "App0StyleRed0", "App0StyleBlack0");
      _createStyle("App0Style1a", "App0StyleBlue0", "App0StyleWhite0");
      _createStyle("App0Style1b", "App0StyleBlue0", "App0StyleBlack1");
      _createStyle("App0Style1c", "App0StyleBlue0", "App0StyleBlack2");
      _createStyle("App0Style1d", "App0StyleBlue0", "App0StyleBlack0");

      _createStyle("App1Style0a", "GrimmStyleWhite0", "App1StyleColor0");
      _createStyle("App1Style0b", "GrimmStyleBlack0", "App1StyleColor0");
      _createStyle("App1Style1a", "GrimmStyleWhite0", "App1StyleColor1");
      _createStyle("App1Style1b", "GrimmStyleBlack0", "App1StyleColor1");
      _createStyle("App1Style2a", "GrimmStyleWhite0", "App1StyleColor2");
      _createStyle("App1Style2b", "GrimmStyleBlack0", "App1StyleColor2");
      _createStyle("App1Style3a", "GrimmStyleWhite0", "App1StyleColor3");
      _createStyle("App1Style3b", "GrimmStyleBlack0", "App1StyleColor3");
      _createStyle("App1Style4a", "GrimmStyleWhite0", "App1StyleColor4");
      _createStyle("App1Style4b", "GrimmStyleBlack0", "App1StyleColor4");
      _createStyle("App1Style5a", "GrimmStyleBlack1", "App1StyleColor5");
      _createStyle("App1Style5b", "GrimmStyleBlack0", "App1StyleColor5");
      _createStyle("App1Style6a", "GrimmStyleBlue0",  "App1StyleColor6");
      _createStyle("App1Style6b", "GrimmStyleBlack0", "App1StyleColor6");
      _createStyle("App1Style7a", "GrimmStyleWhite0", "App1StyleColor7");
      _createStyle("App1Style7b", "GrimmStyleBlack0", "App1StyleColor7");
      _createStyle("App1Style8a", "GrimmStyleWhite0", "App1StyleColor8");
      _createStyle("App1Style8b", "GrimmStyleBlack0", "App1StyleColor8");
      _createStyle("App1Style9a", "GrimmStyleWhite0", "App1StyleColor9");
      _createStyle("App1Style9b", "GrimmStyleBlack0", "App1StyleColor9");

      _createStyle("App2Style0a", "GrimmStyleBlack0", "App2StyleColor0");
      _createStyle("App2Style1a", "GrimmStyleBlack0", "App2StyleColor1");
      _createStyle("App2Style2a", "GrimmStyleBlack0", "App2StyleColor2");
      _createStyle("App2Style3a", "GrimmStyleBlack0", "App2StyleColor3");
      _createStyle("App2Style4a", "GrimmStyleBlack0", "App2StyleColor4");
      _createStyle("App2Style5a", "GrimmStyleBlack0", "App2StyleColor5");

      _createStyle("KakiStyle0a", "GrimmStyleWhite0", "KakiStyleColor0");
      _createStyle("KakiStyle0b", "GrimmStyleBlack0", "KakiStyleColor0");
      _createStyle("KakiStyle1a", "GrimmStyleBlack1", "KakiStyleColor1");
      _createStyle("KakiStyle1b", "GrimmStyleBlack0", "KakiStyleColor1");
      _createStyle("KakiStyle2a", "GrimmStyleWhite0", "KakiStyleColor2");
      _createStyle("KakiStyle2b", "GrimmStyleWhite1", "KakiStyleColor2");
      _createStyle("KakiStyle3a", "GrimmStyleWhite0", "KakiStyleColor3");
      _createStyle("KakiStyle3b", "App2StyleColor0", "KakiStyleColor3");
      _createStyle("KakiStyle4a", "GrimmStyleWhite0", "KakiStyleColor4");
      _createStyle("KakiStyle4b", "KakiStyleColor6", "KakiStyleColor4");
      _createStyle("KakiStyle4c", "KakiStyleColor7", "KakiStyleColor4");

      _createStyle("DaffodilStyle0a", "GrimmStyleBlack0", "DaffodilStyleColor0");
      _createStyle("DaffodilStyle0b", "DaffodilStyleColor4", "DaffodilStyleColor0");
      _createStyle("DaffodilStyle1a", "GrimmStyleBlack0", "DaffodilStyleColor1");
      _createStyle("DaffodilStyle1b", "DaffodilStyleColor5", "DaffodilStyleColor1");
      _createStyle("DaffodilStyle2a", "GrimmStyleBlack0", "DaffodilStyleColor2");
      _createStyle("DaffodilStyle2b", "DaffodilStyleColor6", "DaffodilStyleColor2");
      _createStyle("DaffodilStyle3a", "DaffodilStyleColor6", "DaffodilStyleColor3");
      _createStyle("DaffodilStyle3b", "GrimmStyleBlack0", "DaffodilStyleColor3");
      _createStyle("DaffodilStyle3c", "GrimmStyleBlack1", "DaffodilStyleColor3");
      _createStyle("DaffodilStyle4a", "GrimmStyleWhite0", "DaffodilStyleColor4");
      _createStyle("DaffodilStyle4b", "DaffodilStyleColor0", "DaffodilStyleColor4");
      _createStyle("DaffodilStyle4c", "DaffodilStyleColor1", "DaffodilStyleColor4");
      _createStyle("DaffodilStyle4d", "DaffodilStyleColor7", "DaffodilStyleColor4");
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  void _createStyle(final String keyStyle, final String keyColorFg, final String keyColorBg) {
    try {
      if (!this.mapStyle.containsKey(keyStyle) && this.mapColor.containsKey(keyColorFg) && this.mapColor.containsKey(keyColorBg)) {
        this.mapStyle[keyStyle] = "$keyColorFg/$keyColorBg";
      } else {
        if (P.isDebug) print("Invalid Style: $keyStyle");
      }
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  String? _getColorFromId(final String keyColor) {
    try {
      return mapColor[keyColor];
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }

    return null;
  }

  int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF$hexColor";
    }
    return int.parse(hexColor, radix: 16);
  }

  /// Get style
  /// @param keyStyle Style Id
  /// @return Map with properties: 'fg', 'bg'. Null if not found
  Map<String, Color>? getStylePropertiesFromId(final String keyStyle) {
    Map<String, Color>? mapProp = null;

    try {
      final String? styleProp = mapStyle[keyStyle];
      if (styleProp == null) return null;

      final List<String> lstColor = styleProp.split("/");
      if (lstColor.length != 2) return null;

      final String? fg = _getColorFromId(lstColor[0]);
      if (fg == null) return null;

      final String? bg = _getColorFromId(lstColor[1]);
      if (bg == null) return null;

      mapProp = Map();
      mapProp["fg"] = Color(_getColorFromHex(fg));
      mapProp["bg"] = Color(_getColorFromHex(bg));
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }

    return mapProp;
  }

  /// Get all styles sorted
  /// @return all styles sorted
  List<String> getAllStyleNames() {
    final List<String> newLstStyle = [];
    try {
      mapStyle.keys.forEach((key) {
        newLstStyle.add(key);
      });
      newLstStyle.sort((a, b) {
        a = Diacritic.removeDiacritics(a);
        b = Diacritic.removeDiacritics(b);
        return a.compareTo(b);
      });
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }

    return newLstStyle;
  }
}
