library bible.db_helper;

import 'dart:io';

import 'package:bible_multi_the_life/bookmarkBO.dart';
import 'package:bible_multi_the_life/cacheTabBO.dart';
import 'package:bible_multi_the_life/dbCommonHelper.dart';
import 'package:bible_multi_the_life/pcommon.dart' as P;
import 'package:bible_multi_the_life/resource.dart' as R;

//region -- History --
//** History **
// PROD: Bible 1.31.0,  DbVersion: 13 2025-02-12
// PROD: Bible 1.30.0,  DbVersion: 12 2025-01-28
// PROD: Bible 1.29,0,  DbVersion: 12 2024-12-08
// PROD: Bible 1.28,0,  DbVersion: 12 2024-12-07
// PROD: Bible 1.27.4,  DbVersion: 12 2024-10-03
// PROD: Bible 1.27.3,  DbVersion: 12 2024-07-25 (F-Droid)
// PROD: Bible 1.27.2,  DbVersion: 11 (Apple)
// PROD: Bible 1.27.1,  DbVersion: 11 (Apple)
// PROD: Bible 1.27.0,  DbVersion: 11 2024-07-05
// PROD: Bible 1.26.0,  DbVersion: 10 2024-05-07
// PROD: Bible 1.25.0,  DbVersion: 10 2023-08-23
// PROD: Bible 1.24.0,  DbVersion: 10 2023-05-30
// PROD: Bible 1.23.0,  DbVersion: 9 2023-03-20
// PROD: Bible 1.22.0,  DbVersion: 8 2022-10-22
// PROD: Bible 1.21.0,  DbVersion: 8 2022-09-19
// PROD: Bible 1.20.0,  DbVersion: 8 2022-08-30
// PROD: Bible 1.19.0,  DbVersion: 8 2022-05-26
// PROD: Bible 1.18.0,  DbVersion: 8 2022-01-16
// PROD: Bible 1.17.0,  DbVersion: 8 2022-01-02
// PROD: Bible 1.16.0,  DbVersion: 8 2022-01-01
// PROD: Bible 1.15.0,  DbVersion: 7 2021-11-18
// PROD: Bible 1.14.1,  DbVersion: 6 2021-10-22
// PROD: Bible 1.14.0,  DbVersion: 6 2021-10-21
// PROD: Bible 1.13.0,  DbVersion: 5 2021-10-10 (Linux version only)
// PROD: Bible 1.12.0,  DbVersion: 5 2021-10-05
// PROD: Bible 1.11.0,  DbVersion: 3 2021-09-23
// PROD: Bible 1.10.0,  DbVersion: 2 2021-03-17
// PROD: Bible 1.9.0,   DbVersion: 2 2020-12-17
// PROD: Bible 1.8.0,   DbVersion: 2 2020-11-11
// PROD: Bible 1.7.0,   DbVersion: 2 2020-11-02
// PROD: Bible 1.6.0,   DbVersion: 2 2020-10-25 (version 2 based on version 52)
// PROD: Bible 1.5.0,   DbVersion: 1 2020-10-19
// PROD: Bible 1.4.0,   DbVersion: 1 2020-10-14
// PROD: Bible 1.3.0,   DbVersion: 1 2020-09-29
// PROD: Bible 1.2.0,   DbVersion: 1 2020-07-26
// PROD: Bible 1.1.0,   DbVersion: 1 2020-03-21
// PROD: Bible 1.0.1,   DbVersion: 1 2020-03-15
// PROD: Bible 1.0.0,   DbVersion: 1 2020-03-15 (version 1 based on version 48)
//** End of History **
//endregion

class DbHelper {
  final int _version = 13; //db version code

  //Singleton
  DbHelper._privateConstructor();
  static final DbHelper instance = DbHelper._privateConstructor();

  //Only a single app-wide reference to the database
  static dynamic _database;
  Future<dynamic> get database async {
    if (_database != null) return _database;

    //if _database is null we instantiate it
    _database = await onInitDatabase(Platform.isLinux ? "bible-multi-the-life.db" : "bible.db");
    return _database;
  }

  Future<void> refreshInstanceDatabase() async {
    _database = null;
  }

  /// Get db version of this code, not the official db version installed
  int getDbVersion() => _version;

  /// Get db version from db (official db version)
  Future<int> getDbVersionOfDb() async {
    try {
      final db = await DbHelper.instance.database;
      final List<Map> maps = await db.rawQuery("PRAGMA user_version");
      return maps.length > 0 ? maps[0]['user_version'] : -1;
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return -1;
  }

  //region -- Cache Tab --
  //** Cache Tab **

  /// Add cache tab
  /// @param c
  Future<void> addCacheTab(final CacheTabBO c) async {
    try {
      final int tabId = c.tabId;
      final String tabType = P.rq(c.tabType);
      final String tabTitle = P.rq(c.tabTitle);
      final String fullQuery = P.rq(c.fullQuery);
      final int scrollPosY = c.scrollPosY;
      final String bbName = P.rq(c.bbName);
      final int isBook = c.isBook;
      final int isChapter = c.isChapter;
      final int isVerse = c.isVerse;
      final int bNumber = c.bNumber;
      final int cNumber = c.cNumber;
      final int vNumber = c.vNumber;
      final String trad = P.rq(c.trad);
      final int orderBy = c.orderBy;
      final int favFilter = c.favFilter;
      final double offset = c.offset;

      final db = await DbHelper.instance.database;
      await db.rawInsert('''INSERT OR REPLACE INTO cacheTab (tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad, orderBy, favFilter, offset)
          VALUES ($tabId, '$tabType', '$tabTitle', '$fullQuery', $scrollPosY, '$bbName', $isBook, $isChapter, $isVerse, $bNumber, $cNumber, $vNumber, '$trad', $orderBy, $favFilter, $offset) ''');
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  /// Update cache tab
  /// @param c
  Future<void> updCacheTab(final CacheTabBO c) async {
    try {
      final int tabId = c.tabId;
      final String tabType = P.rq(c.tabType);
      final String tabTitle = P.rq(c.tabTitle);
      final String fullQuery = P.rq(c.fullQuery);
      final int scrollPosY = c.scrollPosY;
      final String bbName = P.rq(c.bbName);
      final int isBook = c.isBook;
      final int isChapter = c.isChapter;
      final int isVerse = c.isVerse;
      final int bNumber = c.bNumber;
      final int cNumber = c.cNumber;
      final int vNumber = c.vNumber;
      final String trad = P.rq(c.trad);
      final int orderBy = c.orderBy;
      final int favFilter = c.favFilter;
      final double offset = c.offset;

      final db = await DbHelper.instance.database;
      await db.rawUpdate('''UPDATE cacheTab SET 
            tabType='$tabType', 
            tabTitle='$tabTitle',  
            fullQuery='$fullQuery',  
            scrollPosY=$scrollPosY,  
            bbName='$bbName', 
            isBook=$isBook, 
            isChapter=$isChapter,  
            isVerse=$isVerse, 
            bNumber=$bNumber, 
            cNumber=$cNumber,  
            vNumber=$vNumber,  
            trad='$trad', 
            orderBy=$orderBy,
            favFilter=$favFilter,
            offset=$offset   
          WHERE tabId=$tabId''');
    } catch (ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  /// Update cache tab offset
  /// @param c
  Future<void> updCacheTabOffset(final int tabId, final double offset) async {
    try {
      final db = await DbHelper.instance.database;
      await db.rawUpdate('''UPDATE cacheTab SET 
          offset=$offset   
        WHERE tabId=$tabId''');
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  /// Delete cache tab by ID
  /// @param tabId
  Future<void> delCacheTabById(final int tabId) async {
    try {
      final db = await DbHelper.instance.database;
      await db.rawDelete('''DELETE FROM cacheTab WHERE tabId=$tabId''');
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  /// Get cache tab count by type
  /// @param tabType
  /// @return count
  Future<int> getTabCacheCountByType(final String tabType) async {
    int res = -1;
    try {
      final db = await DbHelper.instance.database;
      final List<Map> maps = await db.rawQuery('''SELECT COUNT(*) tot
        FROM cacheTab WHERE tabType='$tabType' ''');
      if (maps.length > 0) {
        res = maps[0]["tot"];
        //was: if (res == null) return -1;
      }
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return res;
  }

  /// Get cache tab by ID
  /// @param tabId
  /// @return cache tab
  Future<Map> getCacheTabById(final int tabId) async {
    try {
      final db = await DbHelper.instance.database;
      final List<Map> maps = await db.rawQuery('''SELECT tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad, orderBy, favFilter, offset  
        FROM cacheTab WHERE tabId=$tabId''');
      return maps.length > 0 ? maps[0] : {};
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return {};
  }

  /// Get max tabId (starts at 0)
  /// @param tabId
  /// @return cache tabId max (-1 in case of error or not found)
  Future<int> getCacheTabIdMax() async {
    int res = -1;
    try {
      final db = await DbHelper.instance.database;
      final List<Map> maps = await db.rawQuery('''SELECT MAX(tabId) max FROM cacheTab WHERE tabId >= 0''');
      if (maps.length > 0) {
        res = maps[0]["max"];
        //was: if (res == null) return -1;
      }
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return res;
  }

  /// Get first cache tab by tabType and name
  /// @param tabType
  /// @param query
  /// @return cache tab
  Future<Map> getFirstCacheTabByQuery(String tabType, String fullQuery, final int favFilter) async {
    try {
      tabType = tabType.startsWith("S") ? "S" : tabType;
      fullQuery = P.rq(fullQuery);
      final String whereCondition = (tabType != "F") ? "tabType='${tabType}' AND fullQuery='${fullQuery}'" : "tabType='${tabType}' AND favFilter=${favFilter} AND fullQuery='${fullQuery}'";
      final db = await DbHelper.instance.database;
      final List<Map> maps = await db.rawQuery('''SELECT tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad, orderBy, favFilter, offset  
        FROM cacheTab WHERE ${whereCondition} ORDER BY tabId ASC LIMIT 1''');
      return maps.length > 0 ? maps[0] : {};
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return {};
  }

  /// Get first cache tab ID by type
  /// @param tabType
  /// @return cache tab
  Future<int> getFirstCacheTabIdByType(final String tabType) async {
    int res = -1;
    try {
      final db = await DbHelper.instance.database;
      final List<Map> maps = await db.rawQuery('''SELECT tabId 
        FROM cacheTab WHERE tabType='$tabType' ORDER BY tabId ASC LIMIT 1''');
      if (maps.length > 0) {
        res = maps[0]["tabId"];
        //was: if (res == null) return -1;
      }
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return res;
  }

  /// Get list of cache tab (some fields)
  /// @return list of cache tab
  Future<List<Map>> getListAllCacheTabForHistory() async {
    try {
      final db = await DbHelper.instance.database;
      final List<Map> maps = await db.rawQuery('''SELECT tabId, tabType, tabTitle, fullQuery, bbName, bNumber, cNumber, vNumber, 0 AS tabTypeOrder, PRINTF('%04d', favFilter) || UPPER(tabTitle) AS crit2Order, favFilter FROM cacheTab WHERE tabType='F' 
      UNION SELECT tabId, tabType, tabTitle, fullQuery, bbName, bNumber, cNumber, vNumber, 1 AS tabTypeOrder, PRINTF('%05d', tabId) AS crit2Order, favFilter FROM cacheTab WHERE tabType<>'F' 
      ORDER BY tabTypeOrder ASC, crit2Order ASC''');
      return maps;
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return [];
  }

  /// Get list of cache tab (only necessary fields for display)
  /// @return list of cache tab
  Future<List<Map>> getListAllCacheTabForScrollTab() async {
    try {
      final db = await DbHelper.instance.database;
      final List<Map> maps = await db.rawQuery('''SELECT tabId, tabType, tabTitle, 0 AS tabTypeOrder, PRINTF('%04d', favFilter) || UPPER(tabTitle) AS crit2Order, favFilter FROM cacheTab WHERE tabType='F' 
      UNION SELECT tabId, tabType, tabTitle, 1 AS tabTypeOrder, PRINTF('%05d', tabId) AS crit2Order, favFilter FROM cacheTab WHERE tabType<>'F' 
      ORDER BY tabTypeOrder ASC, crit2Order ASC''');
      return maps;
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return [];
  }

  //** End of Cache Tab **
  //endregion

  //region -- Cache Search --
  //** Cache Search **

  Future<String> _getSearchBibleSqlBodyClause(final String searchType, final String bbName, final String searchString, final String tbbName, final int searchFavFilter) async {
    String res = "";
    try {
      final String bibleNoteRelation = searchType == "F" ? " INNER JOIN " : " LEFT OUTER JOIN ";
      final String whereClause = await _getSearchBibleSqlWhereClause(bbName, searchString, tbbName, searchType, searchFavFilter);
      return '''INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber 
          $bibleNoteRelation bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber 
          LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber 
          $whereClause''';
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return res;
  }

  Future<String> _getSearchBibleSqlWhereClause(final String bbName, final String searchString, final String tbbName, final String searchType, final int searchFavFilter) async {
    final String bbNameExp = "b.bbName='$bbName'";
    final String favClause = (searchType == "F") ? (searchFavFilter > 0) ? "AND n.mark=${searchFavFilter}" : "" : "";
    final String res = "WHERE b.bNumber=0";

    String mergeWords(final int fromWordPos, final List<String> words) {
      final int toWordPos = words.length - 1;
      String merged = "";
      String word, space;
      for (int i = fromWordPos; i <= toWordPos; i++) {
        word = words[i];
        space = (i != fromWordPos) ? " " : "";
        merged = "$merged$space$word";
      }
      return merged;
    }

    try {
      final RegExp patternDigit = RegExp("^[0-9]+\$");
      int bNumber, cNumber, vNumber, vNumberTo;
      String searchStringExp = "";

      bNumber = cNumber = vNumber = vNumberTo = -1;

      final List<String> words = searchString.split(" ");
      final int wCount = words.length;
      if (wCount == 0) return res;
      //...so minimum is one

      bNumber = patternDigit.hasMatch(words[0]) ? int.parse(words[0]) : await getBookNumberByName(bbName, words[0]);

      if (wCount == 4) {
        //<bNumber> <cNumber> <vNumber> <vNumberTo>
        if (bNumber > 0 && patternDigit.hasMatch(words[1]) && patternDigit.hasMatch(words[2]) && patternDigit.hasMatch(words[3])) {
          cNumber = int.parse(words[1]);
          vNumber = int.parse(words[2]);
          vNumberTo = int.parse(words[3]);

          return '''WHERE $bbNameExp 
                AND b.bNumber=$bNumber 
                AND b.cNumber=$cNumber 
                AND b.vNumber >= $vNumber AND b.vNumber <= $vNumberTo 
                $favClause''';
        }
      }

      if (wCount == 3) {
        //<bNumber> <cNumber> <vNumber>
        if (bNumber > 0 && patternDigit.hasMatch(words[1]) && patternDigit.hasMatch(words[2])) {
          cNumber = int.parse(words[1]);
          vNumber = int.parse(words[2]);

          return '''WHERE $bbNameExp 
                AND b.bNumber=$bNumber 
                AND b.cNumber=$cNumber 
                AND b.vNumber=$vNumber 
                $favClause''';
        }
      }

      if (wCount >= 3) {
        //<bNumber> <cNumber> <expr>...
        if (bNumber > 0 && patternDigit.hasMatch(words[1])) {
          cNumber = int.parse(words[1]);
          searchStringExp = mergeWords(2, words);
          searchStringExp = P.rq('%$searchStringExp%');

          return '''WHERE $bbNameExp 
                AND b.bNumber=$bNumber 
                AND b.cNumber=$cNumber 
                AND b.vText LIKE '$searchStringExp' 
                $favClause''';
        }
      }

      if (wCount == 2) {
        //<bNumber> <cNumber>
        if (bNumber > 0 && patternDigit.hasMatch(words[1])) {
          cNumber = int.parse(words[1]);

          return '''WHERE $bbNameExp 
                AND b.bNumber=$bNumber 
                AND b.cNumber=$cNumber 
                $favClause''';
        }
      }

      if (wCount >= 2) {
        //<bNumber> <expr>...
        if (bNumber > 0) {
          searchStringExp = mergeWords(1, words);
          searchStringExp = P.rq('%$searchStringExp%');

          return '''WHERE $bbNameExp 
                AND b.bNumber=$bNumber 
                AND b.vText LIKE '$searchStringExp' 
                $favClause''';
        }
      }

      //<expr>
      searchStringExp = P.rq('%$searchString%');

      return '''WHERE $bbNameExp 
             $favClause 
             AND b.vText LIKE '$searchStringExp' ''';
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }

    return res;
  }

  /// Based on _getSearchBibleSqlWhereClause,
  Future<Map<String, Object>> getSearchTypeParams(final String searchType, final int searchOrderBy, final String searchStringBBName, final String searchString, final int searchFavFilter) async {
    String _mergeWords(final int fromWordPos, final List<String> words) {
      final int toWordPos = words.length - 1;
      String merged = "";
      String word, space;
      for (int i = fromWordPos; i <= toWordPos; i++) {
        word = words[i];
        space = (i != fromWordPos) ? " " : "";
        merged = "$merged$space$word";
      }
      return merged;
    }

    String _correctSearchStringExpr(final String expr) {
      return expr.replaceAll(RegExp("%%"), "%");
    }

    String _correctQueryExpr(final String expr) {
      return expr.replaceFirst(RegExp("^%"), "").replaceFirst(RegExp("%\$"), "");
    }

    try {
      final RegExp patternDigit = RegExp("^[0-9]+\$");
      int bNumber, cNumber, vNumber, vNumberTo;
      String searchStringExp = "", queryExpr = "";

      bNumber = cNumber = vNumber = vNumberTo = -1;

      final List<String> words = searchString.split(" ");
      final int wCount = words.length;
      if (wCount == 0) return {};
      //...so minimum is one

      bNumber = patternDigit.hasMatch(words[0]) ? int.parse(words[0]) : await getBookNumberByName(searchStringBBName, words[0]);

      if (wCount == 4) {
        //<bNumber> <cNumber> <vNumber> <vNumberTo>
        if (bNumber > 0 && patternDigit.hasMatch(words[1]) && patternDigit.hasMatch(words[2]) && patternDigit.hasMatch(words[3])) {
          cNumber = int.parse(words[1]);
          vNumber = int.parse(words[2]);
          vNumberTo = int.parse(words[3]);

          return {
            "tabType": searchType == "F" ? "F" : "S2",
            "bNumber": bNumber,
            "cNumber": cNumber,
            "vNumber": vNumber,
            "vNumberTo": vNumberTo,
            "favFilter": searchFavFilter,
          };
        }
      }

      if (wCount == 3) {
        //<bNumber> <cNumber> <vNumber>
        if (bNumber > 0 && patternDigit.hasMatch(words[1]) && patternDigit.hasMatch(words[2])) {
          cNumber = int.parse(words[1]);
          vNumber = int.parse(words[2]);

          return {
            "tabType": searchType == "F" ? "F" : "S2",
            "bNumber": bNumber,
            "cNumber": cNumber,
            "vNumber": vNumber,
            "favFilter": searchFavFilter,
          };
        }
      }

      if (wCount >= 3) {
        //<bNumber> <cNumber> <expr>...
        if (bNumber > 0 && patternDigit.hasMatch(words[1])) {
          cNumber = int.parse(words[1]);
          searchStringExp = _mergeWords(2, words);
          searchStringExp = P.rq('%$searchStringExp%');
          searchStringExp = _correctSearchStringExpr(searchStringExp);
          queryExpr = _correctQueryExpr(searchStringExp);

          return {
            "tabType": searchType == "F" ? "F" : "S2",
            "bNumber": bNumber,
            "cNumber": cNumber,
            "sqlvText": searchStringExp,
            "queryExpr": queryExpr,
            "favFilter": searchFavFilter,
          };
        }
      }

      if (wCount == 2) {
        //<bNumber> <cNumber>
        if (bNumber > 0 && patternDigit.hasMatch(words[1])) {
          cNumber = int.parse(words[1]);

          return {
            "tabType": searchType == "F" ? "F" :"S",
            "bNumber": bNumber,
            "cNumber": cNumber,
            "favFilter": searchFavFilter,
          };
        }
      }

      if (wCount >= 2) {
        //<bNumber> <expr>...
        if (bNumber > 0) {
          searchStringExp = _mergeWords(1, words);
          searchStringExp = P.rq('%$searchStringExp%');
          searchStringExp = _correctSearchStringExpr(searchStringExp);
          queryExpr = _correctQueryExpr(searchStringExp);

          return {
            "tabType": searchType == "F" ? "F" : "S2",
            "bNumber": bNumber,
            "sqlvText": searchStringExp,
            "queryExpr": queryExpr,
            "favFilter": searchFavFilter,
          };
        }
      }

      //<expr>
      searchStringExp = P.rq('%$searchString%');
      searchStringExp = _correctSearchStringExpr(searchStringExp);
      queryExpr = _correctQueryExpr(searchStringExp);

      return {
        "tabType": searchType == "F" ? "F" : "S2",
        "sqlvText": searchStringExp,
        "queryExpr": queryExpr,
        "favFilter": searchFavFilter,
      };
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }

    return {};
  }

  /// Get all counts of a full searchBible with following input parameters
  /// @return pageNumberMax, rowCount
  Future<Map> getSearchBibleCount(final String searchType, final String searchStringBBName, final String searchString, final String tbbName, final int searchFavFilter) async {
    Map res = {};
    try {
      final db = await DbHelper.instance.database;
      final String bodyClause = await _getSearchBibleSqlBodyClause(searchType, searchStringBBName, searchString, tbbName, searchFavFilter);
      final String countSql = "SELECT COUNT(*) count FROM (SELECT b.id FROM bible b $bodyClause)";
      final List<Map> countMaps = await db.rawQuery(countSql);
      final int rowCount = countMaps[0]['count'];
      if (P.isDebug) print("rows: $rowCount");
      final int pNumberMax = (rowCount / P.pageRowLimit).ceil() - 1;
      res = {
        "pageNumberMax": pNumberMax < 0 ? 0 : pNumberMax,
        "rowCount": rowCount,
      };
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return Future.value(res);
  }

  Future<List<Map>> searchBible(final String searchType, final int searchOrderBy, final String searchStringBBName, final String searchString, final int pNumber, final String tbbName, final int searchFavFilter) async {
    try {
      final db = await DbHelper.instance.database;
      final int limit = P.pageRowLimit * tbbName.length;
      final int limitStart = pNumber * limit;

      String caseClause = caseBible("b.bbName", tbbName);
      final String whereClause = await _getSearchBibleSqlWhereClause(searchStringBBName, searchString, tbbName, searchType, searchFavFilter);
      final String orderByClause = (searchType != "F" || searchOrderBy == 0)
          ? "ORDER BY b2.bNumber ASC, b2.cNumber ASC, b2.vNumber ASC, bbNameOrder ASC "
          : "ORDER BY n.changeDt DESC, b2.bNumber ASC, b2.cNumber ASC, b2.vNumber ASC, bbNameOrder ASC ";
      final String limitClause = "LIMIT $limit OFFSET $limitStart";
      final String inClause = inBible(tbbName);
      final String bibleNoteRelation = searchType == "F" ? " INNER JOIN " : " LEFT OUTER JOIN ";

      caseClause = caseBible("b2.bbName", tbbName);
      final String dynSql = '''
        SELECT b2.id, b2.bNumber, b2.cNumber, b2.vNumber, b2.vText, r2.bName, r2.bsName, n.mark, b2.bbName, $caseClause, t2.tot 
        FROM bible b 
        INNER JOIN bible b2 ON b2.bbName IN $inClause AND b2.bNumber=b.bNumber AND b2.cNumber=b.cNumber AND b2.vNumber=b.vNumber
        INNER JOIN bibleRef r2 ON r2.bbName=b2.bbName AND r2.bNumber=b2.bNumber
        $bibleNoteRelation bibleNote n ON n.bNumber=b2.bNumber AND n.cNumber=b2.cNumber AND n.vNumber=b2.vNumber
        LEFT OUTER JOIN bibleCrossRefi t2 ON t2.bNumber=b2.bNumber AND t2.cNumber=b2.cNumber AND t2.vNumber=b2.vNumber
        $whereClause  
        $orderByClause 
        $limitClause ''';
      final List<Map> maps = await db.rawQuery(dynSql);
      return maps;
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return [];
  }

  //** End of Cache Search **
  //endregion

  //region -- Bible --
  //** Bible **

  /// Get All bbNames possible
  String getAllBBNamesOfDb() => "k2v9lod1aseiycjrtbhuz";

  /// Get book ref
  /// @param bbName
  /// @return book ref
  Future<Map> getBookRef(final String bbName, final int bNumber) async {
    try {
      final db = await DbHelper.instance.database;
      final List<Map> maps = await db.rawQuery('''SELECT bNumber, bName, bsName FROM bibleRef 
          WHERE bNumber=$bNumber AND bbName='$bbName'  
          ''');
      return maps.length > 0 ? maps[0] : {};
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return {};
  }

  /// Get book number by name
  /// @param bbName
  /// @param bName
  /// @return book number (0 if not found)
  Future<int> getBookNumberByName(final String bbName, String bName) async {
    try {
      final db = await DbHelper.instance.database;
      bName = P.rq(bName);
      final List<Map> maps = await db.rawQuery('''SELECT bNumber from bibleRef WHERE 
                    bbName='$bbName' AND bName='$bName' LIMIT 1''');
      return maps.length > 0 ? maps[0]['bNumber'] : 0;
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return 0;
  }

  /// Get bible chapter info by book
  /// @param bbName
  /// @param bNumber
  /// @return chapter count ('cCount'), verse count of the book ('vCount')
  Future<Map> getBibleCiByBook(final String bbName, final int bNumber) async {
    try {
      final db = await DbHelper.instance.database;
      final List<Map> maps = await db.rawQuery('''SELECT COUNT(b.cNumber) AS cCount, SUM(b.vCount) AS vCount FROM bibleCi b 
          WHERE b.bNumber=$bNumber GROUP BY b.bNumber''');
      return maps.length > 0 ? maps[0] : {};
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return {};
  }

  /// Get list of books by bbName, ordered by bNumber
  /// @param bbName
  /// @param orderByFieldName
  /// @return list all books
  Future<List<Map>> getListAllBookByBBName(final String bbName, final String orderByFieldName) async {
    try {
      final db = await DbHelper.instance.database;
      final List<Map> maps = await db.rawQuery('''SELECT bNumber, bName, bsName from bibleRef 
          WHERE bbName='$bbName' 
          ORDER BY $orderByFieldName ASC''');
      return maps;
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return [];
  }

  Future<Map> getVerse(final int bibleId) async {
    try {
      final db = await DbHelper.instance.database;
      final List<Map> maps = await db.rawQuery('''SELECT b.bbName, b.bNumber, b.cNumber, b.vNumber, b.vText, n.mark, r.bName, r.bsName, t.tot FROM bible b 
        INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber 
        LEFT OUTER JOIN bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber 
        LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber 
        WHERE b.id=$bibleId''');
      return maps.length > 0 ? maps[0] : {};
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return {};
  }

  Future<List<Map>> getVerses(final String tbbName, final int bNumber, final int cNumber, final int vNumberFrom, final int vNumberTo) async {
    try {
      final db = await DbHelper.instance.database;
      final String caseClause = caseBible("b.bbName", tbbName);
      final String inClause = inBible(tbbName);
      final String vNumberToClause = vNumberTo >= vNumberFrom ? " AND b.vNumber <= $vNumberTo" : "";
      final List<Map> maps = await db.rawQuery('''SELECT b.id, b.vNumber, b.vText, r.bName, r.bsName, n.mark, b.bbName, $caseClause, t.tot 
           FROM bible b 
           INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber 
           LEFT OUTER JOIN bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber 
           LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber 
           WHERE b.bbName IN $inClause 
           AND b.bNumber=$bNumber 
           AND b.cNumber=$cNumber 
           AND b.vNumber >= $vNumberFrom 
           $vNumberToClause 
           ORDER BY b.vNumber ASC, bbNameOrder ASC''');
      return maps;
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return [];
  }

  /// Get Id of verses only for bbName
  Future<List<Map>> getListVersesId(final String bbName, final int bNumber, final int cNumber, final int vNumberFrom, final int vNumberTo) async {
    try {
      final db = await DbHelper.instance.database;
      final String vNumberToClause = vNumberTo >= vNumberFrom ? " AND b.vNumber <= $vNumberTo" : "";
      final List<Map> maps = await db.rawQuery('''SELECT b.id 
           FROM bible b 
           WHERE b.bbName='$bbName' 
           AND b.bNumber=$bNumber 
           AND b.cNumber=$cNumber 
           AND b.vNumber >= $vNumberFrom 
           $vNumberToClause  
           ORDER BY b.vNumber ASC''');
      return maps;
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return [];
  }

  Future<List<Map>> getCrossReferences(final String tbbName, final int bNumber, final int cNumber, final int vNumber) async {
    try {
      final db = await DbHelper.instance.database;
      final String caseClause = caseBible("b.bbName", tbbName);
      final String inClause = inBible(tbbName);
      List<Map> maps = [];
      List<Map> mapsTmp = [];
      int i = 0;
      while (i <= 1) {
        if (i == 0) {
          mapsTmp = await db.rawQuery('''SELECT b.id, b.bbName, b.bNumber, b.cNumber, b.vNumber, b.vText, r.bName, r.bsName, n.mark, b.bbName, $caseClause, t.tot 
           FROM bible b 
           INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber 
           LEFT OUTER JOIN bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber 
           LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber 
           WHERE b.bbName IN $inClause 
           AND b.bNumber=$bNumber 
           AND b.cNumber=$cNumber 
           AND b.vNumber=$vNumber 
           ORDER BY b.vNumber ASC, bbNameOrder ASC''');
        } else {
          mapsTmp = await db.rawQuery('''SELECT b.id, b.bbName, b.bNumber, b.cNumber, b.vNumber, b.vText, r.bName, r.bsName, n.mark, b.bbName, $caseClause, t.tot 
           FROM bibleCrossRef c 
           INNER JOIN bible b ON b.bNumber=c.bNumberTo AND b.cNumber=c.cNumberTo AND b.vNumber=c.vNumberTo 
           INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber 
           LEFT OUTER JOIN bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber 
           LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber 
           WHERE b.bbName IN $inClause 
           AND c.bNumberFrom=$bNumber 
           AND c.cNumberFrom=$cNumber 
           AND c.vNumberfrom=$vNumber 
           ORDER BY c.crId ASC, bbNameOrder ASC''');
        }
        //was: if (mapsTmp != null) {
          mapsTmp.forEach((map) {
            maps.add(map);
          });
        //was: }
        i++;
      }
      return maps;
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return [];
  }

  /// Construct CASE clause Bible
  /// @param trad  TRAD
  /// @param fld   Table field to case (ex: b.bbName)
  /// @return string with CASE clause for bbNameOrder
  String caseBible(final String fld, final String trad)
  {
    ///EX: CASE b.bbName WHEN 'f' THEN 1 WHEN 'k' THEN 2 END bbNameOrder
    final int size = trad.length;
    final StringBuffer sb = StringBuffer("CASE $fld");
    for (int i = 0; i < size; i++) {
      sb.write(" WHEN '");
      sb.write(trad[i]);
      sb.write("' THEN ");
      sb.write(i + 1);
    }
    sb.write(" END bbNameOrder");

    return sb.toString();
  }
  /*
  /// Construct CASE clause Bible
  /// @param trad  TRAD
  /// @param fld   Table field to case (ex: b.bbName)
  /// @return string with CASE clause for bbNameOrder
  String caseBible(final String fld, final String trad)
  {
    return "' ' AS bbNameOrder";
  }*/

  /// Construct IN clause Bible with ( ).
  /// @param str   Trad
  /// @return string for IN clause
  /// Rem: there is no check of the content, '". Works only for chars.
  String inBible(final String str) {
    final int size = str.length;
    if (size <= 1) return "('$str')";

    final StringBuffer sb = StringBuffer('(');
    for (int i = 0; i < size; i++) {
      if (sb.length > 1) sb.write(',');
      sb.write("'");
      sb.write(str[i]);
      sb.write("'");
    }
    sb.write(")");

    return sb.toString();
  }

  Future<void> manageFavorite(final int bibleId, final int action, final int mark) async {
    try {
      final Map verse = await getVerse(bibleId);
      if (verse.isEmpty) return;

      final int bNumber = verse["bNumber"];
      final int cNumber = verse["cNumber"];
      final int vNumber = verse["vNumber"];
      final db = await DbHelper.instance.database;

      if (action < 0) {
        await db.rawDelete('''DELETE FROM bibleNote WHERE bNumber=$bNumber AND cNumber=$cNumber AND vNumber=$vNumber''');
      } else {
        final String changeDt = P.nowYYYYMMDD();
        await db.rawInsert('''INSERT OR REPLACE INTO bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) 
        VALUES ($bNumber, $cNumber, $vNumber, '$changeDt', $mark, '') ''');
      }
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

//** End of Bible **
//endregion

  //region -- Bookmark --
  //** Bookmark **

  Future<Map> getBookmarkById(final int bmId) async {
    try {
      final db = await DbHelper.instance.database;
      final List<Map> maps = await db.rawQuery('SELECT bmId, bmCurrent, bmDesc, bmDev1, bmDev2, bmDev3, bmPrev1, bmPrev2, bmPrev3, bmPrev4, bmPrev5 FROM bookmark WHERE bmId=${bmId}');
      return maps.length > 0 ? maps[0] : {};
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return {};
  }

  /// Get max bmId
  /// @return bmId max (-1 in case of error or not found)
  Future<int> getBookmarkIdMax() async {
    int res = -1;
    try {
      final db = await DbHelper.instance.database;
      final List<Map> maps = await db.rawQuery('''SELECT MAX(bmId) max FROM bookmark''');
      if (maps.length > 0) {
        res = maps[0]["max"];
        //was: if (res == null) return -1;
      }
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return res;
  }

  /// Don't directly call this. Call R.setFav() instead
  Future<Map> generateBookmarkMap() async {
    try {
      int key;
      final Map mapBm = {};
      final db = await DbHelper.instance.database;
      final List<Map> lstBm = await db.rawQuery('''SELECT bmId, bmCurrent, bmDesc FROM bookmark ORDER BY bmId ASC''');
      lstBm.forEach((row) {
        key = row['bmId'];
        mapBm[ key ] = {
          'bmCurrent': row['bmCurrent'],
          'bmDesc': key >= P.bookmarkIdDevLimit ? row['bmDesc'] : R.getStringByName('fav${key}')
        };
      });
      return mapBm;
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return {};
  }

  Future<void> replaceBookmark(final BookmarkBO b) async {
    try {
      final String bmCurrent = P.rq(b.bmCurrent);
      final String bmDesc = P.rq(b.bmDesc);
      final String bmDev1 = P.rq(b.bmDev1);
      final String bmDev2 = P.rq(b.bmDev2);
      final String bmDev3 = P.rq(b.bmDev3);
      final String bmPrev1 = P.rq(b.bmPrev1);
      final String bmPrev2 = P.rq(b.bmPrev2);
      final String bmPrev3 = P.rq(b.bmPrev3);
      final String bmPrev4 = P.rq(b.bmPrev4);
      final String bmPrev5 = P.rq(b.bmPrev5);

      final db = await DbHelper.instance.database;
      final String sql = '''INSERT OR REPLACE INTO bookmark (bmId, bmCurrent, bmDesc, bmDev1, bmDev2, bmDev3, bmPrev1, bmPrev2, bmPrev3, bmPrev4, bmPrev5) VALUES ( 
           ${b.bmId},
           '${bmCurrent}',
           '${bmDesc}',
           '${bmDev1}',
           '${bmDev2}',
           '${bmDev3}',
           '${bmPrev1}',
           '${bmPrev2}',
           '${bmPrev3}',
           '${bmPrev4}',
           '${bmPrev5}') ''';
      await db.rawInsert(sql);
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  //** End of Bookmark **
//endregion
}
