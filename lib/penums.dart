
library bible.penums;

enum BBLocale {
  EN,
  ES,
  FR,
  IT,
  PT,
  DE,
  IN,
  AR,
  CN,
  JP,
  RU,
  TR,
  BD,
  TZ,
  RO,
  PL
}

enum TopBarMenuType {
  NONE,
  BPA,
  MULTI,
  LANGUAGE
}

enum ClipboardAddType {
  VERSE,
  CHAPTER
}

enum SelectionType {
  SINGLE,
  MULTI,
  ALT
}

enum HighlightType {
  NO_STYLE,
  FILLED,
  SQUARED
}

enum ShortcutKeys {
  ESC,
  LEFT,
  RIGHT,
  M,
  TAB,
  S,
  F,
  B,
  A,
  P
}

/*
enum ChapterViewType {
  LIST, GRID
}
*/
