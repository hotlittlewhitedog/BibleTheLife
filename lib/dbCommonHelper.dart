library bible.db_common_helper;

import 'dart:io';

import 'package:bible_multi_the_life/dbHelper.dart';
import 'package:bible_multi_the_life/dbBakHelper.dart';
import 'package:bible_multi_the_life/pcommon.dart' as P;
import 'package:flutter/services.dart';
import 'package:path/path.dart';

import 'package:sqflite/sqflite.dart' as SQFLITE;
import 'package:sqflite_common_ffi/sqflite_ffi.dart' as SQFLITE_FFI;

onInitDatabase(final String db) async {
  try {
    final String databasesPath = await _getInternalDatabasePath();
    var fullPathDbOut = join(databasesPath, db);
    if (P.isDebug) print("platform: ${Platform.operatingSystem}\n$db path: $fullPathDbOut");

    return (Platform.isLinux)
        ? await SQFLITE_FFI.databaseFactoryFfi.openDatabase(fullPathDbOut)
        : await SQFLITE.openDatabase(fullPathDbOut);
  } catch(ex, stacktrace) {
    if (P.isDebug) P.printLog(ex, stacktrace);
  }
  return null;
}

Future<bool> onUpdateDatabase() async {

  bool willCreateDb = false;
  bool willUpdateDb = false;

  Future<void> _createDatabase(final String path) async {
    //Should happen only the first time you launch your application
    if (P.isDebug) print("Creating new copy from asset");

    //Make sure the parent directory exists
    try {
      await Directory(dirname(path)).create(recursive: true);   //TODO: problem of Mkdir on Linux
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }

    //Copy from asset
    final ByteData data = await rootBundle.load(join("assets", "db", "bible.db"));
    final List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

    //Write and flush the bytes written
    await File(path).writeAsBytes(bytes, flush: true);

    //Set version
    final dal = DbHelper.instance;
    final int dbVersion = dal.getDbVersion();
    var db = Platform.isLinux
        ? await SQFLITE_FFI.databaseFactoryFfi.openDatabase(path)
        : await SQFLITE.openDatabase(path);
    db.setVersion(dbVersion);
    db.close();
  }

  Future<void> _createDefaultPreferences() async {
    try {
      //TODO: to remove bookNumber from prefs
      //TODO: to remove chapterNumber from prefs
      //TODO: to remove layout_dynamic2..7 from prefs
      await P.Prefs.saveThemeName('DARK');
      await P.Prefs.saveBibleLocale('EN');
      await P.Prefs.saveBibleName('k');
      await P.Prefs.saveBibleAltLocale('EN');
      await P.Prefs.saveFontName('AveriaGruesaLibre.regular');
      await P.Prefs.saveFontSize('20.0');
      await P.Prefs.saveTabSelected('0');
      await P.Prefs.saveLayoutDynamic1('1');
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  //_onUpdateDatabase
  try {
    final String databasesPath = await _getInternalDatabasePath();
    final String pathBibleDb = join(databasesPath, Platform.isLinux ? "bible-multi-the-life.db" : "bible.db");
    final String pathBibleBakDb = join(databasesPath, "biblebak.db");

    //Check if the database exists
    final bool exists = Platform.isLinux
        ? await SQFLITE_FFI.databaseFactoryFfi.databaseExists(pathBibleDb)
        : await SQFLITE.databaseExists(pathBibleDb);
    if (exists) {
      final dal = DbHelper.instance;
      final int dbVersionOfCode = await dal.getDbVersion();
      final int dbVersionOfDb = await dal.getDbVersionOfDb();
      if (dbVersionOfDb <= 0) {
        willUpdateDb = false;
        willCreateDb = false;
      } else if (dbVersionOfCode > dbVersionOfDb) {
        willUpdateDb = true;
        willCreateDb = true;
      }
      if (P.isDebug) print("DbVersion of code: $dbVersionOfCode, of db: $dbVersionOfDb\nDb to create: $willCreateDb, to update: $willUpdateDb");
      if (willUpdateDb) {
        //Create db BAK
        if (Platform.isLinux) {
          await SQFLITE_FFI.databaseFactoryFfi.deleteDatabase(pathBibleBakDb);
        } else {
          await SQFLITE.deleteDatabase(pathBibleBakDb);
        }

        const Map struct = {
          "CREATE/CACHETAB/5":  "tabId INTEGER NOT NULL, tabType TEXT CHECK(tabType='S' OR tabType='F' or tabType='A' or tabType='P'), tabTitle TEXT NOT NULL, fullQuery TEXT NOT NULL, scrollPosY INTEGER NOT NULL, bbName TEXT NOT NULL, isBook INTEGER NOT NULL, isChapter INTEGER NOT NULL, isVerse INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, trad TEXT, PRIMARY KEY (tabId)",
          "CREATE/CACHETAB/6":  "tabId INTEGER NOT NULL, tabType TEXT CHECK(tabType='S' OR tabType='F' or tabType='A' or tabType='P'), tabTitle TEXT NOT NULL, fullQuery TEXT NOT NULL, scrollPosY INTEGER NOT NULL, bbName TEXT NOT NULL, isBook INTEGER NOT NULL, isChapter INTEGER NOT NULL, isVerse INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, trad TEXT, orderBy INTEGER DEFAULT 0, PRIMARY KEY (tabId)",
          "CREATE/CACHETAB/7":  "tabId INTEGER NOT NULL, tabType TEXT CHECK(tabType='S' OR tabType='F' or tabType='A' or tabType='P'), tabTitle TEXT NOT NULL, fullQuery TEXT NOT NULL, scrollPosY INTEGER NOT NULL, bbName TEXT NOT NULL, isBook INTEGER NOT NULL, isChapter INTEGER NOT NULL, isVerse INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, trad TEXT, orderBy INTEGER DEFAULT 0, PRIMARY KEY (tabId)",
          "CREATE/CACHETAB/8":  "tabId INTEGER NOT NULL, tabType TEXT CHECK(tabType='S' OR tabType='F' or tabType='A' or tabType='P'), tabTitle TEXT NOT NULL, fullQuery TEXT NOT NULL, scrollPosY INTEGER NOT NULL, bbName TEXT NOT NULL, isBook INTEGER NOT NULL, isChapter INTEGER NOT NULL, isVerse INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, trad TEXT, orderBy INTEGER DEFAULT 0, favFilter INTEGER DEFAULT 0, PRIMARY KEY (tabId)",
          "CREATE/CACHETAB/9":  "tabId INTEGER NOT NULL, tabType TEXT CHECK(tabType='S' OR tabType='F' or tabType='A' or tabType='P'), tabTitle TEXT NOT NULL, fullQuery TEXT NOT NULL, scrollPosY INTEGER NOT NULL, bbName TEXT NOT NULL, isBook INTEGER NOT NULL, isChapter INTEGER NOT NULL, isVerse INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, trad TEXT, orderBy INTEGER DEFAULT 0, favFilter INTEGER DEFAULT 0, PRIMARY KEY (tabId)",
          "CREATE/CACHETAB/10": "tabId INTEGER NOT NULL, tabType TEXT CHECK(tabType='S' OR tabType='F' or tabType='A' or tabType='P'), tabTitle TEXT NOT NULL, fullQuery TEXT NOT NULL, scrollPosY INTEGER NOT NULL, bbName TEXT NOT NULL, isBook INTEGER NOT NULL, isChapter INTEGER NOT NULL, isVerse INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, trad TEXT, orderBy INTEGER DEFAULT 0, favFilter INTEGER DEFAULT 0, PRIMARY KEY (tabId)",
          "CREATE/CACHETAB/11": "tabId INTEGER NOT NULL, tabType TEXT CHECK(tabType='S' OR tabType='F' or tabType='A' or tabType='P'), tabTitle TEXT NOT NULL, fullQuery TEXT NOT NULL, scrollPosY INTEGER NOT NULL, bbName TEXT NOT NULL, isBook INTEGER NOT NULL, isChapter INTEGER NOT NULL, isVerse INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, trad TEXT, orderBy INTEGER DEFAULT 0, favFilter INTEGER DEFAULT 0, PRIMARY KEY (tabId)",
          "CREATE/CACHETAB/12": "tabId INTEGER NOT NULL, tabType TEXT CHECK(tabType='S' OR tabType='F' or tabType='A' or tabType='P'), tabTitle TEXT NOT NULL, fullQuery TEXT NOT NULL, scrollPosY INTEGER NOT NULL, bbName TEXT NOT NULL, isBook INTEGER NOT NULL, isChapter INTEGER NOT NULL, isVerse INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, trad TEXT, orderBy INTEGER DEFAULT 0, favFilter INTEGER DEFAULT 0, PRIMARY KEY (tabId)",
          "CREATE/CACHETAB/13": "tabId INTEGER NOT NULL, tabType TEXT CHECK(tabType='S' OR tabType='F' or tabType='A' or tabType='P'), tabTitle TEXT NOT NULL, fullQuery TEXT NOT NULL, scrollPosY INTEGER NOT NULL, bbName TEXT NOT NULL, isBook INTEGER NOT NULL, isChapter INTEGER NOT NULL, isVerse INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, trad TEXT, orderBy INTEGER DEFAULT 0, favFilter INTEGER DEFAULT 0, offset REAL DEFAULT 0.0, PRIMARY KEY (tabId)",
          "SELECT/CACHETAB/5":  "tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad",
          "SELECT/CACHETAB/6":  "tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad, orderBy",
          "SELECT/CACHETAB/7":  "tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad, orderBy",
          "SELECT/CACHETAB/8":  "tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad, orderBy, favFilter",
          "SELECT/CACHETAB/9":  "tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad, orderBy, favFilter",
          "SELECT/CACHETAB/10": "tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad, orderBy, favFilter",
          "SELECT/CACHETAB/11": "tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad, orderBy, favFilter",
          "SELECT/CACHETAB/12": "tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad, orderBy, favFilter",
          "SELECT/CACHETAB/13": "tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad, orderBy, favFilter, offset",
        };
        final int oldDbVersionCacheTab = dbVersionOfDb <= 5 ? 5 : dbVersionOfDb;
        final String createCacheTab = struct["CREATE/CACHETAB/${oldDbVersionCacheTab}"];
        final String selectCacheTab = struct["SELECT/CACHETAB/${oldDbVersionCacheTab}"];

        final dbBak = await DbBakHelper.instance.database;
        await dbBak.execute("CREATE TABLE cacheTab (${createCacheTab})");
        await dbBak.execute("CREATE TABLE bibleNote (bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, changeDt TEXT NOT NULL, mark INTEGER CHECK(mark >= 1), note TEXT NOT NULL, PRIMARY KEY (bNumber, cNumber, vNumber))");
        await dbBak.execute("CREATE TABLE bookmark (bmId INTEGER NOT NULL, bmCurrent TEXT NOT NULL, bmDesc TEXT NOT NULL, bmDev1 TEXT NOT NULL, bmDev2 TEXT DEFAULT '', bmDev3 TEXT DEFAULT '', bmPrev1 TEXT DEFAULT '', bmPrev2 TEXT DEFAULT '', bmPrev3 TEXT DEFAULT '', bmPrev4 TEXT DEFAULT '', bmPrev5 TEXT DEFAULT '', PRIMARY KEY(bmId))");

        //Backup (cacheTab, bibleNote, bookmark) ORIG => BAK
        final dbOrig = await DbHelper.instance.database;
        await dbOrig.execute('ATTACH DATABASE \'$pathBibleBakDb\' AS BIBLEBAKDB');
        await dbOrig.execute('INSERT INTO BIBLEBAKDB.cacheTab (${selectCacheTab}) SELECT ${selectCacheTab} FROM cacheTab');
        await dbOrig.execute('INSERT INTO BIBLEBAKDB.bibleNote SELECT * FROM bibleNote');
        await dbOrig.execute('INSERT INTO BIBLEBAKDB.bookmark SELECT * FROM bookmark');

        //Create db
        await _createDatabase(pathBibleDb);
        await dal.refreshInstanceDatabase();

        //Restore db (cacheTab, bibleNote, bookmark) BAK => ORIG
        await dbBak.execute('ATTACH DATABASE \'$pathBibleDb\' AS BIBLEDB');
        await dbBak.execute('INSERT OR REPLACE INTO BIBLEDB.cacheTab (${selectCacheTab}) SELECT ${selectCacheTab} FROM cacheTab');
        await dbBak.execute('INSERT OR REPLACE INTO BIBLEDB.bibleNote SELECT * FROM bibleNote');
        await dbBak.execute('INSERT OR REPLACE INTO BIBLEDB.bookmark SELECT * FROM bookmark');

        //Remove BAK
        if (dbBak.isOpen) await dbBak.close();

        if (Platform.isLinux) {
          await SQFLITE_FFI.databaseFactoryFfi.deleteDatabase(pathBibleBakDb);
        } else {
          await SQFLITE.deleteDatabase(pathBibleBakDb);
        }
      }
    } else {
      willCreateDb = true;
      await _createDatabase(pathBibleDb);
      await _createDefaultPreferences();
    }
  } catch(ex, stacktrace) {
    if (P.isDebug) P.printLog(ex, stacktrace);
  }
  return willCreateDb;
}

Future<String> _getInternalDatabasePath() async {
  try {
    if (Platform.isLinux) {
      final Map env = Platform.environment;
      final String home = env["HOME"];
      final bool isFlatpak = env.containsKey("XDG_DATA_HOME");
      final String linuxPath = isFlatpak ? env["XDG_DATA_HOME"] : join(home, ".local/share/bible-multi-the-life");
      await SQFLITE_FFI.databaseFactoryFfi.setDatabasesPath(linuxPath);
    }
  } catch(ex, stacktrace) {
    if (P.isDebug) P.printLog(ex, stacktrace);
  }
  return (Platform.isLinux)
      ? await SQFLITE_FFI.databaseFactoryFfi.getDatabasesPath()
      : await SQFLITE.getDatabasesPath();
}

/* For testing only to populate DBORIG
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (3,1,3,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (3,1,4,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (3,1,5,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (3,1,10,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (3,1,11,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (3,1,12,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (3,1,13,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (3,1,14,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (3,1,15,'20200101',1,'');

insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (5,1,3,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (5,1,4,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (5,1,5,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (5,1,10,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (5,1,11,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (5,1,12,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (5,1,13,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (5,1,14,'20200101',1,'');
insert into bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) values (5,1,15,'20200101',1,'');

insert into cacheTab (tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad)
*/