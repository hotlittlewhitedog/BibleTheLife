library bible.db_bak_helper;

import 'package:bible_multi_the_life/dbCommonHelper.dart';

class DbBakHelper {
  //Singleton
  DbBakHelper._privateConstructor();

  static final DbBakHelper instance = DbBakHelper._privateConstructor();

  //Only a single app-wide reference to the database
  static dynamic _database;
  Future<dynamic> get database async {
    if (_database != null) return _database;

    //if _database is null we instantiate it
    _database = await onInitDatabase("biblebak.db");
    return _database;
  }

  void refreshInstanceDatabase() {
    _database = null;
  }
}
