library bible.cache_tab_bo;

class CacheTabBO {
  late int tabId;
  late String tabType;
  late String tabTitle;
  late String fullQuery;
  late int scrollPosY;
  late String bbName;
  late int isBook;
  late int isChapter;
  late int isVerse;
  late int bNumber;
  late int cNumber;
  late int vNumber;
  late String trad;
  late int orderBy;
  late int favFilter;
  late double offset;
}
