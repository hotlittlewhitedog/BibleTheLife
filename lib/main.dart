import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bible_multi_the_life/art.dart';
import 'package:bible_multi_the_life/bibleSearchStyleBO.dart';
import 'package:bible_multi_the_life/bookTitle.dart';
import 'package:bible_multi_the_life/bookmarkBO.dart';
import 'package:bible_multi_the_life/cacheTabBO.dart';
import 'package:bible_multi_the_life/dbCommonHelper.dart';
import 'package:bible_multi_the_life/dbHelper.dart';
import 'package:bible_multi_the_life/pcommon.dart' as P;
import 'package:bible_multi_the_life/penums.dart';
import 'package:bible_multi_the_life/prbl.dart';
import 'package:bible_multi_the_life/pstyle.dart';
import 'package:bible_multi_the_life/resource.dart' as R;
import 'package:diacritic/diacritic.dart' as Diacritic;
import 'package:icon_decoration/icon_decoration.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hotkey_manager/hotkey_manager.dart' as HK;
import 'package:overlay_support/overlay_support.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart' as HTML;

double gAppBarHeight = 0.0;

void main() async {
  if (Platform.isMacOS || Platform.isLinux) {
    //Must add this line.
    WidgetsFlutterBinding.ensureInitialized();
    await HK.hotKeyManager.unregisterAll(); //For hot reload, `unregisterAll()` needs to be called
  } else if (Platform.isAndroid) {
    //Nothing actually as HK have been disabled
  }

  runApp(App());
}

/*
class ThemeModel extends ChangeNotifier {
  void setThemeAndInformListeners() async {
    notifyListeners();
  }
}
 */

class App extends StatefulWidget {
  App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  ThemeData? _appThemeData;

  static _AppState? of(BuildContext context) => context.findAncestorStateOfType<_AppState>();

  Future<void> _getAppTheme() async {
    ThemeData? _newThemeData;
    final Future<ThemeData> _newThemeDataFut = P.getThemeData();
    _newThemeDataFut.then((value) => _newThemeData = value);
    _newThemeDataFut.whenComplete(() => setState((){
      _appThemeData = _newThemeData!;
    }));
  }

  void _setAppThemeDataOnInit() async {
    final String themeName = await P.Prefs.getThemeName;
    if (P.isDebug) print("App::InitState::getThemeName: '${themeName}'");
    setState(() {
      _appThemeData = (themeName.compareTo('LIGHT') == 0) ? PStyle.instance.lightThemeData : PStyle.instance.darkThemeData;
    });
  }

  @override
  void initState() {
    super.initState();
    _setAppThemeDataOnInit();
  }

  @override
  Widget build(BuildContext context) {
    if (_appThemeData == null) _appThemeData = PStyle.instance.darkThemeData;
    if (Platform.isAndroid) SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge);
    return SafeArea(
      left: true,
      top: true,
      right: true,
      bottom: true,
      minimum: EdgeInsets.only(left: 0.0, top: 0.0, right: 0.0, bottom: 0.0),
      child: OverlaySupport.global(
        child: MaterialApp(
          home: HomePage(title: 'The Life'),
          title: 'The Life',
          theme: _appThemeData,
          debugShowCheckedModeBanner: false,
        ),
        toastTheme: ToastThemeData(textColor: Colors.white, background: Colors.black, alignment: Alignment.bottomCenter),
      ),
    );
  }
}

class OptionPage extends StatefulWidget {
  ///lstItemCheckedOrigin: list of int (-1:disabled, >= -1:true, >= 100:false) or null
  ///@return List<int> when MULTI or int for other cases
  OptionPage({Key? key, required this.titlePage, required this.lstItem, this.lstItemLocale=const Locale('en'),
    required this.isAlignmentStart, required this.topBarMenuType, required this.lstItemCheckedOrig, required this.actionStyle, required this.layoutType, required this.columns})
      : super(key: key);

  final String titlePage;
  final List<TextSpan> lstItem;
  final Locale lstItemLocale;
  final List<int> lstItemCheckedOrig; //Used with MULTI only
  final bool isAlignmentStart;
  final TopBarMenuType topBarMenuType;
  final VerseStyle? actionStyle;
  final int layoutType; //1=linear, 2=grid
  final int columns; //number of columns

  @override
  _OptionPageState createState() => _OptionPageState();
}

class _OptionPageState extends State<OptionPage> {
  List<bool> lstItemCheckedState = []; //Used with MULTI only
  List<int> lstItemCheckedRes = []; //Result of ordered list

  @override
  void initState() {
    super.initState();
    if (widget.topBarMenuType == TopBarMenuType.MULTI) {
      int index = 0;
      widget.lstItemCheckedOrig.forEach((element) {
        lstItemCheckedState.add(element >= -1 && element < 100);
        if (element >= -1 && element < 100) {
          lstItemCheckedRes.add(index);
        }
        index++;
      });
    }
  }

  //For build
  //ItemIndex starts at 0
  void _onItemSelected(final int itemIndex) {
    if (widget.topBarMenuType == TopBarMenuType.MULTI) {
      final bool isSelected = !lstItemCheckedState[itemIndex];
      if (isSelected) {
        lstItemCheckedRes.add(itemIndex);
      } else {
        lstItemCheckedRes.remove(itemIndex);
      }
      setState(() {
        lstItemCheckedState[itemIndex] = isSelected;
      });
      return;
    }

    lstItemCheckedRes.add(itemIndex);
    final Future<int> itemResFut = Future.value(itemIndex);
    Navigator.of(context).pop(itemResFut);
  }

  void _onSaveMultiItemSelected() {
    final Future<List<int>> itemResFut = Future.value(lstItemCheckedRes);
    Navigator.of(context).pop(itemResFut);
  }

  Widget? _onCreateCell(final int index) {
    final Alignment? alignment = widget.isAlignmentStart
        ? (widget.lstItemLocale.languageCode == 'ar')
            ? Alignment(1.0, -1.0)
            : Alignment(-1.0, -1.0)
        : null;
    final TextDirection dir = widget.lstItem[index].locale != null && widget.lstItem[index].locale?.languageCode == 'ar' ? TextDirection.rtl : TextDirection.ltr;

    return (widget.topBarMenuType == TopBarMenuType.MULTI)
        ? CheckboxListTile(
            activeColor: (widget.actionStyle == null) ? null : widget.actionStyle?.accentFgColor,
            value: lstItemCheckedState[index],
            title: Container(
              alignment: alignment,
              child: RichText(overflow: TextOverflow.ellipsis, textAlign: TextAlign.start, textDirection: dir, text: widget.lstItem[index]),
            ),
            onChanged: widget.lstItemCheckedOrig[index] == -1 ? null : (changed) => _onItemSelected(index),
          )
        : widget.isAlignmentStart
            ? TextButton(
                child: Container(
                  alignment: alignment,
                  child: RichText(overflow: TextOverflow.ellipsis, textAlign: TextAlign.start, textDirection: dir, text: widget.lstItem[index]),
                ),
                onPressed: () => _onItemSelected(index),
              )
            : TextButton(
                child: Container(
                  alignment: alignment,
                  child: RichText(overflow: TextOverflow.ellipsis, textAlign: TextAlign.start, textDirection: dir, text: widget.lstItem[index]),
                ),
                onPressed: () => _onItemSelected(index),
              );
  }

  //End build

  @override
  Widget build(BuildContext context) {
    final int itemCount = widget.lstItem.isEmpty ? 0 : widget.lstItem.length;
    final List<Widget>? actions = widget.topBarMenuType == TopBarMenuType.BPA
        ? <Widget>[
            IconButton(
              icon: const DecoratedIcon(
                icon: const Icon(Icons.book, color: Colors.greenAccent),
                decoration: IconDecoration(border: IconBorder()),
              ),
              tooltip: R.getString(R.id.mnuBooks),
              onPressed: () async {
                _onItemSelected(-1);
              },
            ),
            IconButton(
              icon: const DecoratedIcon(
                icon: const Icon(Icons.assignment, color: Colors.yellow),
                decoration: IconDecoration(border: IconBorder()),
              ),
              tooltip: R.getString(R.id.mnuPrbls),
              onPressed: () async {
                _onItemSelected(-2);
              },
            ),
            IconButton(
              icon: const DecoratedIcon(
                icon: const Icon(Icons.short_text, color: Colors.orangeAccent),
                decoration: IconDecoration(border: IconBorder()),
              ),
              tooltip: R.getString(R.id.mnuArts),
              onPressed: () async {
                _onItemSelected(-3);
              },
            ),
          ]
        : widget.topBarMenuType == TopBarMenuType.MULTI
            ? <Widget>[
                IconButton(
                    icon: const Icon(Icons.done),
                    tooltip: R.getString(R.id.mnuSave),
                    onPressed: () async {
                      _onSaveMultiItemSelected();
                    }),
              ]
            : null;

    if (widget.layoutType == 1) {
      final lstViewBuilder = Expanded(
        child: ListView.builder(
          padding: EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0, bottom: 10.0),
          physics: ScrollPhysics(),
          shrinkWrap: false,
          itemCount: itemCount,
          itemBuilder: (context, index) => _onCreateCell(index)
        ),
      );

      return Scaffold(
        appBar: AppBar(
          title: Text(widget.titlePage),
          toolbarHeight: gAppBarHeight,
          actions: actions,
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              lstViewBuilder,
            ],
          ),
        ),
      );
    } else {
      final gridViewBuilder = GridView.builder(
          padding: EdgeInsets.all(10.0),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: widget.columns,
          ),
          itemCount: itemCount,
          itemBuilder: (context, index) => _onCreateCell(index));

      return Scaffold(
        appBar: AppBar(
          title: Text(widget.titlePage),
          toolbarHeight: gAppBarHeight,
          actions: actions,
        ),
        body: gridViewBuilder,
      );
    }
  }
}

class HistoryPage extends StatefulWidget {
  HistoryPage({Key? key, required this.titlePage, required this.lstItem, required this.lstId, required this.isAlignmentStart, required this.topBarMenuType, required this.actionStyle})
      : super(key: key);

  final String titlePage;
  final List<TextSpan> lstItem;
  final List<int> lstId;
  final bool isAlignmentStart;
  final TopBarMenuType topBarMenuType;
  final VerseStyle actionStyle;

  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  final Map<int, int> mapRemovedId = Map(); //index, id
  final dal = DbHelper.instance;
  bool isDeleteMode = false;

  late int itemCount;
  Alignment? alignment;

  void _onRemoveItem(final int index) {
    if (isDeleteMode) {
      setState(() {
        if (mapRemovedId.containsKey(index)) {
          mapRemovedId.remove(index);
        } else {
          mapRemovedId[index] = widget.lstId[index];
        }
      });
    }
  }

  Future<void> _onLongPressItem(final int index) async {
    isDeleteMode = !isDeleteMode;
    _onRemoveItem(index);
  }

  Future<void> _onRemoveItems() async {
    try {
      if (mapRemovedId.length == 0) return;
      mapRemovedId.forEach((k, v) async {
        await dal.delCacheTabById(v);
      });
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  void _onRemoveItemsCompleted() {
    if (mapRemovedId.length == 0) return;
    final Future<int> itemResFut = Future.value(-1);
    Navigator.of(context).pop(itemResFut);
  }

  void _onOpenItem(final int index) {
    if (isDeleteMode) {
      _onRemoveItem(index);
    } else {
      final Future<int> itemResFut = Future.value(index);
      Navigator.of(context).pop(itemResFut);
    }
  }

  @override
  void initState() {
    super.initState();
    itemCount = widget.lstItem.isEmpty ? 0 : widget.lstItem.length;
    alignment = widget.isAlignmentStart ? Alignment(-1.0, -1.0) : null;
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget>? actions = isDeleteMode
        ? <Widget>[
            IconButton(
                icon: const Icon(Icons.delete),
                tooltip: R.getString(R.id.mnuDelete),
                onPressed: () async {
                  Future<void> fut = _onRemoveItems();
                  fut.whenComplete(() => _onRemoveItemsCompleted());
                })
          ]
        : null;

    Widget _buildItem(final int index) {
      return TextButton(
        child: Container(
          alignment: alignment,
          decoration: isDeleteMode && mapRemovedId.containsKey(index) ? BoxDecoration(border: Border.all(style: BorderStyle.solid, color: widget.actionStyle.accentFgColor)) : null,
          child: GestureDetector(
            onLongPress: () async {
              await _onLongPressItem(index);
            },
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: RichText(overflow: TextOverflow.ellipsis, textAlign: TextAlign.start, text: widget.lstItem[index]),
            ),
          ),
        ),
        onPressed: () => _onOpenItem(index),
      );
    }

    final lstBuilder = Expanded(
      child: ListView.builder(
          padding: EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0, bottom: 10.0),
          physics: ScrollPhysics(),
          shrinkWrap: false,
          itemCount: itemCount,
          itemBuilder: (context, index) {
            return _buildItem(index);
          }),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.titlePage),
        toolbarHeight: gAppBarHeight,
        actions: actions,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            lstBuilder,
          ],
        ),
      ),
    );
  }
}

class AboutPage extends StatelessWidget {
  AboutPage({Key? key, required this.titlePage, required this.dbVersion, required this.versionName, required this.versionCode, required this.versionAbout, required this.versionPlatform, required this.policy})
      : super(key: key);

  final String titlePage;
  final int dbVersion; //ex: 48
  final String versionName;
  final String versionCode;
  final String versionAbout;
  final String versionPlatform;
  final String policy;

  final fontSize = PStyle.instance.fontSize;
  final fontFamily = PStyle.instance.fontFamily;
  final String devAbout = "rhotlittlrewhitedorg".replaceAll('r', '');

  @override
  Widget build(BuildContext context) {
    String emo = '(love++)';
    try { emo = "${R.fav[10]['bmCurrent']}"; } catch(ex) {};
    return Scaffold(
      appBar: AppBar(
        title: Text(titlePage),
        toolbarHeight: gAppBarHeight,
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 10.0, top: 0.0, right: 10.0, bottom: 0.0),
              child: Column(
                children: <Widget>[
                  SizedBox(height: 30.0),
                  InkWell(
                    child: Image(width: 50.0, height: 50.0, image: AssetImage('assets/drawables/thelifelogoForMac512.png')),
                    onTap: () {
                      P.debugCounter++;
                      if (P.debugCounter == 7) {
                        P.debugCounter = 0;
                        P.isDebug = !P.isDebug;
                        final String debugStatus = P.isDebug ? "ON" : "OFF";
                        P.showToast(context, "* Debug: $debugStatus *", Toast.LENGTH_SHORT);
                      }
                    },
                  ),
                  SizedBox(height: 10.0),
                  Text(
                    "Bible Multi\nThe Life\n",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
                  ),
                  SizedBox(height: 10.0),
                  Text(
                    versionAbout,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
                  ),
                  Text(
                    versionCode,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
                  ),
                  Text(
                    "@$devAbout\n\n",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20.0, top: 0.0, right: 20.0, bottom: 0.0),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(children: [
                        const Icon(Icons.phone_iphone_outlined),
                        Text(
                          versionPlatform,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
                        ),
                      ]),
                    ),
                  ),
                  Text('\n\n'),
                  InkWell(
                    child: Text(
                      "All The Glory To God ${emo}",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
                    ),
                    onTap: () => P.showToast(context, ":)", Toast.LENGTH_SHORT),
                  ),
                  Text('\n\n'),
                  Text(
                    R.getString(R.id.aboutContactMe),
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
                  ),
                  Text('\n\n'),
                  OutlinedButton(
                    child: Text(
                      "Web",
                      style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
                    ),
                    onPressed: () async {
                      await P.launchUrl(context, "https://hotlittlewhitedog.gitlab.io/biblemulti");
                    },
                  ),
                  Text(''),
                  OutlinedButton(
                    child: Text(
                      "Telegram",
                      style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
                    ),
                    onPressed: () async {
                      await P.launchUrl(context, "XhXtXtXpXsXX:X/XXX/wXwwXX.XXtX.XmXXeXXX/XbXiXXbXlXemXXuXlXXtXXiXtXXXhXeXXliXgXXhXtX".replaceAll('X', ''));
                    },
                  ),
                  Text(''),
                  OutlinedButton(
                    child: Text(
                      "Email",
                      style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
                    ),
                    onPressed: () async {
                      await P.launchEmail(context: context, email: "${devAbout}@gmail.com", subject:'Bible Multi The Life');
                    },
                  ),
                  Text(''),
                  OutlinedButton(
                    child: Text(
                      "Gitlab",
                      style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
                    ),
                    onPressed: () async {
                      await P.launchUrl(context, "https://gitlab.com/hotlittlewhitedog/BibleTheLife/-/issues");
                    },
                  ),
                  Text(''),
                  OutlinedButton(
                    child: Text(
                      "XDA",
                      style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
                    ),
                    onPressed: () async {
                      await P.launchUrl(context, "https://forum.xda-developers.com/t/app-4-1-bible-multi-the-life-open-source.4071757/");
                    },
                  ),
                  Text(''),
                  OutlinedButton(
                    child: Text(
                      "Facebook",
                      style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
                    ),
                    onPressed: () async {
                      await P.launchUrl(context, "https://www.facebook.com/BibleMultiTheLight");
                    },
                  ),
                  Text(''),
                  OutlinedButton(
                    child: Text(
                      "Policy ",
                      style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
                    ),
                    onPressed: () async {
                      await P.launchUrl(context, policy);
                    },
                  ),
                  SizedBox(height: 30.0),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class HelpPage extends StatelessWidget {
  HelpPage({Key? key, required this.titlePage}) : super(key: key);

  final String titlePage;

  final fontSize = PStyle.instance.fontSize; //PStyle.instance.fontSizeForMenu(20.0);
  final fontFamily = PStyle.instance.fontFamily; //PStyle.instance.fontFamilyForMenu();
  final String html = P.replaceCustomHtml(R.getString(R.id.ART_APP_HELP_CONTENT));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(titlePage),
        toolbarHeight: gAppBarHeight,
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 20.0, top: 20.0, right: 20.0, bottom: 20.0),
              child: Column(
                children: <Widget>[
                  SizedBox(height: 20.0),
                  HTML.HtmlWidget(
                    html,
                    textStyle: TextStyle(fontFamily: fontFamily, fontSize: fontSize),
                  ),
                  SizedBox(height: 20.0),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class InviteFriendPage extends StatelessWidget {
  InviteFriendPage({Key? key, required this.titlePage}) : super(key: key);

  final String titlePage;

  final fontSize = PStyle.instance.fontSize; //PStyle.instance.fontSizeForMenu(20.0);
  final fontFamily = PStyle.instance.fontFamily; //PStyle.instance.fontFamilyForMenu();

  @override
  Widget build(BuildContext inviteFriendPageContext) {
    return Scaffold(
      appBar: AppBar(
        title: Text(titlePage),
        toolbarHeight: gAppBarHeight,
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 20.0, top: 20.0, right: 20.0, bottom: 20.0),
              child: Column(
                children: <Widget>[
                  SizedBox(height: 20.0),
                  Text(
                    R.getString(R.id.inviteFriendPageMsg),
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
                  ),
                  SizedBox(height: 20.0),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ManageFavsPage extends StatefulWidget {
  ManageFavsPage({Key? key, required this.titlePage, required this.actionStyle}) : super(key: key);

  final String titlePage;
  final VerseStyle actionStyle;

  @override
  _ManageFavsPageState createState() => _ManageFavsPageState();
}

class _ManageFavsPageState extends State<ManageFavsPage> {
  final dal = DbHelper.instance;
  GlobalKey refreshKey = GlobalKey();

  @override
  Widget build(BuildContext manageFavsPageContext) {
    Future<bool> _onWillPop() async {
      final Future<int> fakeResFut = Future.value(-1);
      Navigator.of(manageFavsPageContext).pop(fakeResFut);
      return true;
    }

    final Color fgColor = widget.actionStyle.defaultFgColor;
    final textStyleMenu = TextStyle(fontFamily: PStyle.instance.fontFamilyForMenu(), color: fgColor, fontSize: 18.0); //fontFamily: "Droid-sans-mono.regular",
    final Icon editIcon = Icon(Icons.edit, semanticLabel: R.getString(R.id.mnuFavUpdate));

    void _onCompleted() {
      setState(() {
        refreshKey = GlobalKey();
      });
    }

    List<Widget> _getFavItems() {
      final List<Widget> lstWidget = [];

      try {
        lstWidget.add(ListTile(
          title: Text(R.getString(R.id.mnuFavAdd), style: textStyleMenu),
          leading: const Icon(Icons.add),
          onTap: () async {
            int bmId = await dal.getBookmarkIdMax() + 1;
            if (bmId < P.bookmarkIdDevLimit) bmId = P.bookmarkIdDevLimit;
            final Future? favPageFut = Navigator.push(
              manageFavsPageContext,
              MaterialPageRoute(
                builder: (context) => ManageFavPage(
                  titlePage: R.getString(R.id.mnuFavAdd),
                  bookmarkId: bmId,
                  bookmarkDesc: '',
                  bookmarkEmo: '',
                  action: 1,
                  actionStyle: widget.actionStyle,
                ),
              ),
            );
            favPageFut?.whenComplete(() => _onCompleted());
          },
        ));

        if (P.isDebug) print("_ManageFavsPageState.getFavItems(): fav is empty:${R.fav.isEmpty.toString()}");
        if (P.isDebug) print("_ManageFavsPageState.getFavItems(): fav length:${R.fav.length}");
        R.fav.keys.forEach((key) {
          if (key >= 1)
            lstWidget.add(
              ListTile(
                title: RichText(
                  text: TextSpan(
                    text: "${R.fav[key]['bmCurrent']} ",
                    style: textStyleMenu,
                    children: [
                      TextSpan(text: "${R.fav[key]['bmDesc']}", style: textStyleMenu),
                    ],
                  ),
                ),
                leading: editIcon,
                onTap: () {
                  Navigator.push(
                    manageFavsPageContext,
                    MaterialPageRoute(
                      builder: (context) => ManageFavPage(
                        titlePage: R.getString(R.id.mnuFavUpdate),
                        bookmarkId: key,
                        bookmarkDesc: "${R.fav[key]['bmDesc']}",
                        bookmarkEmo: "${R.fav[key]['bmCurrent']}",
                        action: 0,
                        actionStyle: widget.actionStyle,
                      ),
                    ),
                  );
                },
              ),
            );
        });
      } catch (ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
      return lstWidget;
    }

    final List<Widget> lstWidget = _getFavItems();

    return WillPopScope(
      onWillPop: () => _onWillPop(),
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.titlePage),
          toolbarHeight: gAppBarHeight,
        ),
        body: Center(
          child: ListView.builder(
            key: refreshKey,
            itemCount: lstWidget.length,
            itemBuilder: (BuildContext lvbContext, int index) {
              return lstWidget[index];
            },
          ),
        ),
      ),
    );
  }
}

class ManageFavPage extends StatefulWidget {
  ManageFavPage({
    Key? key,
    required this.titlePage,
    required this.bookmarkId,
    required this.bookmarkDesc,
    required this.bookmarkEmo,
    required this.action,
    required this.actionStyle,
  }) : super(key: key);

  final String titlePage;
  final int bookmarkId;
  final String bookmarkDesc;
  final String bookmarkEmo;
  final int action;
  final VerseStyle actionStyle;

  @override
  _ManageFavPageState createState() => _ManageFavPageState();
}

class _ManageFavPageState extends State<ManageFavPage> {
  final dal = DbHelper.instance;

  Map bm = {};
  late OutlineInputBorder borderEmo;
  late TextField textFieldDesc;
  late TextField textFieldEmo;
  late OutlinedButton btnSave;
  late SingleChildScrollView rowButtons;
  late TextStyle styleLabel;

  void _onCopyEmo(final String emoToCopy) {
    setState(() {
      textFieldEmo.controller?.text = emoToCopy;
    });
  }

  @override
  void initState() {
    super.initState();

    try {
      final TextEditingController textEditingDescController = TextEditingController(text: "");
      final TextEditingController textEditingEmoController = TextEditingController(text: "");

      final fontSizeLabel = PStyle.instance.fontSize;
      final fontSizeEmo = PStyle.instance.fontSize + 6;
      final fontFamily = PStyle.instance.fontFamily;
      final styleEmo = TextStyle(fontSize: fontSizeEmo, fontFamily: fontFamily, color: widget.actionStyle.defaultFgColor);
      styleLabel = TextStyle(fontSize: fontSizeLabel, fontFamily: fontFamily);

      void _getBookmarkById() async {
        try {
          bm = widget.action == 0 ? await dal.getBookmarkById(widget.bookmarkId) : {};
          late final String bmDev1;
          late final String bmDev2;
          late final String bmDev3;
          if (bm.isEmpty) {
            bmDev1 = bmDev2 = bmDev3 = '';
          } else {
            bmDev1 = bm["bmDev1"];
            bmDev2 = bm["bmDev2"];
            bmDev3 = bm["bmDev3"];
          }
          final bool visBtnDev1 = !bmDev1.isEmpty;
          final bool visBtnDev2 = !bmDev2.isEmpty;
          final bool visBtnDev3 = !bmDev3.isEmpty;
          final TextButton btnDev1 = TextButton(onPressed: () => _onCopyEmo(bmDev1), child: Text(bmDev1, style: styleEmo));
          final TextButton btnDev2 = TextButton(onPressed: () => _onCopyEmo(bmDev2), child: Text(bmDev2, style: styleEmo));
          final TextButton btnDev3 = TextButton(onPressed: () => _onCopyEmo(bmDev3), child: Text(bmDev3, style: styleEmo));

          setState(() {
            textFieldDesc.controller?.text = widget.bookmarkDesc;
            textFieldEmo.controller?.text = widget.bookmarkEmo;
            rowButtons = SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Container(
                padding: EdgeInsets.only(left: 20.0, top: 20.0, right: 20.0, bottom: 20.0),
                child: Row(
                  children: [
                    Visibility(visible: visBtnDev1, child: btnDev1),
                    Visibility(visible: visBtnDev2, child: btnDev2),
                    Visibility(visible: visBtnDev3, child: btnDev3),
                  ],
                ),
              ),
            );
          });
        } catch(ex, stacktrace) {
          if (P.isDebug) P.printLog(ex, stacktrace);
        }
      }

      Future<void> _onSubmit() async {
        try {
          final String? desc = (textFieldDesc.controller == null) ? null : textFieldDesc.controller?.text.trim();
          final String? emo =  (textFieldEmo.controller == null) ? null : textFieldEmo.controller?.text.trim();
          if (desc == null || desc.isEmpty || emo == null || emo.isEmpty) return;
          final String updDesc = "${desc.substring(0, 1).toUpperCase()}${desc.substring(1)}";
          final bool isDevBm = widget.bookmarkId < P.bookmarkIdDevLimit;
          final BookmarkBO updBm = BookmarkBO();
          updBm.bmId = widget.bookmarkId;
          updBm.bmDesc = updDesc;
          updBm.bmCurrent = emo;

          if (isDevBm) {
            updBm.bmDev1 = bm["bmDev1"];
            updBm.bmDev2 = bm["bmDev2"];
            updBm.bmDev3 = bm["bmDev3"];
            updBm.bmPrev1 = bm["bmPrev1"];
            updBm.bmPrev2 = bm["bmPrev2"];
            updBm.bmPrev3 = bm["bmPrev3"];
            updBm.bmPrev4 = bm["bmPrev4"];
            updBm.bmPrev5 = bm["bmPrev5"];
          } else {
            updBm.bmDev1 = updBm.bmDev2 = updBm.bmDev3 = updBm.bmPrev1 = updBm.bmPrev2 = updBm.bmPrev3 = updBm.bmPrev4 = updBm.bmPrev5 = '';
          }

          await dal.replaceBookmark(updBm); //TODO: FAV, ADD TO QUEUE
          await R.setFav();

          final Future<int> fakeResFut = Future.value(1);
          Navigator.of(context).pop(fakeResFut);
          Navigator.of(context).pop(fakeResFut);  //TODO: refresh previous page would be better
        } catch(ex, stacktrace) {
          if (P.isDebug) P.printLog(ex, stacktrace);
        }
      }

      setState(() {
        borderEmo = OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
          borderSide: BorderSide(color: widget.actionStyle.accentFgColor),
        );
        textFieldDesc = TextField(
          textAlign: TextAlign.center,
          decoration: InputDecoration(labelStyle: styleLabel, border: InputBorder.none, hintText: R.getString(R.id.hintFavDesc)),
          readOnly: widget.bookmarkId < P.bookmarkIdDevLimit,
          enabled: true,
          maxLines: 1,
          showCursor: true,
          style: styleLabel,
          controller: textEditingDescController,
        );
        textFieldEmo = TextField(
          textAlign: TextAlign.center,
          decoration: InputDecoration(labelStyle: styleLabel, border: InputBorder.none, hintText: 'Emoji 🔥'),
          readOnly: false,
          enabled: true,
          maxLines: 1,
          showCursor: true,
          style: styleEmo,
          controller: textEditingEmoController,
        );
        btnSave = OutlinedButton(
          child: Text(
            R.getString(R.id.mnuSave),
            style: styleLabel,
          ),
          onPressed: () async {
            await _onSubmit();
          },
        );
        rowButtons = SingleChildScrollView();
      });

      _getBookmarkById();
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  @override
  Widget build(BuildContext manageFavPageContext) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(widget.titlePage),
        toolbarHeight: gAppBarHeight,
        actions: [
          IconButton(
            icon: const Icon(Icons.help_outline),
            tooltip: R.getString(R.id.mnuHelp),
            onPressed: () => P.onShowDialog(context: manageFavPageContext, titleId: R.id.mnuHelp, contentId: R.id.mnuHelpEmo, textStyle: styleLabel),
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 20.0, top: 20.0, right: 20.0, bottom: 20.0),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 20.0, top: 0.0, right: 20.0, bottom: 0.0),
                  child: textFieldDesc,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20.0, top: 0.0, right: 20.0, bottom: 20.0),
                  child: textFieldEmo,
                ),
                btnSave,
                rowButtons,
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class SearchPage extends StatefulWidget {
  SearchPage({
      Key? key,
      required this.titlePage,
      required this.isAlignmentStart,
      required this.topBarMenuType,
      required this.actionStyle,
      required this.extraBBName0,
      required this.extraBBNames,
      required this.extraSearchType,
      required this.extraSearchOrderBy,
      required this.extraSearchFavFilter
  }) : super(key: key);

  final String titlePage;
  final bool isAlignmentStart;
  final TopBarMenuType topBarMenuType;
  final VerseStyle actionStyle;
  final String extraBBName0;
  final String extraBBNames;
  final String extraSearchType;
  final int extraSearchOrderBy;
  final int extraSearchFavFilter;

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final dal = DbHelper.instance;
  FocusNode textFieldFocusNode = FocusNode();
  final TextEditingController textEditingController = TextEditingController(text: "");
  late TextStyle itemTextStyle;
  final fontSize = PStyle.instance.fontSize;
  final fontFamily = PStyle.instance.fontFamily;

  late String searchBoxBBName;
  String searchBoxLanguage = "";
  late String searchBoxType;
  int searchBoxOrderBy = 0;
  int searchBoxFavFilter = 0;
  String searchBoxTypeVerbose = "";
  String searchBoxOrderByVerbose = "";
  String searchBoxFavFilterVerbose = "";
  late TextField textField;
  String? textFieldErrorMsg;
  String hintTextField = "";
  List<String> lstItemFiltered = [];
  List<String> lstItem = [];
  late bool visBibleLanguage;

  Future<List<String>> _getLstItemFiltered(final bool shouldReloadFromDb) async {
    if (shouldReloadFromDb) {
      lstItem.clear();
      final List<Map> lstBooks = await dal.getListAllBookByBBName(searchBoxBBName, 'bName');
      lstBooks.forEach((row) {
        lstItem.add(row['bName']);
      });
    }

    final String searchBoxText = (textField.controller == null) ? '' : Diacritic.removeDiacritics(textField.controller!.text.toLowerCase()); //was: (textField == null) ? "" :
    if (searchBoxText.isEmpty) {
      return Future.value(lstItem); //widget.lstItem;
    } else {
      final List<String> newLstItemFiltered = [];
      lstItem.forEach((element) {
        if (Diacritic.removeDiacritics(element.toLowerCase()).startsWith(searchBoxText)) newLstItemFiltered.add(element);
      });
      return Future.value(newLstItemFiltered);
    }
  }

  int _getLstItemFilteredCount() => lstItemFiltered.length;

  Expanded _getLstItemFilteredBuilder() {
    final Alignment? alignment = widget.isAlignmentStart ? Alignment(-1.0, -1.0) : null;

    return Expanded(
      child: ListView.builder(
        padding: EdgeInsets.only(left: 10.0, top: 0.0, right: 10.0, bottom: 10.0),
        physics: ScrollPhysics(),
        shrinkWrap: false,
        itemCount: _getLstItemFilteredCount(),
        itemBuilder: (context, index) {
          return TextButton(
            child: Container(
              alignment: alignment,
              child: RichText(
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.start,
                text: TextSpan(
                  text: lstItemFiltered[index],
                  style: itemTextStyle,
                ),
              ),
            ),
            onPressed: () => _onItemSelected(index),
          );
        },
      ),
    );
  }

  void _refreshFilter(final bool shouldReloadFromDb) async {
    lstItemFiltered = await _getLstItemFiltered(shouldReloadFromDb);
  }

  void _onItemSelected(final int itemIndex) {
    setState(() {
      if (textField.controller != null) {
        textField.controller?.text = lstItemFiltered[itemIndex];
        _refreshFilter(false);
        _setCursorToEndOfTextField();
      }
    });
  }

  void _setHintTextField() {
    final String hintFav = (searchBoxType == "F" && searchBoxFavFilter != 0) ? R.fav[searchBoxFavFilter]["bmCurrent"] : "";
    hintTextField = R.getString(R.id.hintSearchBible).replaceFirst("{0}", "${searchBoxTypeVerbose} ${hintFav}");
  }

  void _rollSearchType(final bool isCalledFromInit) {
    if (isCalledFromInit) {
      searchBoxType = widget.extraSearchType;
      searchBoxTypeVerbose = R.getString(widget.extraSearchType == "B" ? R.id.bible : R.id.mnuFavorites);
      searchBoxFavFilterVerbose = "${R.fav[searchBoxFavFilter]["bmCurrent"]}";
      _setHintTextField();
      return;
    }

    searchBoxType = searchBoxType == "B" ? "F" : "B";
    searchBoxTypeVerbose = R.getString(searchBoxType == "B" ? R.id.bible : R.id.mnuFavorites);
    searchBoxFavFilterVerbose = "${R.fav[searchBoxFavFilter]["bmCurrent"]}";
    textFieldErrorMsg = null;
    _setHintTextField();
  }

  void _rollSearchOrderBy(final bool isCalledFromInit) {
    if (isCalledFromInit) {
      searchBoxOrderBy = widget.extraSearchOrderBy;
      searchBoxOrderByVerbose = R.getString(searchBoxOrderBy == 0 ? R.id.favOrderByBook : R.id.favOrderByDate);
      return;
    }

    searchBoxOrderBy = searchBoxOrderBy == 0 ? 1 : 0;
    searchBoxOrderByVerbose = R.getString(searchBoxOrderBy == 0 ? R.id.favOrderByBook : R.id.favOrderByDate);
  }

  void _rollBibleLanguage(final bool isCalledFromInit) {
    if (isCalledFromInit) {
      searchBoxBBName = widget.extraBBName0;
      searchBoxLanguage = P.getBibleName(widget.extraBBName0, false);
      visBibleLanguage = widget.extraBBNames.length > 1;
      return;
    }

    final String bbNames = widget.extraBBNames;
    final int pos = bbNames.indexOf(RegExp(searchBoxBBName));
    final int maxPos = bbNames.length - 1;
    final int nextPos = pos + 1 > maxPos ? 0 : pos + 1;

    searchBoxBBName = bbNames.substring(nextPos, nextPos + 1);
    searchBoxLanguage = P.getBibleName(searchBoxBBName, false);
  }

  void _onChangedTextField(final String text) {
    setState(() {
      _refreshFilter(false);
    });
  }

  void _onSubmitTextField(final BuildContext context) async {
    try {
      final String searchBox = (textField.controller == null) ? '' : textField.controller!.text.replaceAll(RegExp("%+"), "%").replaceFirst(RegExp("^%"), "").replaceFirst(RegExp("%\$"), "");

      final Map mapType = await dal.getSearchTypeParams(searchBoxType, searchBoxOrderBy, searchBoxBBName, searchBox, searchBoxFavFilter);
      if (mapType.isEmpty) return;

      String? warnMsg = (searchBoxType != "F" && searchBox.length < P.searchQueryLimit) ? R.getString(R.id.validatorSearchQueryLimit) : null;
      if (warnMsg == null) {
        final Map mapSearchBibleCount = await dal.getSearchBibleCount(searchBoxType, searchBoxBBName, searchBox, searchBoxBBName, searchBoxFavFilter);
        if (mapSearchBibleCount["rowCount"] == 0) warnMsg = R.getString(R.id.toastWarnNoResultFound);
      }

      if (warnMsg != null) {
        setState(() {
          textFieldErrorMsg = warnMsg;
          textField.controller?.text = searchBox;
          _setCursorToEndOfTextField();
        });
        return;
      }

      mapType["searchBox"] = searchBox;
      mapType["searchBoxBBName"] = searchBoxBBName;
      mapType["searchBoxType"] = searchBoxType;
      mapType["searchBoxOrderBy"] = searchBoxOrderBy;
      mapType["searchBoxFavFilter"] = searchBoxFavFilter;
      Navigator.of(context).pop(mapType);
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  void _setCursorToEndOfTextField() {
    final TextSelection cursorPos = TextSelection.fromPosition(TextPosition(offset: textField.controller == null ? 0 : textField.controller!.text.length));
    textEditingController.selection = cursorPos;
  }

  void _onChangeSearchType(final BuildContext context) {
    setState(() {
      _rollSearchType(false);
    });
  }

  void _onChangeSearchOrderBy(final BuildContext context) {
    setState(() {
      _rollSearchOrderBy(false);
    });
  }

  void _onChangeBibleLanguage(final BuildContext context) {
    setState(() {
      _rollBibleLanguage(false);
      _refreshFilter(true);
    });
  }

  void _onChangeFavFilter(final BuildContext context) {
    final textStyleMenu = TextStyle(fontFamily: PStyle.instance.fontFamilyForMenu(), color: widget.actionStyle.defaultFgColor, fontSize: 18.0); //fontFamily: "Droid-sans-mono.regular",

    List<Widget> _getFavItems() {
      final List<Widget> lstWidget = [];

      try {
        if (P.isDebug) print("getFavItems(): fav is empty:${R.fav.isEmpty.toString()}");
        if (P.isDebug) print("getFavItems(): fav length:${R.fav.length}");
        R.fav.keys.forEach((key) {
          if (key >= 0)
            lstWidget.add(
              ListTile(
                contentPadding: EdgeInsets.zero,
                title: RichText(
                  text: TextSpan(
                    text: "${R.fav[key]['bmCurrent']} ",
                    style: textStyleMenu,
                    children: [
                      TextSpan(text: (key == 0) ? "${R.getString(R.id.favAll)}" : "${R.fav[key]['bmDesc']}", style: textStyleMenu),
                    ],
                  ),
                ),
                leading: null,
                onTap: () async {
                  setState(() {
                    searchBoxFavFilter = key;
                    searchBoxFavFilterVerbose = "${R.fav[key]["bmCurrent"]}";
                    _setHintTextField();
                  });
                  Navigator.pop(context);
                },
              ),
            );
        });
      } catch (ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
      return lstWidget;
    }

    final List<Widget> lstWidget = _getFavItems();

    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async => true,
          child: AlertDialog(
            scrollable: true,
            title: Text(R.getString(R.id.mnuSearch)),
            content: Column(
              children: lstWidget,
            ),
            actions: [
              OutlinedButton(
                onPressed: () => Navigator.pop(context),
                child: Text(R.getString(R.id.mnuClose)),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _rollSearchType(true);
    _rollSearchOrderBy(true);
    _rollBibleLanguage(true);
    textFieldErrorMsg = null;
    textFieldFocusNode.requestFocus();
    itemTextStyle = TextStyle(height: 2.0, fontFamily: PStyle.instance.fontFamilyForMenu(), fontSize: PStyle.instance.fontSizeForMenu(), color: widget.actionStyle.defaultFgColor);
    _refreshFilter(true);
  }

  @override
  Widget build(BuildContext searchPageContext) {
    final UnderlineInputBorder border = UnderlineInputBorder(
      borderSide: BorderSide(color: widget.actionStyle.accentFgColor),
    );
    final TextStyle errorStyle = TextStyle(fontSize: fontSize - 4.0, fontFamily: fontFamily, color: widget.actionStyle.accentFgColor);
    //final OutlineInputBorder border = OutlineInputBorder(borderSide: BorderSide(color: widget.actionStyle.accentColor));

    final Expanded lstItemFilteredBuilder = _getLstItemFilteredBuilder();

    final OutlinedButton btnSearch = OutlinedButton(
      child: Text(
        R.getString(R.id.mnuSearch),
        style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
      ),
      onPressed: () {
        _onSubmitTextField(searchPageContext);
      },
    );

    final OutlinedButton btnSearchType = OutlinedButton(
      child: Text(
        searchBoxTypeVerbose,
        style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
      ),
      onPressed: () {
        _onChangeSearchType(searchPageContext);
      },
    );

    final OutlinedButton btnFavFilter = OutlinedButton(
      child: Text(
        searchBoxFavFilterVerbose,
        style: TextStyle(fontSize: fontSize, fontFamily: fontFamily, color: widget.actionStyle.defaultFgColor),
      ),
      onPressed: () {
        _onChangeFavFilter(searchPageContext);
      },
    );

    final OutlinedButton btnBibleLanguage = OutlinedButton(
      child: Text(
        searchBoxLanguage,
        style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
      ),
      onPressed: () {
        _onChangeBibleLanguage(searchPageContext);
      },
    );

    final OutlinedButton btnSearchOrderBy = OutlinedButton(
      child: Text(
        searchBoxOrderByVerbose,
        style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
      ),
      onPressed: () {
        _onChangeSearchOrderBy(searchPageContext);
      },
    );

    textField = TextField(
      decoration: InputDecoration(border: border, hintText: hintTextField, errorText: textFieldErrorMsg, errorStyle: errorStyle, errorBorder: border, errorMaxLines: 1, focusedErrorBorder: border),
      focusNode: textFieldFocusNode,
      controller: textEditingController,
      textDirection: searchBoxBBName == 'y' ? TextDirection.rtl : TextDirection.ltr,
      onChanged: (text) => _onChangedTextField(text),
      onSubmitted: (text) => _onSubmitTextField(searchPageContext),
      enabled: true,
      maxLines: 1,
      style: TextStyle(fontSize: fontSize, fontFamily: fontFamily),
    );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(title: Text(widget.titlePage), toolbarHeight: gAppBarHeight, actions: null),
      body: Column(children: [
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Container(
            padding: EdgeInsets.only(left: 20.0, top: 8.0, right: 20.0, bottom: 0.0),
            child: Row(
              children: [
                btnSearchType,
                Visibility(visible: searchBoxType == "F", child: const Text(" ")),
                Visibility(visible: searchBoxType == "F", child: btnFavFilter),
                Visibility(visible: visBibleLanguage, child: const Text(" ")),
                Visibility(visible: visBibleLanguage, child: btnBibleLanguage),
              ],
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(left: 20.0, top: 0.0, right: 20.0, bottom: 8.0), child: textField),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Container(
            padding: EdgeInsets.only(left: 20.0, top: 0, right: 20.0, bottom: 0.0),
            child: Row(
              children: [Visibility(visible: searchBoxType == "F", child: btnSearchOrderBy), Visibility(visible: searchBoxType == "F", child: const Text(" ")), btnSearch],
            ),
          ),
        ),
        lstItemFilteredBuilder,
      ]),
    );
  }
}

class CustomBiblePage extends StatefulWidget {
  CustomBiblePage({Key? key, required this.titlePage, required this.bbNameSelected, required this.menuVerseStyle}) : super(key: key);

  final String titlePage;
  final String bbNameSelected;
  final VerseStyle menuVerseStyle;

  @override
  _CustomBiblePageState createState() => _CustomBiblePageState();
}

class _CustomBiblePageState extends State<CustomBiblePage> {
  TextSpan versePreviewTextSpan = TextSpan(text: "");
  Widget lstMenuBuilder = SizedBox(height: 10.0);

  final dal = DbHelper.instance;
  double fontSizeMenu = PStyle.instance.fontSizeForMenu();
  String fontFamilyMenu = PStyle.instance.fontFamilyForMenu();
  late TextStyle menuTextStyle;
  late CustomVerseStyle customVerseStyle;
  late bool isLightThemeAsDefault;
  late Map<String, dynamic> customBibleInternal;
  late List<Map> lstSetting;
  final List<Map> lstBible = R.getListAllBibleSettings();

  Future<TextSpan?> _getVerseTextSpan(final Map verseData) async {
    try {
      final String end = ' \n ';
      final int vNumber = verseData['vNumber'];
      final String vBBName = verseData['bbName'];
      final String dir = vBBName == 'y' ? P.getRTL() : P.getLTR();
      final Locale localeDir = vBBName == 'y' ? Locale('ar') : Locale('en');
      final String vText = "${dir}${verseData['vText']}";
      final int crTot = verseData['tot'];

      final String customBibleJsonString = await P.Prefs.getCustomBibleInternal(vBBName);
      if (customBibleJsonString.isEmpty) {
        customBibleInternal = P.getCustomBibleInternalDefault(isLightThemeAsDefault);
      } else {
        try {
          customBibleInternal = jsonDecode(customBibleJsonString);
        } catch (ex, stacktrace) {
          if (P.isDebug) P.printLog(ex, stacktrace);
        }
      }

      customVerseStyle = CustomVerseStyle.convertInternalToCustomVerseStyle(customBibleInternal, isLightThemeAsDefault);
      final String vFontFamily = customVerseStyle.vFontFamily;
      final double vFontSize = customVerseStyle.vFontSize;
      final Color vNumberFgColor = customVerseStyle.vNumberFgColor;
      final Color vNumberBgColor = customVerseStyle.vNumberBgColor;
      final Color vTextFgColor = customVerseStyle.vTextFgColor;
      final Color vTextBgColor = customVerseStyle.vTextBgColor;
      final Color vCrFgColor = customVerseStyle.vCrFgColor;
      final Color vCrBgColor = customVerseStyle.vCrBgColor;

      final TextSpan vNumberStyled = TextSpan(
        text: "$vNumber",
        style: TextStyle(fontFamily: vFontFamily, fontSize: vFontSize, color: vNumberFgColor, backgroundColor: vNumberBgColor),
      );
      final TextSpan vTextStyled = TextSpan(
        text: " $vText ",
        style: TextStyle(fontFamily: vFontFamily, fontSize: vFontSize, color: vTextFgColor, backgroundColor: vTextBgColor),
      );
      final TextSpan crStyled = TextSpan(
        text: "[$crTot]",
        style: TextStyle(fontFamily: vFontFamily, fontSize: vFontSize, color: vCrFgColor, backgroundColor: vCrBgColor),
      );
      final TextSpan endStyled = TextSpan(text: "$end");

      //Finally
      final List<InlineSpan> rowStyled = [];
      rowStyled.add(vNumberStyled);
      rowStyled.add(vTextStyled);
      rowStyled.add(crStyled);
      rowStyled.add(endStyled);

      return TextSpan(children: rowStyled, locale: localeDir);
    } catch (ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return null;
  }

  Future<TextSpan> _getVersePreview() async {
    final List<Map> lstVerse = await dal.getVerses(widget.bbNameSelected, 1, 1, 1, 1);
    final Map verseData = lstVerse[0];
    final TextSpan? versePreview = await _getVerseTextSpan(verseData);
    if (versePreview == null) return TextSpan(text: "Preview!");
    return versePreview;
  }

  Future<void> _setWidgets() async {
    try {
      fontSizeMenu = PStyle.instance.fontSizeForMenu();
      fontFamilyMenu = PStyle.instance.fontFamilyForMenu();
      menuTextStyle = TextStyle(color: widget.menuVerseStyle.defaultFgColor, fontFamily: fontFamilyMenu, fontSize: fontSizeMenu);

      final TextStyle menuNormalStyle = TextStyle(height: 2.0, fontFamily: fontFamilyMenu, fontSize: fontSizeMenu, color: widget.menuVerseStyle.defaultFgColor);
      final TextStyle menuSubHeaderStyle = TextStyle(height: 2.0, fontFamily: fontFamilyMenu, fontSize: fontSizeMenu - 4.0, color: widget.menuVerseStyle.accentFgColor);

      versePreviewTextSpan = await _getVersePreview();

      lstSetting = [];
      lstSetting.add({
        "text": R.getString(R.id.mnuSettingsCustomBibleVFontFamily),
        "value": "${customBibleInternal["vFontFamily"]}",
      });
      lstSetting.add({
        "text": R.getString(R.id.mnuSettingsCustomBibleVFontSize),
        "value": "${customBibleInternal["vFontSize"]}",
      });
      lstSetting.add({
        "text": R.getString(R.id.mnuSettingsCustomBibleVTextFgColor),
        "value": "${customBibleInternal["vTextFgColor"]}",
      });
      lstSetting.add({
        "text": R.getString(R.id.mnuSettingsCustomBibleVTextBgColor),
        "value": "${customBibleInternal["vTextBgColor"]}",
      });
      lstSetting.add({
        "text": R.getString(R.id.mnuSettingsCustomBibleVNumberFgColor),
        "value": "${customBibleInternal["vNumberFgColor"]}",
      });
      lstSetting.add({
        "text": R.getString(R.id.mnuSettingsCustomBibleVNumberBgColor),
        "value": "${customBibleInternal["vNumberBgColor"]}",
      });
      lstSetting.add({
        "text": R.getString(R.id.mnuSettingsCustomBibleVCrFgColor),
        "value": "${customBibleInternal["vCrFgColor"]}",
      });
      lstSetting.add({
        "text": R.getString(R.id.mnuSettingsCustomBibleVCrBgColor),
        "value": "${customBibleInternal["vCrBgColor"]}",
      });
      lstSetting.add({
        "text": R.getString(R.id.mnuSettingsCustomBibleResetWithDefaults),
        "value": "",
      });
      lstSetting.add({
        "text": R.getString(R.id.mnuSettingsCustomBibleResetWithBible),
        "value": "",
      });

      final List<TextSpan> lstItem = [];
      final List<Widget> lstItemOfColor = [];
      int lstSettingIndex = 0;
      lstSetting.forEach((row) {
        final String text = row['text'];
        final String value = row['value'];
        final TextSpan rowStyled = value.isEmpty
            ? TextSpan(text: "$text", style: menuNormalStyle)
            : TextSpan(
                text: "$text",
                style: menuNormalStyle,
                children: [
                  (lstSettingIndex <= 1)
                    ? TextSpan(text: "\n$value", style: menuSubHeaderStyle)
                    : TextSpan(text: "")
                ],
              );

        final Widget widgetStyledWithColor;
        if (lstSettingIndex >= 2 && lstSettingIndex <= 7) {
          final CircleAvatar internalCircleAvatar = CircleAvatar(
            radius: 20.0,
            child: Text((value == "auto") ? "auto" : ""),
            backgroundColor: (value == "auto") ? null : convertStringColorToColor(value),
          );
          final double externalRadius = internalCircleAvatar.radius! + 2.0;
          final int indexValue = lstSettingIndex;

          widgetStyledWithColor = InkWell(
            onTap: () => _onItemSelected(indexValue),
            child: CircleAvatar(
              radius: externalRadius,
              backgroundColor: widget.menuVerseStyle.defaultFgColor,
              child: internalCircleAvatar,
            ),
          );
        } else {
          widgetStyledWithColor = Visibility(child: Text(""), visible: false);
        }

        lstItem.add(rowStyled);
        lstItemOfColor.add(widgetStyledWithColor);
        lstSettingIndex++;
      });

      final int itemCount = lstItem.isEmpty ? 0 : lstItem.length;
      final Alignment? verseAlignment = Alignment(-1.0, -1.0);

      setState(() {
        lstMenuBuilder = Container(
          child: ListView.builder(
            padding: EdgeInsets.only(left: 10.0, top: 0.0, right: 10.0, bottom: 10.0),
            physics: ScrollPhysics(),
            shrinkWrap: true,
            itemCount: itemCount,
            itemBuilder: (context, index) {
              final TextDirection dir = lstItem[index].locale != null && lstItem[index].locale?.languageCode == 'ar' ? TextDirection.rtl : TextDirection.ltr;
              final PopupMenuButton popupCopyPasteMenu = PopupMenuButton<String>(
                onSelected: (popupCopyPasteValue){
                  final List<String> lstPopupValue = popupCopyPasteValue.split("#");
                  final int elementIndex = int.parse(lstPopupValue[1]);
                  if (lstPopupValue[0] == "COPY"){
                    _onPopupItemCopyClicked(lstSetting[elementIndex]["value"]);
                  } else if (lstPopupValue[0] == "PASTE"){
                    _onPopupItemPasteClicked(elementIndex);
                  }
                },
                itemBuilder: (BuildContext popupMenuContext) {
                  return [
                    PopupMenuItem<String>(
                      value: "COPY#${index}",
                      child: Text(R.getString(R.id.mnuSettingsCustomBibleCopy)),
                    ),
                    PopupMenuItem<String>(
                      value: "PASTE#${index}",
                      child: Text(R.getString(R.id.mnuSettingsCustomBiblePaste)),
                    ),
                  ];
                },
              );
              final PopupMenuButton popupResetBibleMenu = PopupMenuButton<String>(
                onSelected: (popupResetValue) async => await _onPopupItemResetWithBibleClicked(popupResetValue),
                itemBuilder: (BuildContext popupMenuContext) {
                  return List<PopupMenuItem<String>>.generate(
                    lstBible.length,
                    (int bibleIndex) => PopupMenuItem<String>(
                      value: lstBible[bibleIndex]["bbName"],
                      child: Text(lstBible[bibleIndex]["text"]),
                    ),
                  );
                },
              );
              final Visibility visPopupMenu = Visibility(visible: (index >= 2 && index <= 7 || index == 9), child: (index >= 2 && index <= 7) ? popupCopyPasteMenu : popupResetBibleMenu);

              return Row(
                children: [
                  Expanded(
                    child: TextButton(
                      child: Container(
                        alignment: verseAlignment,
                        child: RichText(textAlign: TextAlign.start, textDirection: dir, text: lstItem[index]),
                      ),
                      onPressed: () => _onItemSelected(index),
                    ),
                  ),
                  lstItemOfColor[index],
                  visPopupMenu,
                ],
              );
            },
          ),
        );
      });
    } catch (ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  void _onItemSelected(final int indexSelected) {
    final String titlePage = lstSetting[ indexSelected ]["text"];
    if (indexSelected == 0) {
      //Font menu
      late final String fontNameSelected;
      final List<String> lstFontName = R.getListAllFontNames();
      final List<TextSpan> lstTextSpanFontName = P.getListTextSpanWithFontNames(lstFontName, widget.menuVerseStyle);
      final Future optionResult = Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) =>
              OptionPage(
                titlePage: titlePage,
                lstItem: lstTextSpanFontName,
                isAlignmentStart: true,
                topBarMenuType: TopBarMenuType.NONE,
                lstItemCheckedOrig: [],
                actionStyle: null,
                layoutType: 1,
                columns: 1,
              ),
        ),
      );
      optionResult.then((value) => fontNameSelected = (value == null) ? '' : lstFontName[value]);
      optionResult.whenComplete(() async {
        if (fontNameSelected.isEmpty) return;

        customBibleInternal["vFontFamily"] = "${fontNameSelected}";

        P.saveCustomVerseStyle(widget.bbNameSelected, customBibleInternal, isLightThemeAsDefault);
        await _setWidgets();
      });
    } else if (indexSelected == 1) {
      //Font size menu
      late final double fontSizeSelected;
      final List<int> lstFontSize = R.getListAllFontSizes();
      final List<TextSpan> lstTextSpanFontSize = P.getListTextSpanWithFontSizes(lstFontSize, widget.menuVerseStyle);
      final Future optionResult = Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) =>
              OptionPage(
                titlePage: titlePage,
                lstItem: lstTextSpanFontSize,
                isAlignmentStart: false,
                topBarMenuType: TopBarMenuType.NONE,
                lstItemCheckedOrig: [],
                actionStyle: null,
                layoutType: 1,
                columns: 1,
              ),
        ),
      );
      optionResult.then((value) => fontSizeSelected = (value == null) ? -1.0 : lstFontSize[value].toDouble());
      optionResult.whenComplete(() async {
        if (fontSizeSelected < 0.0) return;

        customBibleInternal["vFontSize"] = "${fontSizeSelected}";

        P.saveCustomVerseStyle(widget.bbNameSelected, customBibleInternal, isLightThemeAsDefault);
        await _setWidgets();
      });
    } else if (indexSelected >= 2 && indexSelected <= 7) {
      //FG, BG menus
      late final int? customColorSelected;
      final Future optionResult = Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => CustomColorPage(
              titlePage: titlePage,
              bbNameSelected: widget.bbNameSelected,
              menuVerseStyle: widget.menuVerseStyle),
        ),
      );
      optionResult.then((value) => customColorSelected = (value == null) ? null : value);
      optionResult.whenComplete(() async {
        if (customColorSelected == null) return;

        switch (indexSelected) {
          case 2:
            customBibleInternal["vTextFgColor"] = customColorSelected == -1 ? "auto" : "${customColorSelected}";
            break;
          case 3:
            customBibleInternal["vTextBgColor"] = customColorSelected == -1 ? "auto" : "${customColorSelected}";
            break;
          case 4:
            customBibleInternal["vNumberFgColor"] = customColorSelected == -1 ? "auto" : "${customColorSelected}";
            break;
          case 5:
            customBibleInternal["vNumberBgColor"] = customColorSelected == -1 ? "auto" : "${customColorSelected}";
            break;
          case 6:
            customBibleInternal["vCrFgColor"] = customColorSelected == -1 ? "auto" : "${customColorSelected}";
            break;
          case 7:
            customBibleInternal["vCrBgColor"] = customColorSelected == -1 ? "auto" : "${customColorSelected}";
            break;
        }
        P.saveCustomVerseStyle(widget.bbNameSelected, customBibleInternal, isLightThemeAsDefault);
        await _setWidgets();
      });
    } else if (indexSelected == 8) {
      //Reset
      void reset() async {
        customBibleInternal = P.getCustomBibleInternalDefault(isLightThemeAsDefault);

        P.saveCustomVerseStyle(widget.bbNameSelected, customBibleInternal, isLightThemeAsDefault);
        await _setWidgets();
      };

      reset();
    }
  }

  void _onPopupItemCopyClicked(final String valueToCopyToClipboard) {
    P.setCustomVerseStyleClipboard( valueToCopyToClipboard );
  }

  void _onPopupItemPasteClicked(final int indexSelected) async {
    final String clip = P.getCustomVerseStyleClipboard();
    if (clip.isEmpty) return;

    //print("paste... in property element.xxx [$indexSelected]");
    switch (indexSelected) {
      case 2:
        customBibleInternal["vTextFgColor"] = clip;
        break;
      case 3:
        customBibleInternal["vTextBgColor"] = clip;
        break;
      case 4:
        customBibleInternal["vNumberFgColor"] = clip;
        break;
      case 5:
        customBibleInternal["vNumberBgColor"] = clip;
        break;
      case 6:
        customBibleInternal["vCrFgColor"] = clip;
        break;
      case 7:
        customBibleInternal["vCrBgColor"] = clip;
        break;
    }
    P.saveCustomVerseStyle(widget.bbNameSelected, customBibleInternal, isLightThemeAsDefault);
    await _setWidgets();
  }

  Future<void> _onPopupItemResetWithBibleClicked(final String bbNameSelected) async {
    if (bbNameSelected == widget.bbNameSelected) return;

    final String customBibleJsonString = await P.Prefs.getCustomBibleInternal(bbNameSelected);
    if (customBibleJsonString.isEmpty) {
      customBibleInternal = P.getCustomBibleInternalDefault(isLightThemeAsDefault);
    } else {
      try {
        customBibleInternal = jsonDecode(customBibleJsonString);
      } catch (ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    P.saveCustomVerseStyle(widget.bbNameSelected, customBibleInternal, isLightThemeAsDefault);
    await _setWidgets();
  }

  @override
  void initState() {
    super.initState();
    isLightThemeAsDefault = widget.menuVerseStyle is LightVerseStyle;
    _setWidgets();
  }

  @override
  Widget build(BuildContext customBiblePageContext) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.titlePage),
        toolbarHeight: gAppBarHeight,
        actions: [
          IconButton(
            icon: const Icon(Icons.help_outline),
            tooltip: R.getString(R.id.mnuHelp),
            onPressed: () => P.onShowDialog(context: customBiblePageContext, titleId: R.id.mnuHelp, contentId: R.id.mnuHelpCustomBible, textStyle: menuTextStyle),
          ),
        ],
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: [
          Padding(
            padding: EdgeInsets.only(left:20.0, top:20.0, right:20.0, bottom:20.0),
            child: RichText(text: versePreviewTextSpan),
          ),
          lstMenuBuilder,
        ],
      ),
    );
  }
}

class CustomColorPage extends StatefulWidget {
  CustomColorPage({Key? key, required this.titlePage, required this.bbNameSelected, required this.menuVerseStyle}) : super(key: key);

  final String titlePage;
  final String bbNameSelected;
  final VerseStyle menuVerseStyle;

  @override
  _CustomColorPageState createState() => _CustomColorPageState();
}

class _CustomColorPageState extends State<CustomColorPage> {
  Widget lstPrimaryMenuBuilder = SizedBox(height: 10.0);
  Widget lstShadeMenuBuilder = SizedBox(height: 10.0);

  double fontSizeMenu = PStyle.instance.fontSizeForMenu();
  String fontFamilyMenu = PStyle.instance.fontFamilyForMenu();
  late List<int> lstCustomShadeColor;
  int indexPrimaryColorSelected = 1;
  int indexShadeColorSelected = 0;

  Future<void> _setWidgets() async {
    try {
      fontSizeMenu = PStyle.instance.fontSizeForMenu();
      fontFamilyMenu = PStyle.instance.fontFamilyForMenu();

      final List<int> lstCustomPrimaryColor = R.getListAllCustomPrimaryColors();
      final List<TextSpan> lstPrimaryTextSpanItem = P.getListTextSpanWithCustomColors(lstCustomPrimaryColor, widget.menuVerseStyle);
      lstCustomShadeColor = R.getListCustomShadeColors(indexPrimaryColorSelected);
      final List<TextSpan> lstShadeTextSpanItem = P.getListTextSpanWithCustomColors(lstCustomShadeColor, widget.menuVerseStyle);

      final int primaryTextSpanCount = lstPrimaryTextSpanItem.isEmpty ? 0 : lstPrimaryTextSpanItem.length;
      final int shadeTextSpanCount = lstShadeTextSpanItem.isEmpty ? 0 : lstShadeTextSpanItem.length;
      final Alignment? alignment = Alignment(0.0, 0.0);

      setState(() {
        lstPrimaryMenuBuilder = Expanded(
          child: ListView.builder(
            padding: EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0, bottom: 10.0),
            physics: ScrollPhysics(),
            shrinkWrap: false,
            itemCount: primaryTextSpanCount,
            itemBuilder: (context, indexPrimary) {
              final TextDirection dir = TextDirection.ltr;

              return TextButton(
                child: Container(
                  alignment: alignment,
                  child: RichText(overflow: TextOverflow.ellipsis, textAlign: TextAlign.start, textDirection: dir, text: lstPrimaryTextSpanItem[indexPrimary]),
                ),
                onPressed: () => _onPrimaryItemSelected(indexPrimary),
              );
            },
          ),
        );

        lstShadeMenuBuilder = Expanded(
          child: ListView.builder(
            padding: EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0, bottom: 10.0),
            physics: ScrollPhysics(),
            shrinkWrap: false,
            itemCount: shadeTextSpanCount,
            itemBuilder: (context, indexShade) {
              final TextDirection dir = TextDirection.ltr;

              return TextButton(
                child: Container(
                  alignment: alignment,
                  child: RichText(overflow: TextOverflow.ellipsis, textAlign: TextAlign.start, textDirection: dir, text: lstShadeTextSpanItem[indexShade]),
                ),
                onPressed: () => _onShadeItemSelected(indexShade),
              );
            },
          ),
        );
      });
    } catch (ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  void _onPrimaryItemSelected(final int indexSelected) {
    indexPrimaryColorSelected = indexSelected;
    _setWidgets();
  }

  void _onShadeItemSelected(final int indexSelected) {
    final int colorSelected = lstCustomShadeColor[ indexSelected ];
    final Future<int> itemResFut = Future.value( colorSelected );
    Navigator.of(context).pop(itemResFut);
  }

  @override
  void initState() {
    super.initState();
    _setWidgets();
  }

  @override
  Widget build(BuildContext customBiblePageContext) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.titlePage),
        toolbarHeight: gAppBarHeight,
      ),
      body: Container(
        child: Row(
          children: [
            lstPrimaryMenuBuilder,
            lstShadeMenuBuilder,
          ],
        ),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

//** Widgets **
class ChapterWidget extends StatelessWidget {
  ChapterWidget({Key? key, required this.bbNames, required this.chapterType, required this.lstChapterObject, required this.colCount}) : super(key: key);

  final String bbNames;
  final String chapterType;
  final List<Object> lstChapterObject;
  final int colCount;

  @override
  Widget build(BuildContext chapterWidgetStateContext) {
    final int itemCount = lstChapterObject.isEmpty ? 0 : lstChapterObject.length;
    final int maxIndex = lstChapterObject.isEmpty ? 0 : lstChapterObject.length - 1;

    int rindex = -1;
    int rindexIn = -1;
    List<TextSpan> lstRowCell = [];

    Widget _createCell({required final int rowLength, final bool hasPadding = true}) {
      final TextSpan cellTextSpan = rindexIn + 1 <= rowLength ? lstRowCell[++rindexIn] : TextSpan(text: "");
      final TextDirection cellDir = cellTextSpan.locale != null && cellTextSpan.locale?.languageCode == 'ar' ? TextDirection.rtl : TextDirection.ltr;

      return Expanded(
        flex: 5,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          textDirection: cellDir,
          children: [
            Padding(
              padding: EdgeInsets.only(right: hasPadding ? 6.0 : 0.0),
              child: RichText(
                textDirection: cellDir,
                textAlign: TextAlign.start,
                text: cellTextSpan,
              ),
            ),
          ],
        ),
      );
      //Expanded(flex: 0, child: Text("")), //BORDER
    }

    void _clearRow() {
      rindexIn = -1;
      lstRowCell.clear();
    }

    Widget _createFinalRow(final int rowLength) {
      while (lstRowCell.length < colCount) {
        lstRowCell.add(TextSpan(text: ""));
      }

      return Container(
        alignment: Alignment(-1.0, -1.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: List<Widget>.generate(colCount, (int genIndex) => _createCell(rowLength: rowLength, hasPadding: (genIndex + 1 == colCount) ? false : true)),
        ),
      );
    }

    Widget? _createRow(final int maxIndex) {
      Widget? res = null;
      if (rindex + 1 <= maxIndex) {
        final Object obj = lstChapterObject[rindex + 1];
        if (obj is BookTitle) {
          ++rindex;
          res = RichText(text: obj.title);
        } else if (!(obj is TextSpan)) {
          res = lstChapterObject[++rindex] as Widget?; //TODO: NULL_SAFETY: not sure about it => TO TEST
        } else {
          _clearRow();
          Object lobj;
          int lindex;
          bool shouldLoop = true;
          while (shouldLoop) {
            lindex = rindex + 1;
            if (lindex <= maxIndex) {
              if (lstRowCell.length < colCount) {
                lobj = lstChapterObject[lindex];
                if (lobj is BookTitle) {
                  shouldLoop = false;
                } else if (!(lobj is TextSpan)) {
                  shouldLoop = false;
                } else {
                  lstRowCell.add(lstChapterObject[++rindex] as TextSpan); //TODO: NULL_SAFETY: not sure about it => TO TEST
                }
              } else {
                shouldLoop = false;
              }
            } else {
              shouldLoop = false;
            }
          }
          res = _createFinalRow(lstRowCell.length);
        }
      } else {
        res = null;
      }
      return res;
    }

    final Widget lstBuilder = ListView.builder(
      physics: ScrollPhysics(),
      shrinkWrap: true,
      itemCount: itemCount,
      itemBuilder: (context, index) {
        return _createRow(maxIndex);
      },
    );

    return lstBuilder;
  }
}

class _HomePageState extends State<HomePage> {

  //region -- Variables/States/Listeners --
  //Globals
  String gbbLocale = 'EN';
  String gbbAltLocale = 'EN';
  String gbbNames = 'k'; //All bbName to display
  int gbNumberTemp = 1;
  int gcCount = 1; //Not saved in prefs
  String gThemeName = 'DARK';
  VerseStyle gverseStyle = PStyle.instance.getVerseStyle('DARK');
  String gfontName = 'AveriaGruesaLibre.regular';
  double gfontSize = 20.0;
  Map gstyleHighlightSearch = {"styleName": "App1Style5b", "fg": Color(0xFF262525), "bg": Color(0xFFE2863F)};
  Paint gstyleHighLightSearchSquared = Paint();

  int gcolCount = 1;
  String gtype = "S"; //Internal, can be S, S2, S3, P, A, F
  String gquery = "1 1";
  String gqueryBBName = "k"; //Only one bbName
  String gqueryExpr = "";
  String gqueryType = "B"; //Internal, B=bible, F=fav
  int gqueryFavFilter = 0; //0: ALL or not used, else fav id
  int gqueryOrderBy = 0; //0=book (default), 1=date
  int gbNumber = 1;
  int gcNumber = 1;
  int gvNumber = 0;
  int gpNumber = 0;
  int gpNumberMax = 0;
  double goffset = 0.0;
  int gtabId = 0;
  String gbbName0 = "k"; //Use an internal bbName0 instead of gbbName0 when possible

  //States
  TextSpan navBarTitle = TextSpan(text: ''); //TODO: COLS, non problematic
  String navBarTitleShort = '';
  double dragDistance = 0.0; //Distance for swipe
  final double swipeThreshold = 100.0;

  List<Object> chapterObject = [];
  String chapterType = "S";
  late GlobalKey tabKey;
  SingleChildScrollView scrollTab = SingleChildScrollView();
  ScrollController? scrollTabController = Platform.isLinux ? ScrollController() : null;

  //Main Scaffold ref
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  //Listeners
  late ScrollController chapterTextScrollController;

  //Resources
  String mnuAboutValue = '', mnuAltLanguageValue = '', mnuBiblePreferredValue = '', mnuBibleToDisplayValue = '', mnuBooksValue = '', mnuChaptersValue = '', mnuFavoritesValue = '', mnuHelpValue = '';
  String mnuArtsValue = '', mnuPrblsValue = '', mnuHistoryValue = '';
  String mnuOpenChapterValue = '', mnuOpenCrValue = '', mnuClipboardClearValue = '', mnuClipboardAddVerseValue = '', mnuClipboardAddChapterValue = '';
  String mnuInviteFriendValue = '', mnuSearchValue = '', mnuSettingsValue = '', mnuSettingsBiblesValue = '', mnuSettingsFontsValue = '';
  String mnuSettingsFontSizeValue = '', mnuThemesValue = '';

  //endregion

  ///Keys
  Future<void> _setKeysForHomePage() async {
    bool _canPop() => Navigator.of(context).canPop();

    if (Platform.isMacOS || Platform.isLinux)
    {
      //region -- Escape --
      final HK.HotKey _hotKeyEsc = HK.HotKey(
        key: PhysicalKeyboardKey.escape, //KeyCode.escape,
        modifiers: null,
        scope: HK.HotKeyScope.inapp,
      );

      await HK.hotKeyManager.register(
        _hotKeyEsc,
        keyUpHandler: (hotKey) {
          //Never block the ESC key
          if (_canPop()) Navigator.of(context).pop();
        },
      );
      //endregion

      //region -- Left --
      final HK.HotKey _hotKeyArrowLeft = HK.HotKey(
        key: PhysicalKeyboardKey.arrowLeft, //KeyCode.arrowLeft,
        modifiers: null,
        scope: HK.HotKeyScope.inapp,
      );

      await HK.hotKeyManager.register(
        _hotKeyArrowLeft,
        keyUpHandler: (hotKey) {
          if (_canPop()) return; //Block the key

          _onClickBtnChapterLeft();
        },
      );
      //endregion

      //region -- Right --
      final HK.HotKey _hotKeyArrowRight = HK.HotKey(
        key: PhysicalKeyboardKey.arrowRight, //KeyCode.arrowRight,
        modifiers: null,
        scope: HK.HotKeyScope.inapp,
      );

      await HK.hotKeyManager.register(
        _hotKeyArrowRight,
        keyUpHandler: (hotKey) {
          if (_canPop()) return; //Block the key

          _onClickBtnChapterRight();
        },
      );
      //endregion

      //region -- Menu --
      final HK.HotKey _hotKeyM = HK.HotKey(
        key: PhysicalKeyboardKey.keyM, //KeyCode.keyM,
        modifiers: null,
        scope: HK.HotKeyScope.inapp,
      );

      await HK.hotKeyManager.register(
        _hotKeyM,
        keyUpHandler: (hotKey) {
          if (_canPop()) return; //Block the key

          if (scaffoldKey.currentState != null && scaffoldKey.currentState!.hasDrawer) scaffoldKey.currentState?.openDrawer();
        },
      );
      //endregion

      //region -- History --
      final HK.HotKey _hotKeyTab = HK.HotKey(
        key: PhysicalKeyboardKey.tab, //KeyCode.tab,
        modifiers: null,
        scope: HK.HotKeyScope.inapp,
      );

      await HK.hotKeyManager.register(
        _hotKeyTab,
        keyUpHandler: (hotKey) {
          if (_canPop()) return; //Block the key

          _onShowHistMenu(context, mnuHistoryValue);
        },
      );
      //endregion

      //region -- Search (S) --
      final HK.HotKey _hotKeyS = HK.HotKey(
        key: PhysicalKeyboardKey.keyS, //KeyCode.keyS,
        modifiers: null,
        scope: HK.HotKeyScope.inapp,
      );

      await HK.hotKeyManager.register(
        _hotKeyS,
        keyUpHandler: (hotKey) {
          if (_canPop()) return; //Block the key

          //TODO: search S
          _onShowSearchMenu(context, mnuSearchValue);
        },
      );
      //endregion

      //region -- Search (F) --
      final HK.HotKey _hotKeyF = HK.HotKey(
        key: PhysicalKeyboardKey.keyF, //KeyCode.keyF,
        modifiers: null,
        scope: HK.HotKeyScope.inapp,
      );

      await HK.hotKeyManager.register(
        _hotKeyF,
        keyUpHandler: (hotKey) {
          if (_canPop()) return; //Block the key

          //TODO: search F
          _onShowSearchMenu(context, mnuSearchValue);
        },
      );
      //endregion

      //region -- Books --
      final HK.HotKey _hotKeyB = HK.HotKey(
        key: PhysicalKeyboardKey.keyB, //KeyCode.keyB,
        modifiers: null,
        scope: HK.HotKeyScope.inapp,
      );

      await HK.hotKeyManager.register(
        _hotKeyB,
        keyUpHandler: (hotKey) {
          if (_canPop()) return; //Block the key

          _onShowBooksMenu(context);
        },
      );
      //endregion

      //region -- Arts --
      final HK.HotKey _hotKeyA = HK.HotKey(
        key: PhysicalKeyboardKey.keyA, //KeyCode.keyA,
        modifiers: null,
        scope: HK.HotKeyScope.inapp,
      );

      await HK.hotKeyManager.register(
        _hotKeyA,
        keyUpHandler: (hotKey) {
          if (_canPop()) return; //Block the key

          _onShowArtsMenu(context, mnuArtsValue);
        },
      );
      //endregion

      //region -- Prbls --
      final HK.HotKey _hotKeyP = HK.HotKey(
        key: PhysicalKeyboardKey.keyP, //KeyCode.keyP,
        modifiers: null,
        scope: HK.HotKeyScope.inapp,
      );

      await HK.hotKeyManager.register(
        _hotKeyP,
        keyUpHandler: (hotKey) {
          if (_canPop()) return; //Block the key

          _onShowPrblsMenu(context, mnuPrblsValue);
        },
      );
      //endregion
    }
  }

  ///Keys
  Future<void> _setKeysForSearchPage() async {
    bool _canPop() => Navigator.of(context).canPop();

    if (Platform.isMacOS || Platform.isLinux)
    {
      //region -- Escape --
      final HK.HotKey _hotKeyEsc = HK.HotKey(
        key: PhysicalKeyboardKey.escape, //KeyCode.escape,
        modifiers: null,
        scope: HK.HotKeyScope.inapp,
      );

      await HK.hotKeyManager.register(
        _hotKeyEsc,
        keyUpHandler: (hotKey) {
          //Never block the ESC key
          if (_canPop()) Navigator.of(context).pop();
        },
      );
      //endregion
    }
  }

  Future<void> _unSetAllKeys() async {
    if (Platform.isMacOS || Platform.isLinux) {
      await HK.hotKeyManager.unregisterAll(); //For hot reload, `unregisterAll()` needs to be called
    }
  }

  ///Call at start
  void _getInitResources() {
    void _setStyleHighlightSearch(String styleName) async {
      if (styleName.isEmpty) styleName = gstyleHighlightSearch["styleName"];
      final BibleSearchStyleBO bibleStyle = BibleSearchStyleBO(this.context);
      final Map? mapStyle = bibleStyle.getStylePropertiesFromId(styleName);
      if (mapStyle == null) return;
      final Map style = Map();
      style["styleName"] = styleName;
      style["fg"] = mapStyle["fg"];
      style["bg"] = mapStyle["bg"];
      gstyleHighlightSearch = style;
    }

    void _setThemeAndVerse(final String themeName) {
      gThemeName = themeName;
      gverseStyle = PStyle.instance.getVerseStyle(themeName);
      gstyleHighLightSearchSquared = Paint()
        ..color = gverseStyle.defaultFgColor
        ..style = PaintingStyle.stroke
        ..strokeCap = StrokeCap.round
        ..strokeWidth = 1.0;
    }

    Future<void> _refreshInitResources() async {
      try {
        gcolCount = await _getDynamicColumnCount();

        await _refreshResources(gbbLocale, gbbAltLocale);

        //was: if (gtabId == null) gtabId = 0;

        //Get current tab and set vars
        bool found = false;

        final dal = DbHelper.instance;
        Map c = await dal.getCacheTabById(gtabId);
        if (c.isNotEmpty) {
          found = true;
        } else {
          final int tabIdMax = await dal.getCacheTabIdMax();
          if (tabIdMax >= 0) {
            c = await dal.getCacheTabById(tabIdMax);
            if (c.isNotEmpty) {
              found = true;
              await _setAndSaveCurrentTabId(tabIdMax);
            }
          }
        }
        if (found) {
          gbNumber = c["bNumber"];
          gcNumber = c["cNumber"];
          gvNumber = c["vNumber"];
          gtype = c["tabType"];
          goffset = c["offset"];
          gquery = c["fullQuery"];
          gqueryExpr = ""; //Default
          if (gtype == "F") {
            gpNumber = gpNumberMax = 1; //Are set in _onRefreshChapterWidget (here under)
          } else if (gtype == "S" && gbNumber == 0 && gcNumber == 0 && gvNumber == 0) {
            gtype = "S2";
            gpNumber = gpNumberMax = 1; //Are set in _onRefreshChapterWidget (here under)
          } else if (gtype == "S" && gvNumber > 0 && gbNumber > 0 && gcNumber > 0) {
            gtype = "S3";
            gpNumber = gpNumberMax = 0;
          } else {
            //S
            gpNumber = gpNumberMax = 0;
          }
        } else {
          //Default (not found)
          gbNumber = 1;
          gcNumber = 1;
          gvNumber = 0;
          gtype = "S";
          gquery = "$gbNumber $gcNumber";
          gqueryExpr = "";
          gpNumber = gpNumberMax = 0;
        }

        _onRefreshChapterWidget();
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    try {
      final Future<String> themeNameFut = P.Prefs.getThemeName;
      themeNameFut.then((value) => _setThemeAndVerse(value));

      final Future<String> bbLocaleFut = P.Prefs.getBibleLocale;
      bbLocaleFut.then((value) => gbbLocale = value);

      final Future<String> bbAltLocaleFut = P.Prefs.getBibleAltLocale;
      bbAltLocaleFut.then((value) => gbbAltLocale = value);

      final Future<String> bbNameFut = P.Prefs.getBibleName;
      bbNameFut.then((value) => gbbNames = value);
      bbNameFut.whenComplete(() => gbbName0 = (gbbNames.length > 0) ? gbbNames[0] : "k");

      final Future<String> fontNameFut = P.Prefs.getFontName;
      fontNameFut.then((value) => gfontName = value);
      fontNameFut.whenComplete(() => PStyle.instance.fontFamily = gfontName);

      final Future<String> fontSizeFut = P.Prefs.getFontSize;
      fontSizeFut.then((value) => gfontSize = (double.tryParse(value.toString()) == null ? 20.0 : double.tryParse(value.toString()))!);
      fontSizeFut.whenComplete(() => PStyle.instance.fontSize = gfontSize);

      final Future<String> tabSelectedFut = P.Prefs.getTabSelected;
      tabSelectedFut.then((value) => gtabId = int.tryParse(value)!);

      String styleName = "";
      final Future<String> styleHighlightSearchFut = P.Prefs.getStyleHighlightSearch;
      styleHighlightSearchFut.then((value) => styleName = value);
      styleHighlightSearchFut.whenComplete(() => _setStyleHighlightSearch(styleName));

      final Future<double> appBarFut = P.getAppBarHeight(context);
      appBarFut.then((value) => gAppBarHeight = value);
      appBarFut.whenComplete(() { if (P.isDebug) print("appBarHeight: $gAppBarHeight"); });

      Future<List<String>> allPrefFut = Future.wait([
        themeNameFut,
        bbLocaleFut,
        bbAltLocaleFut,
        bbNameFut,
        fontNameFut,
        fontSizeFut,
        tabSelectedFut,
        styleHighlightSearchFut,
      ]);
      allPrefFut.then((value) {
        if (P.isDebug) print("allPrefFut: $value");
        if (gbbAltLocale != 'EN' && gbbAltLocale != 'ES' && gbbAltLocale != 'FR' && gbbAltLocale != 'IT' && gbbAltLocale != 'PT') {
          if (gbbLocale == 'EN' || gbbLocale == 'ES' || gbbLocale == 'FR' || gbbLocale == 'IT' || gbbLocale == 'PT') {
            gbbAltLocale = gbbLocale;
          } else {
            gbbAltLocale = 'EN';
          }
          P.Prefs.saveBibleAltLocale('EN');
        }
      });
      allPrefFut.whenComplete(() async {
        await P.refreshCustomVerseStylesDisplayed(gverseStyle is LightVerseStyle);
        _refreshInitResources();
        _setKeysForHomePage();
      });
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  //region -- Resources --
  ///Refresh strings of page
  Future<void> _refreshResources(final String localeStr, final String localeAltStr) async {
    final BBLocale bbLocale = P.convertStringToBBLocale(localeStr);
    final BBLocale bbAltLocale = P.convertStringToBBLocale(localeAltStr);
    R.setLocale(bbLocale, bbAltLocale);
    await R.setFav();
    setState(() {
      //Drawer:
      mnuAboutValue = R.getString(R.id.mnuAbout);
      mnuArtsValue = R.getString(R.id.mnuArts);
      mnuAltLanguageValue = R.getString(R.id.mnuAltLanguage);
      mnuBiblePreferredValue = R.getString(R.id.mnuBiblePreferred);
      mnuBibleToDisplayValue = R.getString(R.id.mnuBibleToDisplay);
      mnuBooksValue = R.getString(R.id.mnuBooks);
      mnuChaptersValue = R.getString(R.id.mnuChapters);
      mnuFavoritesValue = R.getString(R.id.mnuFavorites);
      mnuHelpValue = R.getString(R.id.mnuHelp);
      mnuHistoryValue = R.getString(R.id.mnuHistory);
      mnuInviteFriendValue = R.getString(R.id.mnuInviteFriend);
      mnuPrblsValue = R.getString(R.id.mnuPrbls);
      mnuThemesValue = R.getString(R.id.mnuThemes);
      mnuSearchValue = R.getString(R.id.mnuSearch);
      mnuSettingsValue = R.getString(R.id.mnuSettings);
      mnuSettingsBiblesValue = R.getString(R.id.mnuSettingsBibles);
      mnuSettingsFontsValue = R.getString(R.id.mnuSettingsFonts);
      mnuSettingsFontSizeValue = R.getString(R.id.mnuSettingsFontSize);
      //Context menu:
      mnuOpenChapterValue = R.getString(R.id.mnuOpenChapter);
      mnuOpenCrValue = R.getString(R.id.mnuOpenCR);
      mnuClipboardClearValue = R.getString(R.id.mnuClipboardClear);
      mnuClipboardAddVerseValue = R.getString(R.id.mnuClipboardAddVerse);
      mnuClipboardAddChapterValue = R.getString(R.id.mnuClipboardAddChapter);
    });
  }

  ///Call anywhere after start
  Future<void> _onRefreshResources() async {
    try {
      await _refreshResources(gbbLocale, gbbAltLocale);
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  Future<int> _getDynamicColumnCount() async {
    int dc = 1;

    try {
      final String dcCheck = await P.Prefs.getLayoutDynamic1;
      if (dcCheck.isEmpty) {
        await P.Prefs.saveLayoutDynamic1("$dc");
      } else {
        dc = int.parse(dcCheck);
      }
    } catch (ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return dc;
  }

  //endregion

  //region -- Themes/Styles --
  //** Theme **
  void _refreshAndChangeTheme(final BuildContext context, final String newThemeName) async {
    await P.Prefs.saveThemeName(newThemeName);

    gThemeName = newThemeName;
    gverseStyle = PStyle.instance.getVerseStyle(newThemeName);
    gstyleHighLightSearchSquared = Paint()
      ..color = gverseStyle.defaultFgColor
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 1.0;

    final bool isLightThemeAsDefault = gverseStyle is LightVerseStyle;
    await P.refreshCustomVerseStylesDisplayed(isLightThemeAsDefault);

    _onRefreshChapterWidget();

    _AppState.of(context)?._getAppTheme();
    //App refreshed
  }

  ///Select theme
  void _onChangeTheme() {
    try {
      _refreshAndChangeTheme(context, gThemeName.compareTo('LIGHT') == 0 ? 'DARK' : 'LIGHT');
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  //endregion

  //region -- Chapter & ScrollTab Widgets --
  //** Chapter **

  ///Load book chapter
  ///@chapterStep: move (0, +1, -1)
  void _onOpenChapterWidgetBook(final int bNumber, final int cNumber, final int chapterStep, final bool isRefresh, final double offset) async {
    final dal = DbHelper.instance;
    final String bbName0 = gbbName0;

    Future<void> _refreshChapterWidget(final String newChapterType, final List<Object> newChapterObject, final TextSpan newNavBarTitleStyled, final String newNavBarTitleShort) async {
      if (chapterObject.isNotEmpty) chapterObject.clear();
      setState(() {
        navBarTitleShort = newNavBarTitleShort;
        navBarTitle = newNavBarTitleStyled;
        chapterType = newChapterType;
        chapterObject = newChapterObject;
      });
      _manageTab(isRefresh, newNavBarTitleShort);
      _scrollChapterTextController(goffset);
    }

    void _prepareChapterWidget(final List<Map> lstVerse, final String barTitle, final String barTitleShort) async {
      try {
        //Format verse
        final List<Object> chapterObject = [];
        for(var row in lstVerse) {
          final TextSpan? rowStyled = await _getVerseTextSpan(row);
          if (rowStyled != null) chapterObject.add(rowStyled);
        }
        final TextSpan barTitleStyled = _getBarTitleStyled(barTitle, null);

        await _refreshChapterWidget("S", chapterObject, barTitleStyled, barTitleShort);
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    ///@chapterStep: move (0, +1, -1)
    Future<void> _checkChapterWidget(final int bNumber, final int cNumber, final int chapterStep) async {
      String _getBarTitle(final String bbName, final int bNumber, String title, int chapter) {
        try {
          if (bNumber == 22) {
            switch (bbName) {
              case "k":
              case "2":
                title = 'Song Of Solomon';
                break;
              case "v":
                break;
              case "l":
              case "o":
                title = 'Cantique Des Cantiques';
                break;
              case "a":
                title = 'Cantares De Salomão';
                break;
              case "d":
                title = 'Cantico Dei Cantici';
                break;
            }
          }
          return "$title $chapter";
        } catch(ex, stacktrace) {
          if (P.isDebug) P.printLog(ex, stacktrace);
        }
        return '';
      }

      String _getBarTitleShort(final String title, final int chapter) => "$title $chapter".trim();

      //Checks
      int newgcNumber = cNumber + chapterStep;
      if (newgcNumber < 1) return;

      final Map ci = await dal.getBibleCiByBook(bbName0, bNumber);
      if (ci.isEmpty) return;

      //It's valid
      gcCount = ci['cCount'];
      if (newgcNumber > gcCount) {
        newgcNumber = gcCount;
      }
      gbNumber = bNumber;
      gcNumber = newgcNumber;
      gvNumber = 0;
      gtype = "S";
      gquery = "$gbNumber $gcNumber";
      gqueryExpr = "";
      gpNumber = gpNumberMax = 0;
      goffset = offset;

      late String title;
      late String titleShort;
      late String barTitle;
      late String barTitleShort;
      final Future<Map> titleFut = dal.getBookRef(bbName0, gbNumber);
      titleFut.then((value) => title = value['bName']);
      titleFut.then((value) => titleShort = value['bsName']);
      titleFut.whenComplete(() => barTitle = _getBarTitle(bbName0, gbNumber, title, gcNumber));
      titleFut.whenComplete(() => barTitleShort = _getBarTitleShort(titleShort, gcNumber));

      final List<Map> lstVerse = [];
      final Future<List<Map>> chapterFut = dal.getVerses(gbbNames, gbNumber, gcNumber, 1, 0);
      chapterFut.then((fut) => fut.forEach((row) {
            lstVerse.add(row);
          }));

      Future.wait([titleFut, chapterFut]).whenComplete(() => _prepareChapterWidget(lstVerse, barTitle, barTitleShort));
    }

    void _init(final int bNumber, final int cNumber, final int chapterStep) {
      _checkChapterWidget(bNumber, cNumber, chapterStep);
    }

    _init(bNumber, cNumber, chapterStep);
  }

  ///Load search
  ///@pageStep: 0, 1, -1
  void _onOpenChapterWidgetSearch(final String queryType, final int queryOrderBy, final String queryBBName, final String query, final int pageStep, final bool isRefresh, final int queryFavFilter, final double offset) async {
    final dal = DbHelper.instance;

    Future<void> _refreshChapterWidget(final String newChapterType, final List<Object> newChapterObject, final TextSpan newNavBarTitleStyled, final String newNavBarTitleShort) async {
      if (chapterObject.isNotEmpty) chapterObject.clear();
      setState(() {
        navBarTitleShort = newNavBarTitleShort;
        navBarTitle = newNavBarTitleStyled;
        chapterType = newChapterType;
        chapterObject = newChapterObject;
      });
      _manageTab(isRefresh, newNavBarTitleShort);
      _scrollChapterTextController(goffset);
    }

    void _prepareChapterWidget(final List<Map> lstVerse, final String barTitle, final String barTitleShort, final String? barTitleFromTo) async {
      try {
        //Format verse
        final double vNumberFontSize = PStyle.instance.fontSize; //was - 2;
        final TextStyle verseTitleStyle =
            TextStyle(fontFamily: PStyle.instance.fontFamily, fontSize: vNumberFontSize, backgroundColor: gverseStyle.accentFgColor, color: gverseStyle.inverseDefaultFgColor, fontStyle: FontStyle.normal);

        int rowNr = 0;
        int prevBNumber = -1;
        int prevCNumber = -1;
        final List<Object> chapterObject = [];
        for(var row in lstVerse) {
          //Put logic here for several styles
          final int bNumber = row['bNumber'];
          final int cNumber = row['cNumber'];

          final bool shouldDisplayBookChapter = (rowNr == 0) || (prevBNumber != bNumber || prevCNumber != cNumber);
          if (shouldDisplayBookChapter) {
            final String bookTitleShort = row['bsName'];
            final TextSpan bookTitleStyled = TextSpan(
              text: '$bookTitleShort $cNumber ',
              style: verseTitleStyle,
            );
            chapterObject.add(BookTitle(bookTitleStyled));
          }
          final TextSpan? rowStyled = await _getVerseTextSpan(row);
          if (rowStyled != null) chapterObject.add(rowStyled);

          //Next
          prevBNumber = bNumber;
          prevCNumber = cNumber;
          rowNr++;
        }
        final TextSpan barTitleStyled = _getBarTitleStyled(barTitle, barTitleFromTo);

        await _refreshChapterWidget("S", chapterObject, barTitleStyled, barTitleShort); //TODO: problem of "S" here, may be "F"
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    ///Check
    void _checkChapterWidget() async {
      if ((gpNumber + pageStep) < 0) return;
      if ((gpNumber + pageStep) > gpNumberMax) return;

      //It's valid
      gbNumber = 0;
      gcNumber = 0;
      gvNumber = 0;
      gtype = queryType == "F" ? "F" : "S2";
      gqueryType = queryType;
      gqueryFavFilter = queryFavFilter;
      gqueryOrderBy = queryOrderBy;
      gqueryBBName = queryBBName;
      gquery = query;
      gpNumber = gpNumber + pageStep;
      goffset = offset;

      final Map searchParams = await dal.getSearchTypeParams(queryType, queryOrderBy, queryBBName, query, queryFavFilter);
      gqueryExpr = (searchParams.isNotEmpty && searchParams.containsKey("queryExpr")) ? searchParams["queryExpr"] : "";
      //already set gpNumberMax;

      final int visPageFrom = gpNumber + 1;
      final int visPageTo = gpNumberMax + 1;
      final String barTitle = query == "" && queryType == "F" ? "${R.getString(R.id.favAll)} ($visPageFrom/$visPageTo)" : "$query ($visPageFrom/$visPageTo)";
      final String barTitleShort = query;

      final List<Map> lstVerse = await dal.searchBible(queryType, queryOrderBy, queryBBName, query, gpNumber, gbbNames, queryFavFilter);

      String _getVerseFromTo(final Map verse) => "${verse['bsName']} ${verse['cNumber']}.${verse['vNumber']}";

      String? barTitleFromTo = null;
      if (queryType != "F") {
        if (visPageTo >= visPageFrom) {
          if (lstVerse.isNotEmpty && lstVerse.length >= 2) {
            final int indexLast = lstVerse.length - gbbNames.length;
            final String verseFrom = _getVerseFromTo(lstVerse[0]);
            final String verseTo = _getVerseFromTo(lstVerse[indexLast]);
            barTitleFromTo = "$verseFrom - $verseTo";
          }
        }
      }

      _prepareChapterWidget(lstVerse, barTitle, barTitleShort, barTitleFromTo);
    }

    ///Load prbl
    void _init() {
      _checkChapterWidget();
    }

    _init();
  }

  ///Load prbl
  void _onOpenChapterWidgetPrbl(final String query, final bool isRefresh, final double offset) async {
    final dal = DbHelper.instance;
    final String bbName0 = gbbName0;

    Future<void> _refreshChapterWidget(final String newChapterType, final List<Object> newChapterObject, final TextSpan newNavBarTitleStyled, final String newNavBarTitleShort) async {
      if (chapterObject.isNotEmpty) chapterObject.clear();
      setState(() {
        navBarTitleShort = newNavBarTitleShort;
        navBarTitle = newNavBarTitleStyled;
        chapterType = newChapterType;
        chapterObject = newChapterObject;
      });
      _manageTab(isRefresh, newNavBarTitleShort);
      _scrollChapterTextController(goffset);
    }

    void _prepareChapterWidget(final List<Map> lstVerse, final String barTitle, final String barTitleShort, final String bookTitleShort) async {
      try {
        //Format verse
        final double vNumberFontSize = PStyle.instance.fontSize; //was - 2;
        final TextStyle verseTitleStyle =
            TextStyle(fontFamily: PStyle.instance.fontFamily, fontSize: vNumberFontSize, backgroundColor: gverseStyle.accentFgColor, color: gverseStyle.inverseDefaultFgColor, fontStyle: FontStyle.normal);
        final TextSpan bookTitleStyled = TextSpan(
          text: bookTitleShort,
          style: verseTitleStyle,
        );

        final List<Object> chapterObject = [];
        chapterObject.add(BookTitle(bookTitleStyled));
        for(var row in lstVerse) {
          final TextSpan? rowStyled = await _getVerseTextSpan(row);
          if (rowStyled != null) chapterObject.add(rowStyled);
        }
        final TextSpan barTitleStyled = _getBarTitleStyled(barTitle, null);

        await _refreshChapterWidget("P", chapterObject, barTitleStyled, barTitleShort);
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    ///Check
    void _checkChapterWidget() async {
      //Get prbl
      final Prbl prbl = Prbl();
      final String prblNr = query.replaceFirst("PRBL", "");
      final String? prblRef = prbl.getPrblRef(prblNr);
      if (prblRef == null) return;
      final List<String> lstPrblRef = prblRef.split(' ');
      if (lstPrblRef.length != 4) return;

      final int bNumber = int.parse(lstPrblRef[0]);
      final int cNumber = int.parse(lstPrblRef[1]);
      final int vNumberFrom = int.parse(lstPrblRef[2]);
      final int vNumberTo = int.parse(lstPrblRef[3]);

      //It's valid
      gbNumber = bNumber;
      gcNumber = cNumber;
      gvNumber = 0;
      gtype = "P";
      gquery = query;
      gqueryExpr = "";
      gpNumber = gpNumberMax = 0;
      goffset = offset;

      final String barTitle = prbl.getPrblDesc(prblNr);
      final String barTitleShort = query;

      final List<Map> lstVerse = await dal.getVerses(gbbNames, bNumber, cNumber, vNumberFrom, vNumberTo);

      final Map bookRef = await dal.getBookRef(bbName0, bNumber);
      final String bookTitleShort = bookRef['bsName'];

      _prepareChapterWidget(lstVerse, barTitle, barTitleShort, "$bookTitleShort $cNumber ");
    }

    ///Load prbl
    void _init() {
      _checkChapterWidget();
    }

    _init();
  }

  ///Load art
  void _onOpenChapterWidgetArt(final String query, final bool isRefresh, final double offset) async {
    final dal = DbHelper.instance;
    final String bbName0 = gbbName0;
    final String bbLocale = gbbAltLocale;

    Future<void> _refreshChapterWidget(final String newChapterType, final List<Object> newChapterObject, final TextSpan newNavBarTitleStyled, final String newNavBarTitleShort) async {
      if (chapterObject.isNotEmpty) chapterObject.clear();
      setState(() {
        navBarTitleShort = newNavBarTitleShort;
        navBarTitle = newNavBarTitleStyled;
        chapterType = newChapterType;
        chapterObject = newChapterObject;
      });
      _manageTab(isRefresh, newNavBarTitleShort);
      _scrollChapterTextController(goffset);
    }

    void _prepareChapterWidget() async {
      try {
        //Variables
        Map<int, Object> newChapterObjectOrder = {};
        int indexChapterOrder = -1;
        List<Map> lstHref = [];

        //Styles
        final fontSize = PStyle.instance.fontSize; //PStyle.instance.fontSizeForMenu(20.0);
        final fontFamily = PStyle.instance.fontFamily; //PStyle.instance.fontFamilyForMenu();
        final double vNumberFontSize = PStyle.instance.fontSize; //was - 2;
        final TextStyle verseTitleStyle =
            TextStyle(fontFamily: PStyle.instance.fontFamily, fontSize: vNumberFontSize, backgroundColor: gverseStyle.accentFgColor, color: gverseStyle.inverseDefaultFgColor, fontStyle: FontStyle.normal);
        final TextStyle urlStyle = TextStyle(fontFamily: PStyle.instance.fontFamily, fontSize: PStyle.instance.fontSize, color: gverseStyle.accentFgColor, decoration: TextDecoration.underline);

        Future<void> _prepareVerses(final List<Map> lstVerse, final int bNumber, final int cNumber) async {
          if (lstVerse.isNotEmpty) {
            final Map row = await dal.getBookRef(bbName0, bNumber);
            final String bookTitleShort = row['bsName'];

            final TextSpan bookTitleStyled = TextSpan(
              text: '$bookTitleShort $cNumber ',
              style: verseTitleStyle,
            );
            newChapterObjectOrder[++indexChapterOrder] = BookTitle(bookTitleStyled);
          }

          for(var row in lstVerse) {
            final int index = ++indexChapterOrder;
            final TextSpan? rowStyled = await _getVerseTextSpan(row);
            if (rowStyled != null) newChapterObjectOrder[index] = rowStyled;
          }
        }

        Future<void> _prepareHrefBlock(final String artType, final int blockNrToFind) async {
          const space = SizedBox(height: 20.0);
          final String toastCopiedClipboard = R.getString(R.id.copiedClipboard);

          lstHref.forEach((row) {
            final int? blockNr = int.tryParse(row['b']);
            if (blockNr != null && blockNr == blockNrToFind) {
              final int index = ++indexChapterOrder;
              final int indexP1 = ++indexChapterOrder;
              final String url = row['u'];
              final String desc = row['d'];

              final TextSpan urlStyled = TextSpan(
                text: desc,
                style: urlStyle,
              );

              final btnStyled = InkWell(
                  child: Container(
                    alignment: Alignment(-1.0, -1.0),
                    child: RichText(textAlign: TextAlign.start, text: urlStyled),
                  ),
                  onTap: () async {
                    if (artType == "AHREF") {
                      await P.launchUrl(super.context, url);
                    } else {
                      await P.copyTextToClipboard(super.context, url, toastCopiedClipboard, true); //TODO: not sure about context
                    }
                  });

              newChapterObjectOrder[index] = btnStyled;
              newChapterObjectOrder[indexP1] = space;
            }
          });
        }

        List<Object> _getChapterObject(final String ha) {
          if (indexChapterOrder < 0) return [];
          final List<Object> newChapterObject = [];
          for (int i = 0; i <= indexChapterOrder; i++) {
            if (newChapterObjectOrder[i] != null) newChapterObject.add(newChapterObjectOrder[i]!);
          }
          return newChapterObject;
        }

        //Get art
        final Art art = Art();
        final String artType = (query == "ART26" || query == "ART133" || query == "ART136")
            ? "AHREF"
            : (query == "ART77")
                ? "ACOPY"
                : "R";
        final String artIndexNr = query.replaceFirst("ART", "");
        final String ha = await art.getArtDesc(artIndexNr);
        String artContent = await art.getArtContent(this.context, bbLocale, "$query\_CONTENT");

        //Wrong
        int bNumber;
        int cNumber;
        int vNumberFrom;
        int vNumberTo;

        //It's valid
        gbNumber = 0;
        gcNumber = 0;
        gvNumber = 0;
        gtype = "A";
        gquery = query;
        gqueryExpr = "";
        gpNumber = gpNumberMax = 0;
        goffset = offset;

        final String barTitle = P.isDebug ? "($artIndexNr) $ha" : ha;
        final String barTitleShort = query;
        final TextSpan barTitleStyled = _getBarTitleStyled(barTitle, null);

        //Var block
        int rtagStart, rtagEnd;
        List<String> rdigits;
        newChapterObjectOrder = {};

        //Prepare html
        artContent = artContent.replaceAll(RegExp("<HB>.*</HB>"), '<br>').replaceAll("<HA/>", '');
        artContent = artContent.replaceAll("\\'", "'").replaceAll("\\n", '').replaceAll('\\"', '"');
        artContent = artContent.replaceAll("<HS>", "<br><span><u>").replaceAll("</HS>", "</u></span><br><br>").replaceAll("<H>", "<h1><u>").replaceAll("</H>", "</u></h1>");

        //region -- R blocks --
        if (artType == "R") {
          //For all <R>
          const String tagStart = "<R>";
          const String tagEnd = "</R>";

          while (true) {
            rtagStart = artContent.indexOf(tagStart);
            if (rtagStart < 0) break;

            rtagEnd = artContent.indexOf(tagEnd, rtagStart);
            if (rtagEnd < 0) continue;

            rdigits = artContent.substring(rtagStart, rtagEnd).replaceFirst(tagStart, '').split(' ');
            if (rdigits.length == 4) {
              bNumber = int.parse(rdigits[0]);
              cNumber = int.parse(rdigits[1]);
              vNumberFrom = int.parse(rdigits[2]);
              vNumberTo = int.parse(rdigits[3]);
            } else {
              bNumber = int.parse(rdigits[1]);
              cNumber = int.parse(rdigits[2]);
              vNumberFrom = int.parse(rdigits[3]);
              vNumberTo = int.parse(rdigits[4]);
            }

            //Before
            final String beforeBlock = artContent.substring(0, rtagStart);
            newChapterObjectOrder[++indexChapterOrder] = HTML.HtmlWidget(
              beforeBlock,
              textStyle: TextStyle(fontFamily: fontFamily, fontSize: fontSize),
            );

            //Verses
            final List<Map> lstVerse = [];
            final List<Map> verses = await dal.getVerses(gbbNames, bNumber, cNumber, vNumberFrom, vNumberTo);
            verses.forEach((row) {
              lstVerse.add(row);
            });
            await _prepareVerses(lstVerse, bNumber, cNumber);

            //Remove
            artContent = artContent.substring(rtagEnd + tagEnd.length);
          }
        }
        //endregion

        //region -- AHREF/ACOPY blocks --
        else if (artType == "AHREF" || artType == "ACOPY") {
          //For all <AHREF/>
          const String tagStart = "<AHREF/>";
          lstHref = [];

          final String contentArtId = (query == "ART26") ? "ART_APP_YT_CONTENT" : (query == "ART77") ? "ART_APP_POD_CONTENT" : (query == "ART133") ? "ART_APP_TRSG_CONTENT" : "ART_APP_TOOLS_CONTENT";
          String artHrefContent = await art.getArtContent(this.context, bbLocale, contentArtId);
          artHrefContent = artHrefContent.replaceAll("\\n", '').replaceAll("\\'", "'");
          final List<String> lstHrefRow = artHrefContent.split("<br>");
          lstHrefRow.forEach((row) {
            final List<String> lstRow = row.split("|");
            final String blockNr = lstRow[0];
            final String url = lstRow[1];
            final String desc = lstRow[2];
            lstHref.add({"b": blockNr, "u": url, "d": desc});
          });

          if (lstHrefRow.isNotEmpty) lstHrefRow.clear();
          //was: artHrefContent = null;

          int blockNr = 0;
          while (true) {
            rtagStart = artContent.indexOf(tagStart);
            if (rtagStart < 0) break;

            //Before
            final String beforeBlock = artContent.substring(0, rtagStart);
            newChapterObjectOrder[++indexChapterOrder] = HTML.HtmlWidget(
              beforeBlock,
              textStyle: TextStyle(fontFamily: fontFamily, fontSize: fontSize),
            );

            //Hrefs
            await _prepareHrefBlock(artType, blockNr);

            //Remove
            artContent = artContent.substring(rtagStart + tagStart.length);

            blockNr++;
          }
        }
        //endregion

        //End
        final String afterBlock = artContent;
        newChapterObjectOrder[++indexChapterOrder] = HTML.HtmlWidget(
          afterBlock,
          textStyle: TextStyle(fontFamily: fontFamily, fontSize: fontSize),
        );

        await _refreshChapterWidget("A", _getChapterObject(ha), barTitleStyled, barTitleShort);
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    ///Load art
    void _init() {
      _prepareChapterWidget();
    }

    _init();
  }

  ///Load cr
  void _onOpenChapterWidgetCR(final int bNumber, final int cNumber, final int vNumber, final bool isRefresh, final double offset) async {
    final dal = DbHelper.instance;
    //not used: final String bbName0 = gbbName0;

    Future<void> _refreshChapterWidget(final String newChapterType, final List<Object> newChapterObject, final TextSpan newNavBarTitleStyled, final String newNavBarTitleShort) async {
      if (chapterObject.isNotEmpty) chapterObject.clear();
      setState(() {
        navBarTitleShort = newNavBarTitleShort;
        navBarTitle = newNavBarTitleStyled;
        chapterType = newChapterType;
        chapterObject = newChapterObject;
      });
      _manageTab(isRefresh, newNavBarTitleShort);
      _scrollChapterTextController(goffset);
    }

    void _prepareChapterWidget(final List<Map> lstVerse, final String barTitle, final String barTitleShort) async {
      try {
        //Format verse
        final double vNumberFontSize = PStyle.instance.fontSize; //was - 2;
        final TextStyle verseTitleStyle =
        TextStyle(fontFamily: PStyle.instance.fontFamily, fontSize: vNumberFontSize, backgroundColor: gverseStyle.accentFgColor, color: gverseStyle.inverseDefaultFgColor, fontStyle: FontStyle.normal);

        int rowNr = 0;
        int prevBNumber = -1;
        int prevCNumber = -1;
        final List<Object> chapterObject = [];
        for(var row in lstVerse) {
          //Put logic here for several styles
          final int bNumber = row['bNumber'];
          final int cNumber = row['cNumber'];

          final bool shouldDisplayBookChapter = (rowNr == 0) || (prevBNumber != bNumber || prevCNumber != cNumber);
          if (shouldDisplayBookChapter) {
            final String bookTitleShort = row['bsName'];
            final TextSpan bookTitleStyled = TextSpan(
              text: '$bookTitleShort $cNumber ',
              style: verseTitleStyle,
            );
            chapterObject.add(BookTitle(bookTitleStyled));
          }

          final TextSpan? rowStyled = await _getVerseTextSpan(row);
          if (rowStyled != null) chapterObject.add(rowStyled);

          //Next
          prevBNumber = bNumber;
          prevCNumber = cNumber;
          rowNr++;
        }
        final TextSpan barTitleStyled = _getBarTitleStyled(barTitle, null);

        await _refreshChapterWidget("S", chapterObject, barTitleStyled, barTitleShort);
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    Future<void> _checkChapterWidget() async {
      //It's valid
      gcCount = 0;
      gbNumber = bNumber;
      gcNumber = cNumber;
      gvNumber = vNumber;
      gtype = "S3";
      gquery = "$gbNumber $gcNumber $gvNumber CR:";
      gqueryExpr = "";
      gpNumber = gpNumberMax = 0;
      goffset = offset;

      String barTitle = R.getString(R.id.cr);
      String barTitleShort = "CR";

      final List<Map> lstVerse = [];
      final Future<List<Map>> chapterFut = dal.getCrossReferences(gbbNames, gbNumber, gcNumber, gvNumber);
      chapterFut.then((fut) => fut.forEach((row) {
        lstVerse.add(row);
      }));
      chapterFut.whenComplete(() => _prepareChapterWidget(lstVerse, barTitle, barTitleShort));
    }

    void _init() {
      _checkChapterWidget();
    }

    _init();
  }

  ///Scroll chapter widget
  Future<void> _scrollChapterTextController(final double offset) async {
    await chapterTextScrollController.animateTo(offset, duration: Duration(milliseconds: 100), curve: Curves.easeInOut);
  }

  //Position
  double _getPositionOfScrollControllerChapterText() => chapterTextScrollController.position.pixels.floorToDouble();

  ///Navigation Left/Right
  ///Get art nr for move prev (1), next (2)
  ///Returns '' when error or not found
  String _getArtNrForMove(final int elementIndex) {
    try {
      final Art art = Art();
      final String artNr = gquery.replaceFirst("ART", "");

      final int indexOrder = art.getArtOrderIndexOfValue(artNr);
      if (indexOrder < 0) return '';

      final List<String> lstOrder = art.getListArtOrder(indexOrder, true);
      if (lstOrder.length != 3) return '';

      final String newArtNr = lstOrder[elementIndex];
      if (newArtNr.isEmpty) return '';
      return newArtNr;
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return '';
  }

  ///Get prbl nr for move (step: -1 or +1)
  ///Returns 0 when error or not found
  int _getPrblNrForMove(final int step) {
    try {
      final int? prblNr = int.tryParse(gquery.replaceFirst("PRBL", ""));
      if (prblNr == null) return 0;

      final int newPrblNr = prblNr + step;
      final Prbl prbl = Prbl();
      final String? newPrblRef = prbl.getPrblRef(newPrblNr.toString());
      if (newPrblRef == null) return 0;
      return newPrblNr;
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return 0;
  }

  void _onClickBtnChapterLeft() {
    if (gtype == "S") {
      _onOpenChapterWidgetBook(gbNumber, gcNumber, -1, true, 0.0);
    } else if (gtype == "S2" || gtype == "F") {
      _onOpenChapterWidgetSearch(gqueryType, gqueryOrderBy, gqueryBBName, gquery, -1, true, gqueryFavFilter, 0.0);
    } else if (gtype == "A") {
      final String newArtNr = _getArtNrForMove(1);
      if (newArtNr.isEmpty) return;
      _onOpenChapterWidgetArt("ART$newArtNr", true, 0.0);
    } else if (gtype == "P") {
      final int newPrblNr = _getPrblNrForMove(-1);
      if (newPrblNr == 0) return;
      _onOpenChapterWidgetPrbl("PRBL$newPrblNr", true, 0.0);
    }
  }

  void _onClickBtnChapterRight() {
    if (gtype == "S") {
      _onOpenChapterWidgetBook(gbNumber, gcNumber, 1, true, 0.0);
    } else if (gtype == "S2" || gtype == "F") {
      _onOpenChapterWidgetSearch(gqueryType, gqueryOrderBy, gqueryBBName, gquery, 1, true, gqueryFavFilter, 0.0);
    } else if (gtype == "A") {
      final String newArtNr = _getArtNrForMove(2);
      if (newArtNr.isEmpty) return;
      _onOpenChapterWidgetArt("ART$newArtNr", true, 0.0);
    } else if (gtype == "P") {
      final int newPrblNr = _getPrblNrForMove(1);
      if (newPrblNr == 0) return;
      _onOpenChapterWidgetPrbl("PRBL$newPrblNr", true, 0.0);
    }
  }

  void _onRefreshChapterWidget() async {
    switch (gtype) {
      case "S":
        _onOpenChapterWidgetBook(gbNumber, gcNumber, 0, true, goffset);
        break;
      case "S2":
      case "F":
        void _onOpenChapterWidgetSearchCall() async {
          await _setPageNumberParams();
          _onOpenChapterWidgetSearch(gqueryType, gqueryOrderBy, gqueryBBName, gquery, 0, true, gqueryFavFilter, goffset);
        }
        _onOpenChapterWidgetSearchCall();
        break;
      case "S3":
        _onOpenChapterWidgetCR(gbNumber, gcNumber, gvNumber, true, goffset);
        break;
      case "P":
        _onOpenChapterWidgetPrbl(gquery, true, goffset);
        break;
      case "A":
        _onOpenChapterWidgetArt(gquery, true, goffset);
        break;
    }
  }

  //Tabs
  void _onRefreshScrollTab(final bool shouldScrollToBtn) async {
    try {
      Map c;
      int tabId;
      String tabTitle;
      String tabType;
      int favFilter;
      Text text;
      late SingleChildScrollView newScrollTab;

      final dal = DbHelper.instance;
      final tabFontSize = PStyle.instance.fontSize;
      final tabFontFamily = PStyle.instance.fontFamily;
      final edgeInsets = const EdgeInsets.only(left: 1.0, right: 1.0, top: 4.0);
      final List<Map> lstCacheTab = await dal.getListAllCacheTabForScrollTab();

      //TODO: TAB, use futureBuilder ?
      void _refreshScrollTab() {
        if (P.isDebug) print("refresh scrollTab");
        setState(() {
          scrollTab = newScrollTab;
        });
      }

      void _scrollToBtn() {
        try {
          if (tabKey.currentContext != null) {
            setState(() {
              Scrollable.ensureVisible(tabKey.currentContext!, duration: Duration(milliseconds: 300), curve: Curves.easeInOut);
            });
          }
        } catch(ex, stacktrace) {
          if (P.isDebug) P.printLog(ex, stacktrace);
        }
      }

      void _scrollByOffset(final double? hoffset) {
        try {
          if (hoffset != null) {
            if (scrollTabController != null) scrollTabController?.jumpTo(scrollTabController!.position.pixels + hoffset);
          }
        } catch(ex, stacktrace) {
          if (P.isDebug) P.printLog(ex, stacktrace);
        }
      }

      void _onHorizontalDragUpdate(final DragUpdateDetails details) {
        //Actually trackpad and mouse are inverted.
        //Mac: trackpad goes in the right direction (left, right, top, bottom).
        //Android works inverted then Mac.
        //Linux should work like Android by default.
        _scrollByOffset(details.primaryDelta);
      }

      Widget _createTab(final int index) {
        //TODO: TAB, color: gverseStyle.defaultColor, backgroundColor: gverseStyle.bgColor1
        c = lstCacheTab[index];
        tabId = c["tabId"];
        tabType = c["tabType"];
        tabTitle = c["tabTitle"];
        favFilter = c["favFilter"];
        if (tabType == "F") tabTitle = "${R.fav[favFilter]['bmCurrent']} ${tabTitle}".trim();
        text = Text(
          tabTitle,
          style: (tabId == gtabId) ? TextStyle(fontSize: tabFontSize, fontFamily: tabFontFamily) : TextStyle(fontSize: tabFontSize, fontFamily: tabFontFamily, color: gverseStyle.defaultFgColor),
        );

        final Key btnKey = ValueKey<int>(tabId);
        final GlobalKey containerKey = GlobalKey();
        if (tabId == gtabId) tabKey = containerKey;

        _onPressed() {
          try {
            final String strBtnKey = btnKey.toString().replaceFirst("[<", "").replaceFirst(">]", "");
            final int? tabId = int.tryParse(strBtnKey);
            if (tabId == null || tabId == gtabId) return;
            _onLoadTabSelected(tabId);
          } catch(ex, stacktrace) {
            if (P.isDebug) P.printLog(ex, stacktrace);
          }
        }

        _onLongPress() {
          try {
            final String strBtnKey = btnKey.toString().replaceFirst("[<", "").replaceFirst(">]", "");
            final int? tabId = int.tryParse(strBtnKey);
            if (tabId == null) return;
            _onDeleteTab(tabId);
          } catch(ex, stacktrace) {
            if (P.isDebug) P.printLog(ex, stacktrace);
          }
        }

        return tabId == gtabId
            ? Container(
                key: containerKey,
                padding: edgeInsets,
                child: OutlinedButton(
                  key: btnKey,
                  child: text,
                  style: ButtonStyle(foregroundColor: MaterialStatePropertyAll(Colors.white), backgroundColor: MaterialStatePropertyAll(gverseStyle.accentFgColor)),
                  onPressed: () => _onPressed(),
                  onLongPress: () => _onLongPress(),
                ),
              )
            : Container(
                key: containerKey,
                padding: edgeInsets,
                child: OutlinedButton(
                  key: btnKey,
                  child: text,
                  onPressed: () => _onPressed(),
                  onLongPress: () => _onLongPress(),
                ),
              );
      }

      _init() {
        final List<Widget> lstTab = [];
        for (int index = 0; index < lstCacheTab.length; index++) {
          lstTab.add(_createTab(index));
        }

        if (Platform.isLinux) {
          newScrollTab = SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            controller: scrollTabController,
            padding: EdgeInsets.only(left: 15.0, top: 0.0, right: 15.0, bottom: 0.0),
            child: GestureDetector(
              onHorizontalDragUpdate: (details) => _onHorizontalDragUpdate(details),
              child: Row(
                children: lstTab,
              ),
            ),
          );
        } else {
          newScrollTab = SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            padding: EdgeInsets.only(left: 15.0, top: 0.0, right: 15.0, bottom: 0.0),
            child: Row(
              children: lstTab,
            ),
          );
        }

        _refreshScrollTab();
        if (shouldScrollToBtn) Future.delayed(Duration(milliseconds: 100), () => _scrollToBtn());
      }

      _init();
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  //Don't use gtabId to call it
  void _onLoadTabSelected(final int tabId) async {
    try {
      //Load
      final dal = DbHelper.instance;
      final Map c = await dal.getCacheTabById(tabId);
      if (c.isEmpty) return;
      final String fullQuery = c["fullQuery"];

      await _setAndSaveCurrentTabId(tabId);
      final String tabType = c["tabType"];
      final int orderBy = c["orderBy"] ?? 0;
      final double offset = c["offset"] ?? 0.0;
      switch (tabType) {
        case "S":
          final String bbName = c["bbName"];
          final int bNumber = c["bNumber"];
          final int cNumber = c["cNumber"];
          final int vNumber = c["vNumber"];
          if (vNumber == 0 && bNumber == 0 && cNumber == 0) {
            await _setPageNumberParams();
            _onOpenChapterWidgetSearch("B", orderBy, bbName, fullQuery, 0, false, 0, offset);
          } else if (vNumber > 0 && bNumber > 0 && cNumber > 0) {
            _onOpenChapterWidgetCR(bNumber, cNumber, vNumber, false, offset);
          } else {
            _onOpenChapterWidgetBook(bNumber, cNumber, 0, false, offset);
          }
          break;
        case "F":
          final String bbName = c["bbName"];
          final int favFilter = c["favFilter"];
          await _setPageNumberParams();
          _onOpenChapterWidgetSearch("F", orderBy, bbName, fullQuery, 0, false, favFilter, offset);
          break;
        case "P":
          _onOpenChapterWidgetPrbl(fullQuery, false, offset);
          break;
        case "A":
          _onOpenChapterWidgetArt(fullQuery, false, offset);
          break;
      }
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  void _onDeleteTab(final int tabId) async {
    final dal = DbHelper.instance;
    await dal.delCacheTabById(tabId);
    _onRefreshScrollTab(false);
    //was: await _onShowHistMenu(homePageContext, mnuHistoryValue);
  }

//endregion

//region -- Settings menu --
//** Settings menu **
  void _onShowSettingsMenu(final BuildContext context, final String title) {
    Future<void> _optionDynamicLayoutSelected(final int sizeSelected) async {
      await P.Prefs.saveLayoutDynamic1("$sizeSelected");
      gcolCount = sizeSelected; //was await _getDynamicColumnCount(gbbNames.length > 1 ? gbbNames.length : 1);
      _onRefreshChapterWidget();
    }

    Future<void> _optionFontSizeSelected(final double fontSizeSelected) async {
      gfontSize = fontSizeSelected;
      PStyle.instance.fontSize = gfontSize;
      await P.Prefs.saveFontSize(gfontSize.toString());
      _onRefreshChapterWidget();
    }

    Future<void> _optionFontNameSelected(final String fontNameSelected) async {
      gfontName = fontNameSelected;
      PStyle.instance.fontFamily = gfontName;
      await P.Prefs.saveFontName(gfontName);
      _onRefreshChapterWidget();
    }

    Future<void> _optionStyleHighlightSearchSelected(final String styleSelected, final Map<String, Color>? mapStyle) async {
      if (mapStyle == null) return;

      final Map style = Map();
      style["styleName"] = styleSelected;
      style["fg"] = mapStyle["fg"];
      style["bg"] = mapStyle["bg"];
      gstyleHighlightSearch = style;

      await P.Prefs.saveStyleHighlightSearch(styleSelected);
      _onRefreshChapterWidget();
    }

    void _onOpenFavsPage() async {
      final Future? optionResult = Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ManageFavsPage(titlePage: R.getString(R.id.mnuFavorites), actionStyle: gverseStyle)),
      );
      optionResult?.whenComplete(() => _onRefreshChapterWidget());
    }

    Future<void> _optionMainSelected(final int indexMainSelected, final VerseStyle verseStyle) async {
      try {
        final fontSize = PStyle.instance.fontSize;
        if (indexMainSelected == 0) {
          _onChangeTheme();
        } else if (indexMainSelected == 1) {
          _onOpenFavsPage();
        } else if (indexMainSelected == 2) {
          late final String fontNameSelected;
          final List<String> lstFontName = R.getListAllFontNames();
          final List<TextSpan> lstTextSpanFontName = P.getListTextSpanWithFontNames(lstFontName, gverseStyle);
          final Future optionResult = Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => OptionPage(
                titlePage: mnuSettingsFontsValue,
                lstItem: lstTextSpanFontName,
                isAlignmentStart: true,
                topBarMenuType: TopBarMenuType.NONE,
                lstItemCheckedOrig: [],
                actionStyle: null,
                layoutType: 1,
                columns: 1,
              ),
            ),
          );
          optionResult.then((value) => fontNameSelected = (value == null) ? '' : lstFontName[value]);
          optionResult.whenComplete((){ if (fontNameSelected.isNotEmpty)  _optionFontNameSelected(fontNameSelected); });
        } else if (indexMainSelected == 3) {
          late final double fontSizeSelected;
          final List<int> lstFontSize = R.getListAllFontSizes();
          final List<TextSpan> lstTextSpanFontSize = P.getListTextSpanWithFontSizes(lstFontSize, gverseStyle);
          final Future optionResult = Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => OptionPage(
                titlePage: mnuSettingsFontSizeValue,
                lstItem: lstTextSpanFontSize,
                isAlignmentStart: false,
                topBarMenuType: TopBarMenuType.NONE,
                lstItemCheckedOrig: [],
                actionStyle: null,
                layoutType: 1,
                columns: 1,
              ),
            ),
          );
          optionResult.then((value) => fontSizeSelected = (value == null) ? -1.0 : lstFontSize[value].toDouble());
          optionResult.whenComplete((){ if (fontSizeSelected >= 0.0) _optionFontSizeSelected(fontSizeSelected); });
        } else if (indexMainSelected == 4) {
          final BibleSearchStyleBO bibleStyle = BibleSearchStyleBO(context);
          final List<String> lstStyle = bibleStyle.getAllStyleNames();
          late final String styleSelected;
          final int size = lstStyle.length;
          final double fontSizeQuickFox = fontSize + 4.0;
          final List<TextSpan> lstStyled = List.generate(
              size,
                  (index) =>
                  TextSpan(
                    text: "\n",
                    style: TextStyle(color: verseStyle.defaultFgColor, fontSize: fontSize),
                    children: <TextSpan>[
                      TextSpan(text: lstStyle[index]),
                      TextSpan(
                          text: "\n The quick fox.",
                          style: TextStyle(
                              color: bibleStyle.getStylePropertiesFromId(lstStyle[index])?["fg"],
                              backgroundColor: bibleStyle.getStylePropertiesFromId(lstStyle[index])?["bg"],
                              fontSize: fontSizeQuickFox)),
                    ],
                  ));
          final Future optionResult = Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  OptionPage(
                    titlePage: R.getString(R.id.mnuSettingsStyleHighlightSearch),
                    lstItem: lstStyled,
                    isAlignmentStart: true,
                    topBarMenuType: TopBarMenuType.NONE,
                    lstItemCheckedOrig: [],
                    actionStyle: null,
                    layoutType: 1,
                    columns: 1,
                  ),
            ),
          );
          optionResult.then((value) => styleSelected = (value == null) ? '' : lstStyle[value]);
          optionResult.whenComplete((){ if (styleSelected.isNotEmpty) _optionStyleHighlightSearchSelected(styleSelected, bibleStyle.getStylePropertiesFromId(styleSelected)); });
        } else if (indexMainSelected == 5) {
          _onShowCustomBibleMenu(context, R.getString(R.id.mnuSettingsCustomBibles));
        } else if (indexMainSelected >= 6) {
          final List<int> lstSize = List<int>.generate(10,  (int genIndex) => genIndex + 1);

          final fontFamily = PStyle.instance.fontFamilyForMenu();
          final textStyleMenu = TextStyle(height: 2.0, fontFamily: fontFamily, fontSize: fontSize, color: verseStyle.defaultFgColor);
          late int sizeSelected;
          final int size = lstSize.length;
          final List<TextSpan> lstDynamicLayoutSize = List.generate(
              size,
              (index) => TextSpan(
                    text: "${lstSize[index]}",
                    style: textStyleMenu,
                  ));
          final Future optionResult = Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => OptionPage(
                titlePage: R.getString(R.id.mnuSettingsLayoutDynamicSub),
                lstItem: lstDynamicLayoutSize,
                isAlignmentStart: false,
                topBarMenuType: TopBarMenuType.NONE,
                lstItemCheckedOrig: [],
                actionStyle: null,
                layoutType: 1,
                columns: 1,
              ),
            ),
          );
          optionResult.then((value) => sizeSelected = (value == null) ? -1 : lstSize[value]);
          optionResult.whenComplete((){ if (sizeSelected >= 0) _optionDynamicLayoutSelected(sizeSelected); }); //Start at layout 1
        }
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    Future<void> _show(final List<TextSpan> lstSetting, final VerseStyle verseStyle) async {
      try {
        late int indexSelected;
        final Future optionResult = Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => OptionPage(
              titlePage: title,
              lstItem: lstSetting,
              isAlignmentStart: true,
              topBarMenuType: TopBarMenuType.NONE,
              lstItemCheckedOrig: [],
              actionStyle: null,
              layoutType: 1,
              columns: 1,
            ),
          ),
        );
        optionResult.then((value) => indexSelected = value ?? -1);
        optionResult.whenComplete(() => _optionMainSelected(indexSelected, verseStyle));
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    void _prepareData(final String themeName, final List<Map> lstSetting) {
      try {
        final VerseStyle verseStyle = themeName.compareTo('LIGHT') == 0 ? LightVerseStyle() : DarkVerseStyle();
        final fontSize = PStyle.instance.fontSizeForMenu();
        final fontSizeSub = fontSize - 4.0;
        final fontFamily = PStyle.instance.fontFamilyForMenu();
        final TextStyle rowStyle = TextStyle(height: 2.0, fontFamily: fontFamily, fontSize: fontSize, color: verseStyle.defaultFgColor);
        final TextStyle headerStyle = TextStyle(height: 3.0, fontFamily: fontFamily, fontSize: fontSize, color: verseStyle.accentFgColor);
        final List<TextSpan> newLstSetting = [];
        lstSetting.forEach((row) {
          //Put logic here for several styles
          final String text = row['text'];
          final String subText = row['subText'];
          final String value = row['value'];
          final String header = row['header'];
          final TextSpan rowStyled = (value.isEmpty)
              ? TextSpan(
                  text: "$text",
                  style: rowStyle,
                )
              : TextSpan(
                  text: "$text\n",
                  style: rowStyle,
                  children:
                  [
                    (!subText.isEmpty)
                        ? TextSpan(text: "$subText\n", style: TextStyle(fontSize: fontSizeSub))
                        : TextSpan(text: ""),
                          TextSpan(text: "($value)", style: TextStyle(color: verseStyle.accentFgColor, fontSize: fontSizeSub)),
                    //TextSpan(text: "\n", style: TextStyle(height: 1.5)),
                  ],
                );
          final TextSpan rowStyledWithHeader = !header.isEmpty
              ? TextSpan(
                  text: "$header\n",
                  style: headerStyle,
                  children: [rowStyled],
                )
              : rowStyled;
          newLstSetting.add(rowStyledWithHeader);
        });

        _show(newLstSetting, verseStyle);
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    Future<List<Map>> _getData(final String themeName) async {
      final List<Map> lstSetting = [];
      lstSetting.add({
        "text": R.getString(R.id.mnuThemes),
        "subText": "",
        "value": "",
        "header": "",
      });
      lstSetting.add({
        "text": R.getString(R.id.mnuFavorites),
        "subText": "",
        "value": "",
        "header": "",
      });
      lstSetting.add({
        "text": R.getString(R.id.mnuSettingsFonts),
        "subText": "",
        "value": gfontName,
        "header": "",
      });
      lstSetting.add({
        "text": R.getString(R.id.mnuSettingsFontSize),
        "subText": "",
        "value": gfontSize.toString(),
        "header": "",
      });
      lstSetting.add({
        "text": R.getString(R.id.mnuSettingsStyleHighlightSearch),
        "subText": "",
        "value": gstyleHighlightSearch["styleName"],
        "header": "",
      });
      lstSetting.add({
        "text": R.getString(R.id.mnuSettingsCustomBibles),
        "subText": "",
        "value": "",
        "header": "",
      });
      lstSetting.add({
        "text": R.getString(R.id.mnuSettingsLayoutDynamicSub),
        "subText": "",
        "value": await P.Prefs.getLayoutDynamic1,
        "header": R.getString(R.id.mnuSettingsLayoutDynamicHeader),
      });

      _prepareData(themeName, lstSetting);

      return lstSetting;
    }

    void _init() {
      _getData(gThemeName);
    }

    _init();
  }

//** End of Settings menu **
//endregion

//region -- About menu --
//** About menu **
  void _onShowAboutMenu(final BuildContext context, final String title) async {
    try {
      final dal = DbHelper.instance;
      final int dbVersion = dal.getDbVersion();
      final Map versionInfo = P.getVersion();
      final String versionName = versionInfo['versionNumber']; //ex: number like 1.0.0
      final String versionCode = versionInfo['versionDate']; //ex: date like 20200206
      final String versionAbout = "$versionName (${dbVersion})";
      final String versionPlatform = ' iPhone, iPad, Big Sur, Mac, Linux, XAXXndXrXoXXiXd'.replaceAll("X", "");
      final String policy = Platform.isLinux ? "https://hotlittlewhitedog.gitlab.io/biblemulti/blog/policy-thelife-linux" : "https://hotlittlewhitedog.gitlab.io/biblemulti/blog/policy-thelife";

      await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => AboutPage(
                  titlePage: title,
                  dbVersion: dbVersion,
                  versionName: versionName,
                  versionCode: versionCode,
                  versionAbout: versionAbout,
                  versionPlatform: versionPlatform,
                  policy: policy,
                )),
      );
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

//** End of About menu **
//endregion

//region -- Help menu --
//** Help menu **
  void _onShowHelpMenu(final BuildContext context, final String title) async {
    try {
      await Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HelpPage(titlePage: title)),
      );
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

//** End of Help menu **
//endregion

//region -- Invite menu --
//** Invite Friend menu **
  void _onShowInviteFriendMenu(final BuildContext context, final String title) async {
    try {
      const String appIosUrls =
          "* iPhone/iPad/Big Sur:\nhttps://apps.apple.com/us/app/bible-multi-the-life/id1502832136 \n\n* Mac:\nhttps://apps.apple.com/us/app/bible-multi-the-life/id1587163844 \n\n";
      final String appOtherUrls = (P.getVersion())["appOtherUrls"];
      final textForClipboard = "${R.getString(R.id.inviteFriendClipboardMsg1)}$appIosUrls$appOtherUrls${R.getString(R.id.inviteFriendClipboardMsg2)}";
      await P.copyTextToClipboard(context, textForClipboard, R.getString(R.id.copiedClipboard), true);
      await Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => InviteFriendPage(titlePage: title)),
      );
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  //** End of Invite Friend menu **
  //endregion

//region -- Bibles menu --
//** Bibles menu **
  Future<void> _onShowBiblesMenu(final BuildContext context, final String title, final SelectionType selectionType) async {
    final VerseStyle actionStyle = gverseStyle; //await PStyle.instance.getVerseStyle();
    List<Map> lstSetting = [];

    void _optionRefresh() async {
      await _onRefreshResources();
      await P.Prefs.saveBibleLocale(gbbLocale);
      await P.Prefs.saveBibleAltLocale(gbbAltLocale);
      await P.Prefs.saveBibleName(gbbNames);

      final bool isLightThemeAsDefault = gverseStyle is LightVerseStyle;
      await P.refreshCustomVerseStylesDisplayed(isLightThemeAsDefault);

      _onRefreshChapterWidget();
      if (Navigator.of(context).canPop()) Navigator.of(context).pop();
    }

    Future<void> _optionSelectedMulti(final List<int> lstIndexSelected) async {
      try {
        //Generate gbbNames
        String tbbName = "";
        lstIndexSelected.forEach((element) {
          if (element >= 0 && element <= 20) {
            final String bbName = lstSetting[element]['bbName'];
            tbbName = "$tbbName$bbName";
          }
        });
        gbbNames = tbbName;
        gcolCount = await _getDynamicColumnCount();
        _optionRefresh();
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    Future<void> _optionSelectedSingle(final int indexSelected) async {
      try {
        final dal = DbHelper.instance;
        final String order = dal.getAllBBNamesOfDb();
        gbbName0 = (indexSelected >= 0 && indexSelected <= 20) ? order[indexSelected] : "k";
        final String tbbName = gbbNames.replaceAll(gbbName0, "");
        gbbNames = "$gbbName0$tbbName";
        gbbLocale = (indexSelected >= 0 && indexSelected <= 20) ? P.getLocaleName(indexSelected) : "EN";
        gbbAltLocale = (gbbLocale != "DE" && gbbLocale != "IN" && gbbLocale != "AR" && gbbLocale != "CN" && gbbLocale != "JP" && gbbLocale != "RU" && gbbLocale != "TR" && gbbLocale != "BD" && gbbLocale != "TZ" && gbbLocale != "de" && gbbLocale != "RO" && gbbLocale != "PL") ? gbbLocale : "EN";
        gcolCount = await _getDynamicColumnCount();
        _optionRefresh();
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    Future<void> _optionSelectedAlt(final int indexSelected) async {
      try {
        final List<String> lstAltLocale = [
          "EN",
          "ES",
          "FR",
          "IT",
          "PT",
        ];
        gbbAltLocale = (indexSelected >= 0 && indexSelected <= 4) ? lstAltLocale[indexSelected] : "EN";
        _optionRefresh();
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    List<TextSpan> _prepareData(final String themeName) {
      final List<TextSpan> newLstSetting = [];
      try {
        final fontSize = PStyle.instance.fontSizeForMenu();
        final fontFamily = PStyle.instance.fontFamilyForMenu();
        lstSetting.forEach((row) {
          //Put logic here for several styles
          final String text = row['text'];

          final TextSpan rowStyled = TextSpan(
            text: text,
            style: TextStyle(height: 2.0, fontFamily: fontFamily, fontSize: fontSize, color: actionStyle.defaultFgColor),
          );
          newLstSetting.add(rowStyled);
        });
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
      return newLstSetting;
    }

    Future<void> _show(final String themeName) async {
      try {
        final String tbbName = gbbNames;
        if (selectionType == SelectionType.MULTI) {
          lstSetting[0]['order'] = gbbName0 == "k"
              ? -1
              : (tbbName.contains("k"))
                  ? tbbName.indexOf("k")
                  : 100;
          lstSetting[1]['order'] = gbbName0 == "2"
              ? -1
              : (tbbName.contains("2"))
                  ? tbbName.indexOf("2")
                  : 100;
          lstSetting[2]['order'] = gbbName0 == "v"
              ? -1
              : (tbbName.contains("v"))
                  ? tbbName.indexOf("v")
                  : 100;
          lstSetting[3]['order'] = gbbName0 == "9"
              ? -1
              : (tbbName.contains("9"))
              ? tbbName.indexOf("9")
              : 100;
          lstSetting[4]['order'] = gbbName0 == "l"
              ? -1
              : (tbbName.contains("l"))
                  ? tbbName.indexOf("l")
                  : 100;
          lstSetting[5]['order'] = gbbName0 == "o"
              ? -1
              : (tbbName.contains("o"))
                  ? tbbName.indexOf("o")
                  : 100;
          lstSetting[6]['order'] = gbbName0 == "d"
              ? -1
              : (tbbName.contains("d"))
                  ? tbbName.indexOf("d")
                  : 100;
          lstSetting[7]['order'] = gbbName0 == "1"
              ? -1
              : (tbbName.contains("1"))
              ? tbbName.indexOf("1")
              : 100;
          lstSetting[8]['order'] = gbbName0 == "a"
              ? -1
              : (tbbName.contains("a"))
                  ? tbbName.indexOf("a")
                  : 100;
          lstSetting[9]['order'] = gbbName0 == "s"
              ? -1
              : (tbbName.contains("s"))
                  ? tbbName.indexOf("s")
                  : 100;
          lstSetting[10]['order'] = gbbName0 == "e"
              ? -1
              : (tbbName.contains("e"))
              ? tbbName.indexOf("e")
              : 100;
          lstSetting[11]['order'] = gbbName0 == "i"
              ? -1
              : (tbbName.contains("i"))
              ? tbbName.indexOf("i")
              : 100;
          lstSetting[12]['order'] = gbbName0 == "y"
              ? -1
              : (tbbName.contains("y"))
              ? tbbName.indexOf("y")
              : 100;
          lstSetting[13]['order'] = gbbName0 == "c"
              ? -1
              : (tbbName.contains("c"))
              ? tbbName.indexOf("c")
              : 100;
          lstSetting[14]['order'] = gbbName0 == "j"
              ? -1
              : (tbbName.contains("j"))
              ? tbbName.indexOf("j")
              : 100;
          lstSetting[15]['order'] = gbbName0 == "r"
              ? -1
              : (tbbName.contains("r"))
              ? tbbName.indexOf("r")
              : 100;
          lstSetting[16]['order'] = gbbName0 == "t"
              ? -1
              : (tbbName.contains("t"))
              ? tbbName.indexOf("t")
              : 100;
          lstSetting[17]['order'] = gbbName0 == "b"
              ? -1
              : (tbbName.contains("b"))
              ? tbbName.indexOf("b")
              : 100;
          lstSetting[18]['order'] = gbbName0 == "h"
              ? -1
              : (tbbName.contains("h"))
              ? tbbName.indexOf("h")
              : 100;
          lstSetting[19]['order'] = gbbName0 == "u"
              ? -1
              : (tbbName.contains("u"))
              ? tbbName.indexOf("u")
              : 100;
          lstSetting[20]['order'] = gbbName0 == "z"
              ? -1
              : (tbbName.contains("z"))
              ? tbbName.indexOf("z")
              : 100;
          lstSetting.sort((a, b) => a['order'].compareTo(b['order']));
        }

        final List<int> lstItemCheckedOrigin = [];
        lstSetting.forEach((element) {
          lstItemCheckedOrigin.add(element['order']);
        });
        final List<TextSpan> newLstSetting = _prepareData(themeName);

        late int indexSelected;
        late List<int> lstIndexSelected;
        final TopBarMenuType topBarMenuType = (selectionType == SelectionType.MULTI) ? TopBarMenuType.MULTI : TopBarMenuType.NONE;
        switch (selectionType) {
          case SelectionType.MULTI:
            final Future optionResult = Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => OptionPage(
                  titlePage: title,
                  lstItem: newLstSetting,
                  isAlignmentStart: false,
                  topBarMenuType: topBarMenuType,
                  lstItemCheckedOrig: lstItemCheckedOrigin,
                  actionStyle: actionStyle,
                  layoutType: 1,
                  columns: 1,
                ),
              ),
            );
            optionResult.then((value) => lstIndexSelected = (value == null) ? [] : value);
            optionResult.whenComplete((){ if (lstIndexSelected.isNotEmpty) _optionSelectedMulti(lstIndexSelected); });
            break;

          case SelectionType.SINGLE:
            final Future optionResult = Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => OptionPage(
                  titlePage: title,
                  lstItem: newLstSetting,
                  isAlignmentStart: false,
                  topBarMenuType: topBarMenuType,
                  lstItemCheckedOrig: lstItemCheckedOrigin,
                  actionStyle: actionStyle,
                  layoutType: 1,
                  columns: 1,
                ),
              ),
            );
            optionResult.then((value) => indexSelected = value ?? -1);
            optionResult.whenComplete((){ if (indexSelected >= 0) _optionSelectedSingle(indexSelected); });
            break;

          case SelectionType.ALT:
            final Future optionResult = Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => OptionPage(
                  titlePage: title,
                  lstItem: newLstSetting,
                  isAlignmentStart: false,
                  topBarMenuType: topBarMenuType,
                  lstItemCheckedOrig: lstItemCheckedOrigin,
                  actionStyle: actionStyle,
                  layoutType: 1,
                  columns: 1,
                ),
              ),
            );
            optionResult.then((value) => indexSelected = value);
            optionResult.whenComplete(() => _optionSelectedAlt(indexSelected));
            break;
        }
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    void _getData(final String themeName) {
      if (selectionType == SelectionType.ALT) {
        //Definite order
        lstSetting = [
          {'text': "English", 'order': 1},
          {'text': "Española", 'order': 1},
          {'text': "Français", 'order': 1},
          {'text': "Italiano", 'order': 1},
          {'text': "Português", 'order': 1}
        ];
      } else {
        //Undefinite order
        lstSetting = R.getListAllBibleSettings();
      }
      _show(themeName);
    }

    void _init() {
      _getData(gThemeName);
    }

    _init();
  }

//** End of Bibles menu **
//endregion

//region -- History menu --
//** History menu **
  Future<void> _onShowHistMenu(final BuildContext context, final String title) async {
    final dal = DbHelper.instance;
    final String bbName0 = gbbName0;
    List<Map> lstHist = [];
    List<int> lstId = [];

    Future<void> _optionHistSelected(final int indexSelected) async {
      try {
        if (indexSelected == -1) {
          //Reopen
          _onRefreshScrollTab(true);
          await _onShowHistMenu(context, title);
          return;
        }

        if (Navigator.of(context).canPop()) Navigator.of(context).pop();

        final int tabId = lstId[indexSelected];

        //Load
        final Map c = await dal.getCacheTabById(tabId);
        if (c.isEmpty) return;
        final String fullQuery = c["fullQuery"];

        await _setAndSaveCurrentTabId(tabId);

        final String tabType = c["tabType"];
        final int orderBy = c["orderBy"] ?? 0;
        final double offset = c["offset"] ?? 0.0;
        switch (tabType) {
          case "S":
            final String bbName = c["bbName"];
            final int bNumber = c["bNumber"];
            final int cNumber = c["cNumber"];
            final int vNumber = c["vNumber"];
            if (vNumber == 0 && bNumber == 0 && cNumber == 0) {
              await _setPageNumberParams();
              _onOpenChapterWidgetSearch("B", orderBy, bbName, fullQuery, 0, false, 0, offset);
            } else if (vNumber > 0 && bNumber > 0 && cNumber > 0) {
              _onOpenChapterWidgetCR(bNumber, cNumber, vNumber, false, offset);
            } else {
              _onOpenChapterWidgetBook(bNumber, cNumber, 0, false, offset);
            }
            break;
          case "F":
            final String bbName = c["bbName"];
            final int favFilter = c["favFilter"];
            await _setPageNumberParams();
            _onOpenChapterWidgetSearch("F", orderBy, bbName, fullQuery, 0, false, favFilter, offset);
            break;
          case "P":
            _onOpenChapterWidgetPrbl(fullQuery, false, offset);
            break;
          case "A":
            _onOpenChapterWidgetArt(fullQuery, false, offset);
            break;
        }
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    Future<void> _show(final List<TextSpan> lstHist, final VerseStyle verseStyle) async {
      try {
        late int indexSelected;
        final Future histResult = Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => HistoryPage(titlePage: title, lstItem: lstHist, lstId: lstId, isAlignmentStart: true, topBarMenuType: TopBarMenuType.NONE, actionStyle: verseStyle),
          ),
        );
        histResult.then((value) => indexSelected = value ?? -1);
        histResult.whenComplete((){ if (indexSelected >= 0)  _optionHistSelected(indexSelected); });
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    void _prepareData(final String themeName) {
      try {
        final VerseStyle verseStyle = themeName.compareTo('LIGHT') == 0 ? LightVerseStyle() : DarkVerseStyle();

        final fontSize = PStyle.instance.fontSizeForMenu();
        final fontFamily = PStyle.instance.fontFamilyForMenu();
        final List<TextSpan> newLstHist = [];
        lstHist.forEach((row) {
          //Put logic here for several styles
          final String text = row['desc'];
          final int style = row['style'];

          final TextSpan rowStyled = TextSpan(
            text: text,
            style: TextStyle(height: 2.0, fontFamily: fontFamily, fontSize: fontSize, color: style == 0 ? verseStyle.defaultFgColor : verseStyle.accentFgColor),
          );

          newLstHist.add(rowStyled);
        });

        _show(newLstHist, verseStyle);
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    void _getData(final String themeName) async {
      lstHist = [];
      lstId = [];
      Prbl? prbl;
      Art? art;

      final String crShort = R.getString(R.id.crShort);
      final List<Map> lstCacheTab = await dal.getListAllCacheTabForHistory();
      for (var row in lstCacheTab) {
        final int tabId = row["tabId"];
        final String tabTitle = row["tabTitle"];
        final String tabType = row["tabType"];
        final String fullQuery = row["fullQuery"];
        final int bNumber = row["bNumber"];
        final int cNumber = row["cNumber"];
        final int vNumber = row["vNumber"];
        final String bbName = bbName0; //row["bbName"];
        final int favFilter = row["favFilter"];

        lstId.add(tabId);
        String histTitleVerbose = tabTitle;
        switch (tabType) {
          case "S":
            if (vNumber > 0 && bNumber > 0 && cNumber > 0) {
              final Map bookRef = await dal.getBookRef(bbName, bNumber);
              if (bookRef.isNotEmpty) {
                final String bName = bookRef["bName"];
                histTitleVerbose = "$crShort: $bName $cNumber.$vNumber";
              }
              break;
            }

            final List<String> lstSearch = fullQuery.split(" ");
            if (lstSearch.length == 2) {
              final int? trybNumber = int.tryParse(lstSearch[0]);
              if (trybNumber == null) break;
              final int? trycNumber = int.tryParse(lstSearch[1]);
              if (trycNumber == null) break;
              final Map bookRef = await dal.getBookRef(bbName, trybNumber);
              if (bookRef.isNotEmpty) {
                final String bName = bookRef["bName"];
                histTitleVerbose = "$bName $trycNumber";
              }
            }
            break;
          case "F":
            final String favEmo = R.fav[favFilter]['bmCurrent'];
            final String title = tabTitle == "" ? R.getString(R.id.favAll) : tabTitle;
            histTitleVerbose = "${favEmo} ${title}";
            break;
          case "P":
            if (prbl == null) prbl = Prbl();
            final String prblDesc = prbl.getPrblDesc(fullQuery.replaceFirst("PRBL", ""));
            histTitleVerbose = prblDesc; //was: if (prblDesc != null)
            break;
          case "A":
            if (art == null) art = Art();
            final String artDesc = await art.getArtDesc(fullQuery.replaceFirst("ART", ""));
            if (artDesc.isNotEmpty) histTitleVerbose = artDesc;
            break;
          default:
            histTitleVerbose = tabTitle;
            break;
        }

        lstHist.add({'desc': histTitleVerbose, 'style': gtabId == row["tabId"] ? 1 : 0});
      }

      _prepareData(themeName);
    }

    void _init() {
      _getData(gThemeName);
    }

    _init();
  }

//** End of History menu **
//endregion

//region -- Common to Books/Arts/Prbls/Search/CR --

  Future<void> _onShowContextMenu(final int bibleId) async {
    try {
      final BuildContext cmContext = this.context;
      final dal = DbHelper.instance;
      final Color fgColor = gverseStyle.defaultFgColor;
      final textStyleMenu = TextStyle(fontFamily: PStyle.instance.fontFamilyForMenu(), color: fgColor, fontSize: 18.0); //fontFamily: "Droid-sans-mono.regular",

      final Map currentVerse = await dal.getVerse(bibleId);
      if (currentVerse.isEmpty) return;
      final String bbName = currentVerse["bbName"];
      final int bNumber = currentVerse["bNumber"];
      final int cNumber = currentVerse["cNumber"];
      final int vNumber = currentVerse["vNumber"];
      final int crTot = currentVerse["tot"] ?? 0;
      final double offset = 0.0;

      Future<void> _openChapter() async {
        try {
          if (Navigator.canPop(cmContext)) Navigator.pop(cmContext);
          if (Navigator.canPop(cmContext)) Navigator.pop(cmContext);
          _onOpenChapterWidgetBook(bNumber, cNumber, 0, false, offset);
        } catch (ex, stacktrace) {
          if (P.isDebug) P.printLog(ex, stacktrace);
        }
      }

      Future<void> _openCR() async {
        try {
          if (Navigator.canPop(cmContext)) Navigator.pop(cmContext);
          if (Navigator.canPop(cmContext)) Navigator.pop(cmContext);
          _onOpenChapterWidgetCR(bNumber, cNumber, vNumber, false, offset);
        } catch (ex, stacktrace) {
          if (P.isDebug) P.printLog(ex, stacktrace);
        }
      }

      Future<void> _clearClipboard(final bool shouldShowToast) async {
        try {
          await P.Prefs.saveClipboardIds([]);
          await P.copyTextToClipboard(cmContext, "", R.getString(R.id.emptyClipboard), shouldShowToast);
        } catch (ex, stacktrace) {
          if (P.isDebug) P.printLog(ex, stacktrace);
        }
      }

      ///Returns text generated
      Future<String> _generateTextForClipboard(final bool shouldShowToast) async {
        String textToClipboard = "";
        int warnType = 0;

        try {
          List<String> lstIdGen = await P.Prefs.getClipboardIds;
          int size = lstIdGen.length;
          if (size <= 0) {
            await _clearClipboard(false);
            return "";
          }

          Map currentVerse, nextVerse;
          int idIntCurrent, idIntNext, bNumberCurrent, bNumberNext, cNumberCurrent, cNumberNext, vNumberCurrent;
          String dir,
              vTextCurrent,
              bNameCurrent,
              bbNameCurrent,
              bbNameNext,
              bbNames = "";

          //Get bbNames
          for (int index = 0; index < size; index++) {
            idIntCurrent = int.parse(lstIdGen[index]);
            currentVerse = await dal.getVerse(idIntCurrent);
            bbNameCurrent = (currentVerse.isEmpty) ? '' : currentVerse["bbName"];
            if (bbNameCurrent.isNotEmpty && !bbNames.contains(bbNameCurrent)) bbNames = "$bbNames$bbNameCurrent";
          }

          //Check KJV 2000...
          final String bbNamesBeforeCheck = bbNames;
          for (int bib = 0; bib < bbNamesBeforeCheck.length; bib++) {
            final String bbNameCurrentBeforeCheck = bbNames.substring(bib, bib + 1);
            if (bbNameCurrentBeforeCheck == "2" || bbNameCurrentBeforeCheck == "9" || bbNameCurrentBeforeCheck == "1" || bbNameCurrentBeforeCheck == "i" || bbNameCurrentBeforeCheck == "j" ||
                bbNameCurrentBeforeCheck == "c" || bbNameCurrentBeforeCheck == "t" || bbNameCurrentBeforeCheck == "b" || bbNameCurrentBeforeCheck == "h" || bbNameCurrentBeforeCheck == "u" ||
                bbNameCurrentBeforeCheck == "z") {
              final Map<int, int> mapBookCount = Map();
              for (int bNumber = 1; bNumber <= 66; bNumber++)
                mapBookCount[bNumber] = 0;

              //Counts
              int content;
              int vCountOther = 0;
              for (int i = 0; i < size; i++) {
                idIntCurrent = int.parse(lstIdGen[i]);
                currentVerse = await dal.getVerse(idIntCurrent);
                if (currentVerse.isEmpty) continue;
                bNumberCurrent = currentVerse["bNumber"];
                bbNameCurrent = currentVerse["bbName"];
                if (bbNameCurrent == bbNameCurrentBeforeCheck) {
                  if (!mapBookCount.containsKey(bNumberCurrent)) {
                    vCountOther++;
                  } else {
                    content = mapBookCount[bNumberCurrent]!;
                    mapBookCount[bNumberCurrent] = content + 1;
                  }
                }
              }

              //Check all book count and total
              warnType = 0;
              int sumBooks = vCountOther;
              Map ci;
              for (int bNumber in mapBookCount.keys) {
                if (!mapBookCount.containsKey(bNumber)) continue;

                content = mapBookCount[bNumber]!;
                ci = await dal.getBibleCiByBook(bbName, bNumber);
                if (ci.isNotEmpty && content >= ci["vCount"]) {
                  warnType = 1;
                  break;
                }

                sumBooks += content;
              }
              if (warnType <= 0) if (sumBooks > 500) warnType = 2;

              //Cleaning
              mapBookCount.clear();

              //Remove incorrect
              if (warnType > 0) {
                bbNames = bbNames.replaceAll(bbNameCurrentBeforeCheck, "");
                P.showToast(cmContext, warnType == 1 ? R.getString(R.id.toastWarnKJV2000LimitFullBook) : R.getString(R.id.toastWarnKJV2000Limit500), Toast.LENGTH_LONG);
                for (int i = size - 1; i >= 0; i--) {
                  idIntCurrent = int.parse(lstIdGen[i]);
                  currentVerse = await dal.getVerse(idIntCurrent);
                  if (currentVerse.isEmpty) continue;
                  bbNameCurrent = currentVerse["bbName"];
                  if (bbNameCurrent == bbNameCurrentBeforeCheck) {
                    lstIdGen.removeAt(i);
                  }
                }

                //Reset
                size = lstIdGen.length;
                if (size <= 0) {
                  await _clearClipboard(false);
                  return "";
                }
                await P.Prefs.saveClipboardIds(lstIdGen);
              }
            }
          }

          //Gen
          int prevId = -1;
          final StringBuffer sb = StringBuffer("");
          for (int index = 0; index < size; index++) {
            //Current
            idIntCurrent = int.parse(lstIdGen[index]);
            currentVerse = await dal.getVerse(idIntCurrent);
            if (currentVerse.isEmpty) continue;
            bNumberCurrent = currentVerse["bNumber"];
            cNumberCurrent = currentVerse["cNumber"];
            vNumberCurrent = currentVerse["vNumber"];
            vTextCurrent = currentVerse["vText"];
            bNameCurrent = currentVerse["bName"];
            bbNameCurrent = currentVerse["bbName"];
            dir = bbNameCurrent == "y" ? P.getRTL() : P.getLTR();

            if (prevId == -1) {
              sb.writeln("\n\n$dir$bNameCurrent $cNumberCurrent");
            } else if (prevId + 1 != idIntCurrent) {
              sb.writeln();
            }
            sb.writeln("$dir$vNumberCurrent: $vTextCurrent");
            prevId = idIntCurrent;

            //Next
            if ((index + 1) < size) {
              idIntNext = int.parse(lstIdGen[index + 1]);
              nextVerse = await dal.getVerse(idIntNext);
              if (nextVerse.isEmpty) continue;
              bNumberNext = nextVerse["bNumber"];
              cNumberNext = nextVerse["cNumber"];
              bbNameNext = nextVerse["bbName"];

              if ((bNumberCurrent != bNumberNext) || (cNumberCurrent != cNumberNext) || (bbNameCurrent != bbNameNext)) {
                prevId = -1;
              }
            }
          }

          //Sources
          String bbNamesVerbose = "";
          for (int i = 0; i < bbNames.length; i++) {
            final String bbName = bbNames.substring(i, i + 1);
            if (bbName == "k") bbNamesVerbose = "$bbNamesVerbose, KJV1611";
            if (bbName == "l") bbNamesVerbose = "$bbNamesVerbose, Louis Segond";
            if (bbName == "o") bbNamesVerbose = "$bbNamesVerbose, Ostervald";
            if (bbName == "v") bbNamesVerbose = "$bbNamesVerbose, Reina Valera 1909";
            if (bbName == "a") bbNamesVerbose = "$bbNamesVerbose, Almeida";
            if (bbName == "d") bbNamesVerbose = "$bbNamesVerbose, DIO1649";
            if (bbName == "s") bbNamesVerbose = "$bbNamesVerbose, SCH1951";
            if (bbName == "2") bbNamesVerbose = "$bbNamesVerbose, KJ2K";
            if (bbName == "9") bbNamesVerbose = "$bbNamesVerbose, RVA1989";
            if (bbName == "1") bbNamesVerbose = "$bbNamesVerbose, LND1991";
            if (bbName == "i") bbNamesVerbose = "$bbNamesVerbose, IRV2017";
            if (bbName == "y") bbNamesVerbose = "$bbNamesVerbose, SVDA";
            if (bbName == "c") bbNamesVerbose = "$bbNamesVerbose, CUVS";
            if (bbName == "j") bbNamesVerbose = "$bbNamesVerbose, NJB1973";
            if (bbName == "r") bbNamesVerbose = "$bbNamesVerbose, RST1876";
            if (bbName == "t") bbNamesVerbose = "$bbNamesVerbose, NTB2001";
            if (bbName == "b") bbNamesVerbose = "$bbNamesVerbose, BCL2016";
            if (bbName == "h") bbNamesVerbose = "$bbNamesVerbose, SUV1997";
            if (bbName == "u") bbNamesVerbose = "$bbNamesVerbose, RUC1928";
            if (bbName == "z") bbNamesVerbose = "$bbNamesVerbose, BW1975";
            if (bbName == "e") bbNamesVerbose = "$bbNamesVerbose, ELB1932";
          }
          if (bbNamesVerbose.length > 0) bbNamesVerbose = bbNamesVerbose.substring(2);
          sb.write("\n(${P.getLTR()}$bbNamesVerbose)");

          //Finally
          textToClipboard = sb.toString().trim();
          await P.copyTextToClipboard(cmContext, textToClipboard, R.getString(R.id.copiedClipboard), warnType > 0 ? false : shouldShowToast);
        } catch (ex, stacktrace) {
          if (P.isDebug) P.printLog(ex, stacktrace);
        }
        return textToClipboard;
      }

      Future<void> _addToClipboard(final ClipboardAddType addType) async {
        try {
          if (Navigator.canPop(cmContext)) Navigator.pop(cmContext);
          if (Navigator.canPop(cmContext)) Navigator.pop(cmContext);

          //Manage IDs
          List<String> lstIdGen = await P.Prefs.getClipboardIds;
          final List<Map> lstMapVerse =
              addType == ClipboardAddType.CHAPTER ? await dal.getListVersesId(bbName, bNumber, cNumber, 1, 0) : await dal.getListVersesId(bbName, bNumber, cNumber, vNumber, vNumber);
          for (Map mapVerse in lstMapVerse) {
            final String id = mapVerse["id"].toString();
            if (!lstIdGen.contains(id)) lstIdGen.add(id);
          }
          lstIdGen.sort((a, b) => int.parse(a).compareTo(int.parse(b)));
          await P.Prefs.saveClipboardIds(lstIdGen);
          await _generateTextForClipboard(true);
        } catch (ex, stacktrace) {
          if (P.isDebug) P.printLog(ex, stacktrace);
        }
      }

      Widget _createHeaderMenu({required final R.id id, required final bool addTopSpace}) {
        return Container(
          padding: EdgeInsets.only(left: 0.0, top: addTopSpace ? 20.0 : 10.0, right: 0.0, bottom: 0.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                R.getString(id),
                style: TextStyle(fontFamily: PStyle.instance.fontFamilyForMenu(), color: fgColor, fontSize: 16.0, letterSpacing: 2.0),
              ),
              Divider(),
            ],
          ),
        );
      }

      final Visibility visListTileOpenChapter = Visibility(
        visible: gtype == 'S' ? false : true,
        child: Column(
          children: [
            ListTile(
              title: Text(mnuOpenChapterValue, style: textStyleMenu),
              leading: const Icon(Icons.book_outlined),
              onTap: () async {
                _openChapter();
              },
            ),
          ],
        ),
      );

      final Visibility visListTileOpenCR = Visibility(
        visible: crTot > 0,
        child: Column(
          children: [
            ListTile(
              title: Text(mnuOpenCrValue, style: textStyleMenu),
              leading: const Icon(Icons.book_outlined),
              onTap: () async {
                _openCR();
              },
            ),
          ],
        ),
      );

      final Visibility visListTileShare = Visibility(
        visible: true,
        child: Column(
          children: <Widget>[
            ListTile(
                title: Text(R.getString(R.id.mnuShare), style: textStyleMenu),
                leading: const Icon(Icons.share),
                onTap: () async {
                  if (Navigator.canPop(cmContext)) Navigator.pop(cmContext);
                  if (Navigator.canPop(cmContext)) Navigator.pop(cmContext);
                  final String textToShare = await _generateTextForClipboard(false);
                  await P.share(textToShare);
                }),
          ],
        ),
      );

      void _showOpenMenu() =>
          showModalBottomSheet<void>(
            context: cmContext,
            builder: (BuildContext context) {
              return Container(
                child: Center(
                  child: ListView(
                    padding: EdgeInsets.zero,
                    children: <Widget>[
                      _createHeaderMenu(id: R.id.mnuOpen, addTopSpace: true),
                      visListTileOpenChapter,
                      visListTileOpenCR,
                    ],
                  ),
                ),
              );
            },
          );

      final Visibility visListTileOpen = Visibility(
        visible: gtype != 'S' || crTot > 0,
        child: Column(
          children: [
            ListTile(
              title: Text(R.getString(R.id.mnuOpen), style: textStyleMenu),
              leading: const Icon(Icons.book_outlined),
              onTap: () async {
                _showOpenMenu();
              },
            ),
          ],
        ),
      );

      void _favoritesMgt(final int bibleId, final int action, final int mark) async {
        try {
          await dal.manageFavorite(bibleId, action, mark);

          if (Navigator.canPop(cmContext)) Navigator.pop(cmContext);
          if (Navigator.canPop(cmContext)) Navigator.pop(cmContext);
          P.showToast(cmContext, R.getString(action < 0 ? R.id.toastDeleted : R.id.toastAdded), Toast.LENGTH_SHORT);
          _onRefreshChapterWidget();
        } catch (ex, stacktrace) {
          if (P.isDebug) P.printLog(ex, stacktrace);
        }
      }

     void _showFavoritesMenu() {
        List<Widget> _getFavItems() {
          final List<Widget> lstWidgetInternal = [];

          try {
            const Icon addIcon = Icon(Icons.add);
            lstWidgetInternal.add(_createHeaderMenu(id: R.id.mnuFavorites, addTopSpace: true));
            lstWidgetInternal.add(ListTile(
              title: Text(R.getString(R.id.mnuDelete), style: textStyleMenu),
              leading: const Icon(Icons.delete_outline),
              onTap: () async {
                _favoritesMgt(bibleId, -1, -1);
              },
            ));

            if (P.isDebug) print("_favoritesMgt.getFavItems(): fav is empty:${R.fav.isEmpty.toString()}");
            if (P.isDebug) print("_favoritesMgt.getFavItems(): fav length:${R.fav.length}");
            R.fav.keys.forEach((key) {
              if (key >= 1)
                lstWidgetInternal.add(
                  ListTile(
                    title: RichText(
                      text: TextSpan(
                        text: "${R.fav[key]['bmCurrent']} ",
                        style: textStyleMenu,
                        children: [
                          TextSpan(text: "${R.fav[key]['bmDesc']}", style: textStyleMenu),
                        ],
                      ),
                    ),
                    leading: addIcon,
                    onTap: () async {
                      _favoritesMgt(bibleId, 1, key);
                    },
                  ),
                );
            });
          } catch (ex, stacktrace) {
            if (P.isDebug) P.printLog(ex, stacktrace);
          }
          return lstWidgetInternal;
        }

        try {
          final List<Widget> lstWidget = _getFavItems();
          if (P.isDebug) print("ContextualMenuFav clicked: lstWidget is empty:${lstWidget.isEmpty.toString()}");
          if (P.isDebug) print("ContextualMenuFav clicked: lstWidget length:${lstWidget.length}");

          showModalBottomSheet<void>(
            context: cmContext,
            builder: (BuildContext context) {
              return Container(
                child: Center(
                  child: ListView.builder(
                    itemCount: lstWidget.length,
                    itemBuilder: (BuildContext lvbContext, int index) {
                      return lstWidget[index];
                    },
                  ),
                ),
              );
            },
          );
        } catch (ex, stacktrace) {
          if (P.isDebug) P.printLog(ex, stacktrace);
        }
      }

      Future<void> _showClipboardMenu() => showModalBottomSheet<void>(
            context: cmContext,
            builder: (BuildContext context) {
              return Container(
                child: Center(
                  child: ListView(
                    padding: EdgeInsets.zero,
                    children: <Widget>[
                      _createHeaderMenu(id: R.id.mnuClipboard, addTopSpace: true),
                      ListTile(
                        title: Text(mnuClipboardAddVerseValue, style: textStyleMenu),
                        leading: const Icon(Icons.add),
                        onTap: () async {
                          await _addToClipboard(ClipboardAddType.VERSE);
                        },
                      ),
                      ListTile(
                        title: Text(mnuClipboardAddChapterValue, style: textStyleMenu),
                        leading: const Icon(Icons.add),
                        onTap: () async {
                          await _addToClipboard(ClipboardAddType.CHAPTER);
                        },
                      ),
                      ListTile(
                        title: Text(mnuClipboardClearValue, style: textStyleMenu),
                        leading: const Icon(Icons.delete_outline),
                        onTap: () async {
                          await _clearClipboard(true);
                        },
                      ),
                      visListTileShare,
                    ],
                  ),
                ),
              );
            },
          );

      showModalBottomSheet<void>(
        context: cmContext,
        builder: (BuildContext context) {
          return Container(
            child: Center(
              child: ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[
                  visListTileOpen,
                  ListTile(
                    title: Text(mnuFavoritesValue, style: textStyleMenu),
                    leading: const Icon(Icons.bookmark_border_outlined),
                    onTap: () async {
                      _showFavoritesMenu();
                    },
                  ),
                  ListTile(
                    title: Text(R.getString(R.id.mnuClipboard), style: textStyleMenu),
                    leading: const Icon(Icons.copy),
                    onTap: () async {
                      await _showClipboardMenu();
                    },
                  ),
                ],
              ),
            ),
          );
        },
      );
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  ///On option button selected in OptionPage: True => exit, False => continue
  bool _onOptionButtonSelected(final int indexButtonSelected) {
    try {
      switch (indexButtonSelected) {
        case -1:
          _onShowBooksMenu(this.context);
          return true;
        case -2:
          _onShowPrblsMenu(this.context, mnuPrblsValue);
          return true;
        case -3:
          _onShowArtsMenu(this.context, mnuArtsValue);
          return true;
      }
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return false;
  }

  Map _replaceBookName(final String bbName, final Map oldMap) {
    String newbName;
    switch (bbName) {
      case "k":
      case "2":
        newbName = 'Song Of Solomon';
        break;
      case "l":
      case "o":
        newbName = 'Cantique Des Cantiques';
        break;
      case "a":
        newbName = 'Cantares De Salomão';
        break;
      case "d":
        newbName = 'Cantico Dei Cantici';
        break;
      case "v":
      default:
        newbName = oldMap['bName'];
        break;
    }
    final Map newMap = {'bNumber': oldMap['bNumber'], 'bName': newbName, 'bsName': oldMap['bsName']};
    return newMap;
  }

  Future<void> _setAndSaveCurrentTabId(final int tabId) async {
    try {
      if (gtabId != tabId) {
        //Previous
        double updOffset = _getPositionOfScrollControllerChapterText();
        if (updOffset < 0.0) updOffset = 0.0;
        if (P.isDebug) print("gtabId:${gtabId} svOffset:${updOffset} => gtabId:${tabId}");
        await DbHelper.instance.updCacheTabOffset(gtabId, updOffset);
      }

      //Current
      gtabId = tabId;
      await P.Prefs.saveTabSelected(tabId.toString());
    } catch (ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  ///Sets gpNumber and gpNumberMax !!!
  ///To call only for Search. Pagination are not used for Article, Parable, Book, CR
  ///@newBBName and newSearchQuery are the params of new search
  Future<void> _setPageNumberParams({final String? newSearchType, final String? newBBName, final String? newSearchQuery, final int? newSearchFavFilter}) async {
    try {
      final dal = DbHelper.instance;
      int scrollPosY;
      String bbName;
      String query;
      String tabType;
      int favFilter;
      if (newSearchType == null && newBBName == null && newSearchQuery == null && newSearchFavFilter == null) {
        final Map c = await dal.getCacheTabById(gtabId);
        if (c.isEmpty) return;
        scrollPosY = c["scrollPosY"];
        bbName = c["bbName"];
        query = c["fullQuery"];
        tabType = c["tabType"];
        favFilter = c["favFilter"];
      } else {
        //TODO: NULL_SAFETY: problem if one or the 4 fields is null !!
        scrollPosY = 0;
        bbName = newBBName!;
        query = newSearchQuery!;
        tabType = newSearchType!;
        favFilter = newSearchFavFilter!;
      }
      final Map mapSearchBibleCount = await dal.getSearchBibleCount(tabType == "F" ? "F" : "B", bbName, query, gbbNames, favFilter);
      gqueryType = tabType == "F" ? "F" : "B";
      gqueryFavFilter = favFilter;
      gqueryBBName = bbName;
      gpNumber = (mapSearchBibleCount.isEmpty) ? 0 : (scrollPosY / P.pageRowLimit).floor();
      gpNumberMax = (mapSearchBibleCount.isEmpty) ? 0 : mapSearchBibleCount["pageNumberMax"];
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  ///@isRefresh means was already displayed and it's refreshed.
  Future<void> _manageTab(final bool isRefresh, final String navBarTitleShort) async {
    try {
      final dal = DbHelper.instance;

      //Insert or replace cache tab, common to BOOK/SEARCH/PRBL/ART
      if (isRefresh) {
        //Update
        if (gtype == "S") {
          final CacheTabBO c = CacheTabBO();
          c.tabId = gtabId;
          c.tabType = gtype;
          c.tabTitle = navBarTitleShort;
          c.favFilter = 0;
          c.fullQuery = gquery;
          c.scrollPosY = 0;
          c.bbName = gbbNames;
          c.isBook = 0;
          c.isChapter = 0;
          c.isVerse = 0;
          c.bNumber = gbNumber;
          c.cNumber = gcNumber;
          c.vNumber = 0;
          c.trad = c.bbName;
          c.orderBy = 0;
          c.offset = _getPositionOfScrollControllerChapterText();

          await dal.updCacheTab(c);
          await _setAndSaveCurrentTabId(gtabId);
        } else if (gtype == "S2" || gtype == "F") {
          final CacheTabBO c = CacheTabBO();
          c.tabId = gtabId;
          c.tabType = gtype == "F" ? "F" : "S";
          c.tabTitle = navBarTitleShort;
          c.favFilter = gtype == "F" ? gqueryFavFilter : 0;
          c.fullQuery = gquery;
          c.scrollPosY = gpNumber * P.pageRowLimit;
          c.bbName = gqueryBBName; //gbbNames
          c.isBook = 0;
          c.isChapter = 0;
          c.isVerse = 0;
          c.bNumber = 0;
          c.cNumber = 0;
          c.vNumber = 0;
          c.trad = c.bbName;
          c.orderBy = gtype == "F" ? gqueryOrderBy : 0;
          c.offset = _getPositionOfScrollControllerChapterText();

          await dal.updCacheTab(c);
          await _setAndSaveCurrentTabId(gtabId);
        } else if (gtype == "S3") {
          final CacheTabBO c = CacheTabBO();
          c.tabId = gtabId;
          c.tabType = "S";
          c.tabTitle = navBarTitleShort;
          c.favFilter = 0;
          c.fullQuery = gquery;
          c.scrollPosY = 0;
          c.bbName = gbbNames;
          c.isBook = 0;
          c.isChapter = 0;
          c.isVerse = 0;
          c.bNumber = gbNumber;
          c.cNumber = gcNumber;
          c.vNumber = gvNumber;
          c.trad = c.bbName;
          c.orderBy = 0;
          c.offset = _getPositionOfScrollControllerChapterText();

          await dal.updCacheTab(c);
          await _setAndSaveCurrentTabId(gtabId);
        } else if (gtype == "A" || gtype == "P") {
          final CacheTabBO c = CacheTabBO();
          c.tabId = gtabId;
          c.tabType = gtype;
          c.tabTitle = navBarTitleShort;
          c.favFilter = 0;
          c.fullQuery = gquery;
          c.scrollPosY = 0;
          c.bbName = gbbName0;
          c.isBook = 0;
          c.isChapter = 0;
          c.isVerse = 0;
          c.bNumber = 0;
          c.cNumber = 0;
          c.vNumber = 0;
          c.trad = gbbNames;
          c.orderBy = 0;
          c.offset = _getPositionOfScrollControllerChapterText();

          await dal.updCacheTab(c);
          await _setAndSaveCurrentTabId(gtabId);
        }
        return;
      } else {
        //Add new or replace
        final Map c = await dal.getFirstCacheTabByQuery(gtype, gquery, gqueryFavFilter);
        if (c.isNotEmpty) {
          //Already exists
          await _setAndSaveCurrentTabId(c["tabId"]);
        } else {
          //Add new
          final int tabIdMax = await dal.getCacheTabIdMax();
          final CacheTabBO c = CacheTabBO();
          c.tabId = tabIdMax + 1;
          c.tabType = gtype.startsWith("S") ? "S" : gtype;
          c.tabTitle = navBarTitleShort;
          c.favFilter = (gtype == "F") ? gqueryFavFilter : 0;
          c.fullQuery = gquery;
          c.scrollPosY = 0; //TODO: scrollposY to set to BIBLEID which will be decrypted to bnumber, cnumber, vnumber to be generic
          c.bbName = gtype == "S2" || gtype == "F" ? gqueryBBName : gbbName0;
          c.isBook = 0;
          c.isChapter = 0;
          c.isVerse = 0;
          c.bNumber = (gtype == "S" || gtype == "S3") ? gbNumber : 0;
          c.cNumber = (gtype == "S" || gtype == "S3") ? gcNumber : 0;
          c.vNumber = (gtype == "S3") ? gvNumber : 0;
          c.trad = gbbNames;
          c.orderBy = gtype == "F" ? gqueryOrderBy : 0;
          c.offset = _getPositionOfScrollControllerChapterText();

          await dal.addCacheTab(c);
          await _setAndSaveCurrentTabId(c.tabId);

          //Manage limit
          if (gtype == "A" || gtype == "P") {
            final int count = await dal.getTabCacheCountByType(gtype);
            if (count > P.historyLimit) {
              final int tabIdDel = await dal.getFirstCacheTabIdByType(gtype);
              if (tabIdDel >= 0) {
                await dal.delCacheTabById(tabIdDel);
              }
            }
          }
        }
      }
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    } finally {
      _onRefreshScrollTab(true); //TODO: TAB, add await if delete A P above?
    }
  }

  Future<TextSpan?> _getVerseTextSpan(final Map verseData) async {
    try {
      final int bibleId = verseData['id'];
      final int mark = verseData['mark'] ?? 0;
      final int vNumber = verseData['vNumber'];
      final String vBBName = verseData['bbName'];
      final String dir = vBBName == 'y' ? P.getRTL() : P.getLTR();
      final Locale localeDir = vBBName == 'y' ? Locale('ar') : Locale('en');
      final String vText = "${dir}${verseData['vText']}";
      final int crTot = verseData['tot'] ?? 0;

      const String end = ' \n ';
      final String vFontFamily;
      final double vFontSize;
      final Color vNumberFgColor, vNumberBgColor, vTextFgColor, vTextBgColor, vCrFgColor, vCrBgColor;
      {
        final CustomVerseStyle customVerseStyle = await P.getCustomVerseStyle(vBBName, false, gverseStyle is LightVerseStyle);
        vFontFamily = customVerseStyle.vFontFamily;
        vFontSize = customVerseStyle.vFontSize;
        vNumberFgColor = customVerseStyle.vNumberFgColor;
        vNumberBgColor = customVerseStyle.vNumberBgColor;
        vTextFgColor = customVerseStyle.vTextFgColor;
        vTextBgColor = customVerseStyle.vTextBgColor;
        vCrFgColor = customVerseStyle.vCrFgColor;
        vCrBgColor = customVerseStyle.vCrBgColor;
      }
      final TextSpan vNumberStyled = TextSpan(
        text: "$vNumber",
        style: TextStyle(fontFamily: vFontFamily, fontSize: vFontSize, color: vNumberFgColor, backgroundColor: vNumberBgColor),
        recognizer: TapGestureRecognizer()
          ..onTap = () async {
            await _onShowContextMenu(bibleId);
          },
      );
      final TextSpan crStyled = (crTot == 0)
          ? TextSpan(text: "")
          : TextSpan(
        text: "[$crTot]",
        style: TextStyle(fontFamily: vFontFamily, fontSize: vFontSize, color: vCrFgColor, backgroundColor: vCrBgColor),
        recognizer: TapGestureRecognizer()
          ..onTap = () async {
            final dal = DbHelper.instance;
            final Map verse = await dal.getVerse(bibleId);
            if (verse.isEmpty) return;
            final int bNumber = verse["bNumber"];
            final int cNumber = verse["cNumber"];
            final int vNumber = verse["vNumber"];
            _onOpenChapterWidgetCR(bNumber, cNumber, vNumber, false, 0);
          },
      );
      final TextSpan markStyled = (mark <= 0)
          ? TextSpan(text: "")
          : TextSpan(
        text: "${R.fav[mark]['bmCurrent']} ",
        style: TextStyle(
            fontFamily: vFontFamily,
            fontSize: vFontSize,
            color: vTextFgColor,
            backgroundColor: vTextBgColor,
        ),
        recognizer: TapGestureRecognizer()
          ..onTap = () async {
            await _onShowContextMenu(bibleId);
          },
      );
      final TextSpan endStyled = TextSpan(text: "$end");
      final TextSpan spacerStyled = TextSpan(text: " ", style: TextStyle(fontFamily: vFontFamily, fontSize: vFontSize, color: vTextFgColor, backgroundColor: vTextBgColor));

      TextSpan _getTextSpanTextPart({required final String text, Color? fgColor, Color? bgColor}) {
        if (fgColor == null) fgColor = vTextFgColor;
        if (bgColor == null) bgColor = vTextBgColor;
        return TextSpan(
          text: "$text",
          style: TextStyle(fontFamily: vFontFamily, fontSize: vFontSize, color: fgColor, backgroundColor: bgColor),
          recognizer: TapGestureRecognizer()
            ..onTap = () async {
              await _onShowContextMenu(bibleId);
            },
        );
      }

      //Gen parts
      final List<TextSpan> lstvTextStyled = [];
      if (gqueryExpr.isEmpty) {
        final TextSpan vTextStyled = _getTextSpanTextPart(text: vText);
        lstvTextStyled.add(vTextStyled);
      } else {
        final RegExp pattern = RegExp(gqueryExpr.replaceAll("%", ".*"), caseSensitive: false);
        final Iterable<RegExpMatch> matches = pattern.allMatches(vText);
        if (matches.length == 0) {
          final TextSpan vTextStyled = _getTextSpanTextPart(text: vText);
          lstvTextStyled.add(vTextStyled);
        } else {
          int pos = 0, matchStart, matchEnd, matchIndex = 0;
          String vTextPart;
          matches.forEach((match) {
            matchIndex++;
            matchStart = match.start;
            matchEnd = match.end;

            vTextPart = vText.substring(pos, matchStart);
            final TextSpan vTextPartBeforeStyled = _getTextSpanTextPart(text: vTextPart);
            lstvTextStyled.add(vTextPartBeforeStyled);

            vTextPart = vText.substring(matchStart, matchEnd);
            final TextSpan vTextPartFoundStyled = _getTextSpanTextPart(text: vTextPart, fgColor: gstyleHighlightSearch["fg"], bgColor: gstyleHighlightSearch["bg"]);
            lstvTextStyled.add(vTextPartFoundStyled);
            pos = matchEnd;

            if (matchIndex == matches.length) {
              vTextPart = vText.substring(matchEnd);
              final TextSpan vTextPartLastStyled = _getTextSpanTextPart(text: vTextPart);
              lstvTextStyled.add(vTextPartLastStyled);
            }
          });
        }
      }

      //Finally
      final List<InlineSpan> rowStyled = [];
      rowStyled.add(markStyled);
      rowStyled.add(vNumberStyled);
      rowStyled.add(spacerStyled);
      lstvTextStyled.forEach((vTextPartStyled) {
        rowStyled.add(vTextPartStyled);
      });
      if (crTot > 0) rowStyled.add(spacerStyled);
      rowStyled.add(crStyled);
      rowStyled.add(endStyled);

      return TextSpan(children: rowStyled, locale: localeDir);
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
    return null;
  }

  ///Create bar title
  TextSpan _getBarTitleStyled(final String barTitle, final String? barTitleFromTo) {
    final TextSpan barTitleFromToStyled = barTitleFromTo == null
        ? TextSpan(text: '')
        : TextSpan(
            text: "\n$barTitleFromTo",
            style: TextStyle(fontSize: PStyle.instance.fontSize + 2, fontFamily: PStyle.instance.fontFamily, fontStyle: FontStyle.normal, color: gverseStyle.bookNameFgColor),
          );
    final TextSpan barTitleWithoutPrefix = TextSpan(
      text: barTitle,
      style: TextStyle(fontSize: PStyle.instance.fontSize + 4, fontFamily: PStyle.instance.fontFamily, fontStyle: FontStyle.normal, color: gverseStyle.defaultFgColor),
      children: [barTitleFromToStyled],
    );
    return (gtype != "F")
        ? barTitleWithoutPrefix
        : TextSpan(
            text: "${R.fav[gqueryFavFilter]['bmCurrent']} ",
            style: TextStyle(fontSize: PStyle.instance.fontSize + 4, fontFamily: PStyle.instance.fontFamily, fontStyle: FontStyle.normal, color: gverseStyle.defaultFgColor),
            children: [barTitleWithoutPrefix],
          );
  }

//endregion -- End of Common to Books/Arts/Prbls --

//region -- Prbls menu --
//** Prbls menu **
  Future<void> _onShowPrblsMenu(final BuildContext context, final String title) async {
    final String bbName0 = gbbName0;
    final Prbl prbl = Prbl();

    void _optionPrblSelected(final int indexPrblSelected, final VerseStyle verseStyle) async {
      try {
        if (_onOptionButtonSelected(indexPrblSelected)) return;

        final int prblId = indexPrblSelected + 1;
        final String prblQuery = "PRBL$prblId";
        if (Navigator.of(context).canPop()) Navigator.of(context).pop();

        _onOpenChapterWidgetPrbl(prblQuery, false, 0.0);
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    Future<void> _show(final List<TextSpan> lstPrbl, final VerseStyle verseStyle) async {
      try {
        late int indexSelected;
        final Future optionResult = Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => OptionPage(
              titlePage: title,
              lstItem: lstPrbl,
              isAlignmentStart: true,
              topBarMenuType: TopBarMenuType.BPA,
              lstItemCheckedOrig: [],
              actionStyle: null,
              layoutType: 1,
              columns: 1,
            ),
          ),
        );
        optionResult.then((value) => indexSelected = value ?? -100);
        optionResult.whenComplete((){ if (indexSelected != -100) _optionPrblSelected(indexSelected, verseStyle); });
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    void _prepareData(final String themeName, final List<Map> lstPrbl) {
      try {
        final VerseStyle verseStyle = themeName.compareTo('LIGHT') == 0 ? LightVerseStyle() : DarkVerseStyle();

        final fontSize = PStyle.instance.fontSizeForMenu();
        final fontFamily = PStyle.instance.fontFamilyForMenu();
        final List<TextSpan> newLstPrbl = [];
        lstPrbl.forEach((row) {
          //Put logic here for several styles
          final String desc = row['desc'];
          final String bsName = row['bsName'];

          final TextSpan rowStyled = TextSpan(
            text: "($bsName) $desc",
            style: TextStyle(height: 2.0, fontFamily: fontFamily, fontSize: fontSize, color: verseStyle.defaultFgColor),
          );
          newLstPrbl.add(rowStyled);
        });

        _show(newLstPrbl, verseStyle);
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    void _getData(final String themeName) {
      final dal = DbHelper.instance;
      final List<Map> lstPrbl = [];
      final Map mapPrbl = prbl.getListAllPrblDesc();
      late Map bookRef;
      late Future<Map> bookRefFut;
      mapPrbl.forEach((k, v) {
        final String? prblRef = prbl.getPrblRef(k);
        if (prblRef != null) {
          final List<String> lstPrblRef = prblRef.split(' ');
          if (lstPrblRef.length != 4) return;
          final int bNumber = int.parse(lstPrblRef[0]);

          bookRefFut = dal.getBookRef(bbName0, bNumber);
          bookRefFut.then((value) => bookRef = value);
          bookRefFut.whenComplete(() {
            lstPrbl.add({'desc': v, 'bsName': bookRef['bsName']});
          });
        }
      });
      Future.wait([bookRefFut]).whenComplete(() => _prepareData(themeName, lstPrbl));
    }

    void _init() {
      _getData(gThemeName);
    }

    _init();
  }

//** End of Prbl menu **
//endregion

//region -- Arts menu --
//** Arts menu **
  Future<void> _onShowArtsMenu(final BuildContext context, final String title) async {
    void _optionArtSelected(final int indexOrderSelected, final VerseStyle verseStyle) async {
      try {
        if (_onOptionButtonSelected(indexOrderSelected)) return;

        final Art art = Art();
        final List<String> lstOrder = art.getListArtOrder(indexOrderSelected, false);
        final int artId = int.parse(lstOrder[0]);
        final String artQuery = "ART$artId";
        if (Navigator.of(context).canPop()) Navigator.of(context).pop();

        _onOpenChapterWidgetArt(artQuery, false, 0.0);
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    Future<void> _show(final List<TextSpan> lstArt, final VerseStyle verseStyle) async {
      try {
        late int indexSelected;
        final Future optionResult = Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => OptionPage(
              titlePage: title,
              lstItem: lstArt,
              isAlignmentStart: true,
              topBarMenuType: TopBarMenuType.BPA,
              lstItemCheckedOrig: [],
              actionStyle: null,
              layoutType: 1,
              columns: 1,
            ),
          ),
        );
        optionResult.then((value) => indexSelected = value ?? -100);
        optionResult.whenComplete((){ if (indexSelected != -100) _optionArtSelected(indexSelected, verseStyle); });
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    void _prepareData(final String themeName, final List<Map> lstArt) {
      try {
        final VerseStyle verseStyle = themeName.compareTo('LIGHT') == 0 ? LightVerseStyle() : DarkVerseStyle();

        final fontSize = PStyle.instance.fontSizeForMenu();
        final fontFamily = PStyle.instance.fontFamilyForMenu();
        final List<TextSpan> newLstArt = [];
        lstArt.forEach((row) {
          //Put logic here for several styles
          final String id = row['id'];
          final String desc = row['desc'];
          final String text = P.isDebug ? "($id) $desc" : desc;
          final int style = row['style'];
          final Color colorStyle = style == 2 ? Colors.red : verseStyle.defaultFgColor;

          final TextSpan rowStyled = TextSpan(
            text: text,
            style: TextStyle(height: 2.0, fontFamily: fontFamily, fontSize: fontSize, color: verseStyle.defaultFgColor),
          );

          newLstArt
              .add(style == 0 ? rowStyled : TextSpan(children: <TextSpan>[TextSpan(text: style == 1 ? "\u26A1 " : "\u2665 ", style: TextStyle(fontSize: fontSize, color: colorStyle)), rowStyled]));
        });

        _show(newLstArt, verseStyle);
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    void _getData(final String themeName) async {
      final Art art = Art();
      final List<String> mapArtOrder = art.getListArtOrder(-1, false);
      final Map mapArtDesc = await art.getListAllArtDesc();
      final List<Map> lstArt = [];
      int i = 0;
      for (var k in mapArtOrder) {
        lstArt.add({
          'id': k,
          'desc': mapArtDesc[k],
          'style': (i >= 6 && i <= 13)
              ? 1
              : (i == 5 || i == 14)
                  ? 2
                  : 0,
        });
        i++;
      }

      _prepareData(themeName, lstArt);
    }

    void _init() {
      _getData(gThemeName);
    }

    _init();
  }

//** End of Arts menu **
//endregion

//region -- Custom Bible menu --
//** Custom Bible menu **
  Future<void> _onShowCustomBibleMenu(final BuildContext context, final String title) async {
    late List<Map> lstBible;

    void _onItemBibleSelected(final int indexSelected, final VerseStyle verseStyle) async {
      try {
        if (_onOptionButtonSelected(indexSelected)) return;

        final int indexBibleSelected = indexSelected;
        if (indexBibleSelected < 0) return;

        final String bbNameSelected = lstBible[ indexBibleSelected ]["bbName"];
        final Future optionResult = Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CustomBiblePage(
              titlePage: lstBible[ indexBibleSelected ]["text"],
              bbNameSelected: bbNameSelected,
              menuVerseStyle: gverseStyle,
            ),
          ),
        );
        optionResult.whenComplete(() async {
          _onRefreshChapterWidget();
        });
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    Future<void> _show(final List<TextSpan> lstBible, final VerseStyle verseStyle) async {
      try {
        //Bibles
        late int indexSelected;
        final Future optionResult = Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => OptionPage(
              titlePage: title,
              lstItem: lstBible,
              isAlignmentStart: true,
              topBarMenuType: TopBarMenuType.NONE,
              lstItemCheckedOrig: [],
              actionStyle: null,
              layoutType: 1,
              columns: 1,
            ),
          ),
        );
        optionResult.then((value) => indexSelected = value ?? -1);
        optionResult.whenComplete((){ if (indexSelected >= 0) _onItemBibleSelected(indexSelected, verseStyle); });
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    void _prepareData(final String themeName, final List<Map> lstBible) async {
      try {
        final VerseStyle verseStyle = themeName.compareTo('LIGHT') == 0 ? LightVerseStyle() : DarkVerseStyle();

        final fontSize = PStyle.instance.fontSizeForMenu();
        final fontFamily = PStyle.instance.fontFamilyForMenu();
        final List<TextSpan> newLstBible = [];
        lstBible.forEach((row) {
          //Put logic here for several styles
          final String desc = row['text'];

          final TextSpan rowStyled = TextSpan(
            text: "$desc",
            style: TextStyle(height: 2.0, fontFamily: fontFamily, fontSize: fontSize, color: verseStyle.defaultFgColor),
          );
          newLstBible.add(rowStyled);
        });

        _show(newLstBible, verseStyle);
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    void _getData(final String themeName) {
      lstBible = R.getListAllBibleSettings();
      _prepareData(themeName, lstBible);
    }

    void _init() {
      _getData(gThemeName);
    }

    _init();
  }

//** End of Prbl menu **
//endregion

//region -- Search menu --
//** Search menu **
  Future<void> _onShowSearchMenu(final BuildContext context, final String title) async {
    final String bbName0 = gbbName0;

    void _optionSearchSelected(final Map? mapSearchResult, final VerseStyle verseStyle) async {
      try {
        await _setKeysForHomePage();
        if (mapSearchResult == null || mapSearchResult.isEmpty) return;

        gqueryExpr = mapSearchResult["queryExpr"];
        gqueryType = mapSearchResult["searchBoxType"];
        final String tabType = mapSearchResult["tabType"];
        if (tabType == 'S2' || tabType == "F") {
          final String searchQuery = mapSearchResult["searchBox"];
          final String searchQueryBBName = mapSearchResult["searchBoxBBName"];
          final String searchQueryType = mapSearchResult["searchBoxType"];
          final int searchQueryOrderBy = mapSearchResult["searchBoxOrderBy"];
          final int searchQueryFavFilter = mapSearchResult["searchBoxFavFilter"];
          //New Search: page 0
          await _setPageNumberParams(newSearchType: searchQueryType, newBBName: searchQueryBBName, newSearchQuery: searchQuery, newSearchFavFilter: searchQueryFavFilter);
          _onOpenChapterWidgetSearch(searchQueryType, searchQueryOrderBy, searchQueryBBName, searchQuery, 0, false, searchQueryFavFilter, 0.0);
        } else {
          final int bNumber = mapSearchResult["bNumber"];
          final int cNumber = mapSearchResult["cNumber"];
          _onOpenChapterWidgetBook(bNumber, cNumber, 0, false, 0.0);
        }
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    Future<void> _show(final VerseStyle verseStyle) async {
      try {
        final Map? searchResult = await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SearchPage(
              titlePage: mnuSearchValue,
              isAlignmentStart: true,
              topBarMenuType: TopBarMenuType.LANGUAGE,
              actionStyle: verseStyle,
              extraBBName0: bbName0,
              extraBBNames: gbbNames,
              extraSearchType: gqueryType,
              extraSearchOrderBy: gqueryOrderBy,
              extraSearchFavFilter: gqueryFavFilter,
            ),
          ),
        );
        _optionSearchSelected(searchResult, verseStyle);
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    Future<void> _init() async {
      await _unSetAllKeys();
      await _setKeysForSearchPage();
      await _show(gverseStyle);
    }

    _init();
  }

//** End of Search menu **
//endregion

//region -- Books menu --
//** Books menu **
  Future<void> _onShowBooksMenu(final BuildContext context) async {
    late int cCount;
    final String themeName = gThemeName;
    final String bbName0 = gbbName0;
    final dal = DbHelper.instance;
    final fontSize = PStyle.instance.fontSizeForMenu();
    final fontFamily = PStyle.instance.fontFamilyForMenu();
    final VerseStyle verseStyle = themeName.compareTo('LIGHT') == 0 ? LightVerseStyle() : DarkVerseStyle();
    final textStyleMenu = TextStyle(height: 2.0, fontFamily: fontFamily, fontSize: fontSize, color: verseStyle.defaultFgColor);

    void _optionChapterSelected(final int indexChapterSelected) {
      try {
        gbNumber = gbNumberTemp;
        gcNumber = indexChapterSelected + 1;
        gvNumber = 0;
        if (Navigator.of(context).canPop()) Navigator.of(context).pop();
        _onOpenChapterWidgetBook(gbNumber, gcNumber, 0, false, 0.0); //TODO: OFFSET, not goffset here?
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    void _whenCompletedCi() async {
      try {
        final List<TextSpan> lstChapter = List.generate(cCount, (index) => TextSpan(text: "${index + 1}", style: textStyleMenu));

        late int indexChapterSelected;
        final Future optionResult = Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => OptionPage(
              titlePage: mnuChaptersValue,
              lstItem: lstChapter,
              isAlignmentStart: false,
              topBarMenuType: TopBarMenuType.NONE,
              lstItemCheckedOrig: [],
              actionStyle: null,
              layoutType: 1,
              columns: 1,
            ),
          ),
        );
        optionResult.then((value) => indexChapterSelected = value ?? -1);
        optionResult.whenComplete((){ if (indexChapterSelected >= 0) _optionChapterSelected(indexChapterSelected); });
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    void _optionBookSelected(final int indexBookSelected, final VerseStyle verseStyle) async {
      try {
        if (_onOptionButtonSelected(indexBookSelected)) return;

        gbNumberTemp = indexBookSelected + 1;

        final Future<Map> countFut = dal.getBibleCiByBook(bbName0, gbNumberTemp);
        countFut.then((value) => cCount = value.isEmpty ? 0 : value['cCount']);
        countFut.whenComplete(() => _whenCompletedCi());
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    Future<void> _show(final List<TextSpan> lstBook, final VerseStyle verseStyle) async {
      try {
        late int indexSelected;
        final Future optionResult = Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => OptionPage(
              titlePage: mnuBooksValue,
              lstItem: lstBook,
              lstItemLocale: bbName0 == 'y' ? Locale('ar') : Locale('en'),
              isAlignmentStart: true,
              topBarMenuType: TopBarMenuType.BPA,
              lstItemCheckedOrig: [],
              actionStyle: null,
              layoutType: 1,
              columns: 1,
            ),
          ),
        );
        optionResult.then((value) => indexSelected = value ?? -100);
        optionResult.whenComplete((){ if (indexSelected != -100) _optionBookSelected(indexSelected, verseStyle); });
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    void _prepareData(final List<Map> lstBook) {
      try {
        final List<TextSpan> newLstBook = [];
        lstBook.forEach((row) {
          //Put logic here for several styles
          final int bNumber = row['bNumber'];
          final String bName = row['bName'];
          final String text = "$bNumber $bName";

          final TextSpan rowStyled = TextSpan(
            text: text,
            style: TextStyle(height: 2.0, fontFamily: fontFamily, fontSize: fontSize, color: verseStyle.defaultFgColor),
            locale: bbName0 == 'y' ? Locale('ar') : Locale('en'),
          );
          newLstBook.add(rowStyled);
        });

        _show(newLstBook, verseStyle);
      } catch(ex, stacktrace) {
        if (P.isDebug) P.printLog(ex, stacktrace);
      }
    }

    Future<List<Map>> _getData(final String bbName) async {
      final List<Map> lstBook = [];
      final Future<List<Map>> fut = dal.getListAllBookByBBName(bbName, 'bNumber');
      fut.then((fut) => fut.forEach((row) {
            if (row['bNumber'] == 22) {
              final Map newRow = _replaceBookName(bbName, row);
              lstBook.add(newRow);
            } else {
              lstBook.add(row);
            }
          }));
      fut.whenComplete(() => _prepareData(lstBook));

      return fut;
    }

    void _init() {
      _getData(bbName0);
    }

    _init();
  }

//** End of Books menu **
//endregion

  @override
  void initState() {
    try {
      super.initState();
      chapterTextScrollController = ScrollController();

      Future<bool> willCreateDbFut = onUpdateDatabase();
      willCreateDbFut.whenComplete(() => _getInitResources());
    } catch(ex, stacktrace) {
      if (P.isDebug) P.printLog(ex, stacktrace);
    }
  }

  @override
  Widget build(final BuildContext homePageContext) {

    //bool _canPop() => Navigator.of(homePageContext).canPop();

    /*
    void _onHorizontalDragEnd(final DragEndDetails details) {
      const velocity = 10.0;
      if (details.primaryVelocity! < velocity) {
        _onClickBtnChapterLeft(); //Swipe left
      } else if (details.primaryVelocity! > -velocity) {
        _onClickBtnChapterRight(); //Swipe right
      }
    }
    */

    void _onHorizontalDragEnd(final DragEndDetails details) {
      if (dragDistance > swipeThreshold) {
        _onClickBtnChapterRight(); //Swipe right
      } else if (dragDistance < -swipeThreshold) {
        _onClickBtnChapterLeft(); //Swipe left
      }

      dragDistance = 0.0;
    }

    final textStyleMenu = TextStyle(fontFamily: PStyle.instance.fontFamilyForMenu(), fontSize: 18.0); //fontFamily: "Droid-sans-mono.regular",
    final Visibility visAltLanguage = Visibility(
      visible: ("siycjrtbheuz".indexOf(gbbName0) >= 0),
      child: ListTile(
        title: Text(mnuAltLanguageValue, style: textStyleMenu),
        subtitle: Text(P.getLocaleNameVerbose(gbbAltLocale)),
        leading: const Icon(Icons.language),
        onTap: () async {
          await _onShowBiblesMenu(homePageContext, mnuAltLanguageValue, SelectionType.ALT);
        },
      ),
    );
    final bool enableLeftChapter = (gtype == "S2" || gtype == "F")
        ? (gpNumber >= 1)
        : (gtype == "S" || gtype == "A" || gtype == "P")
            ? true
            : false;
    final bool enableRightChapter = (gtype == "S2" || gtype == "F")
        ? (gpNumber < gpNumberMax)
        : (gtype == "S" || gtype == "A" || gtype == "P")
            ? true
            : false;

    final Drawer drawer = Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          SizedBox(height: 30.0),
          ListTile(
            title: Text(widget.title, style: TextStyle(fontFamily: "AveriaGruesaLibre.regular", fontSize: 22.0, fontWeight: FontWeight.bold)),
          ),
          Divider(),
          ListTile(
            title: Text(mnuBiblePreferredValue, style: textStyleMenu),
            subtitle: Text(P.getBibleName(gbbName0, true)),
            leading: const Icon(Icons.language),
            onTap: () async {
              await _onShowBiblesMenu(homePageContext, mnuBiblePreferredValue, SelectionType.SINGLE);
            },
          ),
          visAltLanguage,
          ListTile(
            title: Text(mnuBibleToDisplayValue, style: textStyleMenu),
            leading: const Icon(Icons.language),
            onTap: () async {
              await _onShowBiblesMenu(homePageContext, mnuBibleToDisplayValue, SelectionType.MULTI);
            },
          ),
          Divider(),
          ListTile(
            title: Text(mnuSearchValue, style: textStyleMenu),
            leading: const Icon(Icons.search),
            onTap: () async {
              await _onShowSearchMenu(homePageContext, mnuSearchValue);
            },
          ),
          ListTile(
            title: Text(mnuHistoryValue, style: textStyleMenu),
            leading: const Icon(Icons.history),
            onTap: () async {
              await _onShowHistMenu(homePageContext, mnuHistoryValue);
            },
          ),
          ListTile(
            title: Text(mnuBooksValue, style: textStyleMenu),
            leading: const Icon(Icons.book_outlined),
            onTap: () {
              _onShowBooksMenu(homePageContext);
            },
          ),
          ListTile(
            title: Text(mnuPrblsValue, style: textStyleMenu),
            leading: const Icon(Icons.assignment),
            onTap: () {
              _onShowPrblsMenu(homePageContext, mnuPrblsValue);
            },
          ),
          ListTile(
            title: Text(mnuArtsValue, style: textStyleMenu),
            leading: const Icon(Icons.short_text),
            onTap: () {
              _onShowArtsMenu(homePageContext, mnuArtsValue);
            },
          ),
          const Divider(),
          ListTile(
            title: Text(mnuSettingsValue, style: textStyleMenu),
            leading: const Icon(Icons.settings_outlined),
            onTap: () {
              _onShowSettingsMenu(homePageContext, mnuSettingsValue);
            },
          ),
          ListTile(
            title: Text(mnuHelpValue, style: textStyleMenu),
            leading: const Icon(Icons.help_outline),
            onTap: () {
              _onShowHelpMenu(homePageContext, mnuHelpValue);
            },
          ),
          ListTile(
            title: Text(mnuInviteFriendValue, style: textStyleMenu),
            leading: const Icon(Icons.contacts_outlined),
            onTap: () {
              _onShowInviteFriendMenu(homePageContext, mnuInviteFriendValue);
            },
          ),
          ListTile(
            title: Text(mnuAboutValue, style: textStyleMenu),
            leading: const Icon(Icons.info_outline),
            onTap: () {
              _onShowAboutMenu(homePageContext, mnuAboutValue);
            },
          ),
        ],
      ),
    );

    final Column chapterTitle = Column(
      children: [
        SizedBox(
          height: 50.0,
        ),
        Container(
          alignment: Alignment(-1.0, -1.0),
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: RichText(
              text: navBarTitle,
            ),
            trailing: Column(
              children: [
                IconButton(
                  alignment: Alignment(1.0, 0.0),
                  padding: EdgeInsets.zero,
                  icon: const Icon(Icons.clear),
                  //size: PStyle.instance.fontSizeForIconInPage),
                  tooltip: R.getString(R.id.mnuDelete),
                  onPressed: () => _onDeleteTab(gtabId),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 50.0,
        ),
      ],
    );

    final PopupMenuButton popMenuBook = PopupMenuButton(
      icon: Icon(Icons.book_outlined),
      tooltip: mnuBooksValue,
      itemBuilder: (BuildContext context) => [
        PopupMenuItem(
          padding: EdgeInsets.zero,
          child: Row(
            children: [
              IconButton(
                icon: const DecoratedIcon(
                  icon: const Icon(Icons.book, color: Colors.greenAccent),
                  decoration: IconDecoration(border: IconBorder()),
                ),
                tooltip: mnuBooksValue,
                onPressed: () async {
                  Navigator.pop(context);
                  _onShowBooksMenu(context);
                },
              ),
              IconButton(
                icon: const DecoratedIcon(
                  icon: const Icon(Icons.assignment, color: Colors.yellow),
                  decoration: IconDecoration(border: IconBorder()),
                ),
                tooltip: mnuPrblsValue,
                onPressed: () async {
                  Navigator.pop(context);
                  _onShowPrblsMenu(context, mnuPrblsValue);
                },
              ),
              IconButton(
                icon: const DecoratedIcon(
                  icon: const Icon(Icons.short_text, color: Colors.orangeAccent),
                  decoration: IconDecoration(border: IconBorder()),
                ),
                tooltip: mnuArtsValue,
                onPressed: () async {
                  Navigator.pop(context);
                  _onShowArtsMenu(context, mnuArtsValue);
                },
              ),
            ],
          ),
        ),
      ],
    );

    final SliverAppBar appBar = SliverAppBar(
      floating: false,
      pinned: true,
      snap: false,
      title: null,
      toolbarHeight: gAppBarHeight,
      actions: <Widget>[
        IconButton(
            icon: const Icon(Icons.search),
            tooltip: R.getString(R.id.mnuSearch),
            onPressed: () async {
              await _onShowSearchMenu(homePageContext, mnuSearchValue);
            }),
        popMenuBook,
        IconButton(
          icon: const Icon(Icons.history),
          tooltip: R.getString(R.id.mnuHistory),
          onPressed: () async {
            await _onShowHistMenu(homePageContext, mnuHistoryValue);
          },
        ),
        IconButton(
          icon: const Icon(Icons.chevron_left),
          disabledColor: Colors.grey,
          onPressed: enableLeftChapter
              ? () {
                  _onClickBtnChapterLeft();
                }
              : null,
        ),
        IconButton(
          icon: const Icon(Icons.chevron_right),
          disabledColor: Colors.grey,
          onPressed: enableRightChapter
              ? () {
                  _onClickBtnChapterRight();
                }
              : null,
        ),
      ],
    );

    final Widget body = Container(
      child: ListView(
        controller: chapterTextScrollController,
        padding: EdgeInsets.only(left: 15.0, top: 0.0, right: 15.0, bottom: 0.0),
        shrinkWrap: false,
        physics: const AlwaysScrollableScrollPhysics(),
        children: [
          Container(
            padding: EdgeInsets.zero,
            child: Column(
              children: [
                chapterTitle,
                GestureDetector(
                  onHorizontalDragUpdate: (details) => dragDistance += details.primaryDelta ?? 0.0,
                  onHorizontalDragEnd: (details) => _onHorizontalDragEnd(details),
                  child: ChapterWidget(bbNames: gbbNames, chapterType: chapterType, lstChapterObject: chapterObject, colCount: gcolCount),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      key: scaffoldKey,
      drawer: drawer,
      body: NestedScrollView(
        headerSliverBuilder: (context, innerBoxIsScrolled) => [
          appBar,
        ],
        body: Column(
          children: [
            scrollTab,
            Expanded(child: body),
          ],
        ),
      ),
    );

    /*
    SliverAppBar(
            floating: false,
            pinned: true,
            snap: false,
            //expandedHeight: 50,
            flexibleSpace: FlexibleSpaceBar(
              title: Text('Header Scrollable'),
              background: Container(color: Colors.blue),
            ),
          ),
     */

    /*
    return Scaffold(
      key: scaffoldKey,
      drawer: drawer,
      appBar: appBar,
      //body: body,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            floating: false, // Ne réapparaît pas immédiatement lors d'un petit scroll
            pinned: true, // Ne reste pas visible en permanence
            snap: false, // Doit être combiné avec floating pour s'animer directement

            flexibleSpace: FlexibleSpaceBar(
              title: Text('Mon SliverAppBar'),
              background: Container(color: Colors.blue),
            ),
          ),
          SliverFillRemaining(
            hasScrollBody: true,
            child: body,
          ),

          /*
          SliverList(
           delegate: SliverChildBuilderDelegate(
              (context, index) => ListTile(
                title: Text('Élément $index'),
              ),
              childCount: 30,
            ),
          ),
           */
        ],
      ),
    );

     */

/*
      NOTE: Following code has been removed because there is a bug in GHK.

      import 'package:galactic_hotkeys/galactic_hotkeys.dart' as GHK;

      body: GHK.GalacticHotkeys<ShortcutKeys>(
        shortcuts: {
          ShortcutKeys.ESC:   [[LogicalKeyboardKey.escape]],
          ShortcutKeys.LEFT:  [[LogicalKeyboardKey.arrowLeft]],
          ShortcutKeys.RIGHT: [[LogicalKeyboardKey.arrowRight]],
          ShortcutKeys.M:     [[LogicalKeyboardKey.keyM]],
          ShortcutKeys.TAB:   [[LogicalKeyboardKey.tab]],
          ShortcutKeys.S:     [[LogicalKeyboardKey.keyS]],
          ShortcutKeys.F:     [[LogicalKeyboardKey.keyF]],
          ShortcutKeys.B:     [[LogicalKeyboardKey.keyB]],
          ShortcutKeys.A:     [[LogicalKeyboardKey.keyA]],
          ShortcutKeys.P:     [[LogicalKeyboardKey.keyP]],
        },
        onShortcutPressed: (ShortcutKeys identifier, List<LogicalKeyboardKey> pressedKeys)
        {
          print("Shortcut pressed: ${identifier.toString()}");

          switch (identifier) {
            case ShortcutKeys.ESC: {
              //Never block the ESC key
              if (_canPop()) Navigator.of(homePageContext).pop();
              break;
            }
            case ShortcutKeys.LEFT: {
              if (_canPop()) return; //Block the key
              _onClickBtnChapterLeft();
              break;
            }
            case ShortcutKeys.RIGHT: {
              if (_canPop()) return; //Block the key
              _onClickBtnChapterRight();
              break;
            }
            case ShortcutKeys.M: {
              if (_canPop()) return; //Block the key
              if (scaffoldKey.currentState != null && scaffoldKey.currentState!.hasDrawer) scaffoldKey.currentState?.openDrawer();
              break;
            }
            case ShortcutKeys.TAB: {
              if (_canPop()) return; //Block the key
              _onShowHistMenu(homePageContext, mnuHistoryValue);
              break;
            }
            case ShortcutKeys.S: {
              if (_canPop()) return; //Block the key
              _onShowSearchMenu(homePageContext, mnuSearchValue);
              break;
            }
            case ShortcutKeys.F: {
              if (_canPop()) return; //Block the key
              //TODO: search F
              _onShowSearchMenu(homePageContext, mnuSearchValue);
              break;
            }
            case ShortcutKeys.B: {
              if (_canPop()) return; //Block the key
              _onShowBooksMenu(homePageContext);
              break;
            }
            case ShortcutKeys.P: {
              if (_canPop()) return; //Block the key
              _onShowPrblsMenu(homePageContext, mnuPrblsValue);
              break;
            }
            case ShortcutKeys.A: {
              if (_canPop()) return; //Block the key
              _onShowArtsMenu(homePageContext, mnuArtsValue);
              break;
            }
            default:
              break;
          }
        },
        child: body,
      ),
*/

  } //Build

}

/*

CREATE TABLE bookmark (
bmId INTEGER NOT NULL,
bmCurrent TEXT NOT NULL,
bmDesc TEXT NOT NULL,
bmDev1 TEXT NOT NULL,
bmDev2 TEXT DEFAULT '',
bmDev3 TEXT DEFAULT '',
bmPrev1 TEXT DEFAULT '',
bmPrev2 TEXT DEFAULT '',
bmPrev3 TEXT DEFAULT '',
bmPrev4 TEXT DEFAULT '',
bmPrev5 TEXT DEFAULT '',
PRIMARY KEY(bmId)
)

delete from bookmark;
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (0, '☆', '☆', 'Internal', '', '');
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (1, '✞', '✞', 'Favorite', '✝', '✝️');
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (2, '️📖', '📖', 'Reading', '', '');
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (10, '💕️', '💕️', 'Love', '❤️', '♥️');
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (20, '️💯', '💯', 'Top 100', '☝️', '👍');
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (23, '📚', '📚', 'To read', '📓', '📔');
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (30, '️✏️', '✏️', 'To study', '🖋️', '🖍️️');
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (40, '🤔️', '🤔', 'To solve', '🤨', '💡');
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (50, '✅', '✅', 'Done', '☑️', '🆗');
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (60, '❗️', '❗', 'Exclamation', '❕', '');
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (70, '️❓', '❓', 'Question', '❔', '');
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (80, '️🔥', '🔥', 'Important', '🔑', '🗝️');
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (90, '️⚡', '⚡', 'Danger', '💥', '⚠️️');
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (100, '️💀', '💀', 'Death', '☠️', '👽');
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (105, '️🌱', '🌱', 'Life', '🌼', '🌸');
insert into bookmark (bmId, bmDev1, bmCurrent, bmDesc, bmDev2, bmDev3) values (110, '️✨', '✨', 'Prophecy', '🌟', '☄️');
select  * from bookmark;

 */
