### 1.31.0 (2025-02-12) 

• Scroll position is saved

### 1.30.0 (2025-01-28)

• Support for Android 15.0 (V): Target

### 1.29.1 (2024-12-12)

• Fix: Swipe

### 1.29.0 (2024-12-08)

• Settings: Customize each Bible with colors and fonts
• Settings: Simplification with grid layout (please use the new setting)

### 1.27.4 (2024-10-03)

• Support: Linux (https://flathub.org)
• Support: Linux (https://www.appimagehub.com)

### 1.27.3 (2024-07-25)

• Rebuild of 1.27 to solve F-Droid problem

### 1.27 (2024-07-05)

• Turkish Bible: New Turkish Bible 2001
• Bengali Bible: Bengali C.L. Bible 2016
• Swahili Bible: Swahili Union Version 1997
• German Bible: Elberfelder 1932
• Romanian Bible: Romanian Cornilescu 1928
• Polish Bible: Biblia Warszawska 1975

### 1.26 (2024-05-07)

• Support for Android 14.0 (U): Target

### 1.25 (2023-08-23)

• Fix: Open articles
• Support for Android 13.0 (T): Target

### 1.24 (2023-05-30)

• Hindi Bible: Hindi Indian Revised Version 2017
• Arabic Bible: Smith & Van Dyke 1865
• Chinese Bible: Chinese Union Version Simplified
• Japanese Bible: New Japanese Bible 1973
• Russian Bible: Russian Synodal Translation 1876
• New setting for some Bibles: Language of User interface
• Removed German translation (please use the new setting)

### 1.23 (2023-03-20)

• Spanish Bible: Reina Valera Actualizada 1989
• Italian Bible: Nuova Diodati 1991

### 1.22 (2022-10-22)

• Support for Android 12.0 (S): Target

### 1.21 (2022-09-19)

• Fix: Open chapter

### 1.20 (2022-08-30)

• About: Website
• Article: How to be saved?
• Article: Additional tools
• Better performance

### 1.19 (2022-05-26)

• (English) Free ebook: The Rapture Survival Guide
• MacOS, Linux: Shortcut keys (A, B, P, M, S, F, ESC, TAB, LEFT, RIGHT)

### 1.18 (2022-01-16)

• History view in main screen

### 1.17 (2022-01-02)

• Fix: history
• Search for specific favorites
• Fix: German translation (Thanks ptC7H12)
• Fix: Portuguese translation (Thanks Danesc)

### 1.15 (2021-11-18)

• Better performance
• Settings: Favorites (management of types of favorites)

### 1.14 (2021-10-21)

• Favorites

### 1.13 (2021-10-10)

• Support: Linux (https://snapcraft.io/)

### 1.12 (2021-10-05)

• Better performance
• Display support for visually impaired: large fonts supported
• Swipe left/right to change page
• Search in displayed Bibles
• Support: MacOS

### 1.11 (2021-09-23)

• English Bible: KJV 2000
• Contact: Telegram
• Fix: Share, URL...
• Fonts added
• Settings: Search style

### 1.10 (2021-03-17)

• Youtubers

### 1.9 (2020-12-17)

• Articles & Youtubers

### 1.8 (2020-11-11)

• Cross references

### 1.7 (2020-11-02)

• Several fixes

### 1.6 (2020-10-25)

• German Bible: Schlachter

### 1.5 (2020-10-19)

• Display in columns

### 1.4 (2020-10-14)

• Display several Bibles

### 1.3 (2020-09-30)

• Search in the Bible (see the section in Help)
• +14 Youtubers
• +4 Articles

### 1.2 (2020-07-26)

• +24 Articles
• +16 Youtubers
• +6 Podcasts
• New icon (thanks Jesper Stighem)
• Click on a verse number to Copy to clipboard, Open a chapter

### 1.1 (2020-05-21)

• Parables
• Articles
• History

### 1.0 (2020-03-20)

• First version
